<?php

return [
    'services' => [
        'facebook' => [
            'name'  => 'facebook',
            'icon'  => 'fa-facebook-official',
            'title' => 'Facebook',
            'color' => '#103944',
        ],
        'youtube' => [
            'name'  => 'youtube',
            'icon'  => 'fa-youtube-play',
            'title' => 'YouTube',
            'color' => '#9f0800',
        ],
        'gplus' => [
            'name'  => 'gplus',
            'icon'  => 'fa-google-plus-square',
            'title' => 'Google+',
            'color' => '#9f4d00',
        ],
        'twitter' => [
            'name'  => 'twitter',
            'icon'  => 'fa-twitter',
            'title' => 'Twitter',
            'color' => '#008fa3',
        ],
    ],
];
