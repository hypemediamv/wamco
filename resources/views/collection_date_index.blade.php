@extends('layouts.front2')

@section('content')

<div class="main-container">
           <section class="text-center imagebg" data-overlay="4">
               <div class="background-image-holder"> <img alt="background" src="{{url('/')}}/img/hero-1.jpg"> </div>
               <div class="container">
                   <div class="row">
                       <div class="col-sm-10 col-md-8">
                           <h1>Scheduled Collection Dates</h1>
                           <p class="lead"> Collection Dates for regions and sites </p>
                       </div>
                   </div>
               </div>
           </section>
       </div>


       <div class="tabs-container text-center" data-content-align="left">
       	<ul class="tabs">

          @foreach($collection_zones as $key => $zone)

          @if($key == 0)
            <li class="active">
          @else
          <li>
          @endif
          <div class="tab__title">
            <span class="h5">{{$zone->name}}</span>

              <section class="text-center">
                  <div class="container">
                      <div class="row">

                        <h1 class="text-center">{{$zone->name}}</h1>
                        <p class="lead text-center"> Select Scheduled Collection Sites in  <b>{{$zone->name}}<b> </p>

                          @foreach($zone->collection_date_sites as $key2 =>  $site)

                          <h2 class="text-center">{{$site->name}}</h2>


                          @foreach($site->collection_dates as $collection_date)
                                    <p class="lead"> {{$collection_date->details}}</p>
                                    {!! $collection_date->content!!}

                            @endforeach



                          @endforeach

                      </div>
                    </div>
              </section>
       			</div>

       		</li>

          @endforeach

       	</ul>
       </div><!--end of tabs container-->






@endsection
