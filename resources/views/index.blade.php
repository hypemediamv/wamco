@extends('layouts.front2')

@section('content')

            <section class="cover height-70 imagebg text-center slider slider--ken-burns" data-arrows="true" data-paging="true">
                <ul class="slides">
                  @foreach($slideshow->slides as $key => $slide)


                    <li class="imagebg" data-overlay="4">
                        <div class="background-image-holder background--top"> <img alt="background" src=" {{$slide->slide_image->image->path}}"> </div>
                        <div class="container pos-vertical-center">
                            <div class="row">
                                <div class="col-sm-12">
                                    <span>
                                  <h1>{{$slide->slide_name}}</h1>
                                  <h2>{{$slide->slide_description}}</h2>


                                    </span>
                                </div>
                            </div>
                        </div>
                    </li>
                    @endforeach

                </ul>
            </section>

            <section class="text-center bg--secondary" id="services">
                <div class="container" >
                    <div class="row">
                        <div class="col-sm-12">
                          <h1>{{$service_page->title}}</h1>
                          <p class="lead">{{$service_page->description}} </p>
                            <div class="row slider" data-arrows="false" data-paging="true">
                                <ul class="slides">

                               @foreach($service_page->services as $service)

                                    <li class="col-sm-4 col-xs-12">
                                      <div class="card__top">
                                          <a href="#"> <img alt="Image" src="{{url('/').$service->image->path}}"> </a>
                                      </div>
                                        <div class="feature feature-3 boxed boxed--lg boxed--border text-center">

                                            <h4>{{$service->name}}</h4>
                                            <p>
                                              {{$service->description}}
                                            </p>
                                        	<a href="{{route('service-single',$service->id)}}">
                                             Read More`
                                            </a>
                                        </div>
                                        <!--end feature-->
                                    </li>
                                    @endforeach

                                </ul>
                            </div>
                            <!--end of row-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

            <section class="text-center" id="about">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                            <h1>{{$about_page->title}}</h1>
                            <p class="lead"> {{$about_page->description}} </p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="text-center">
                <div class="container">
                    <div class="row">
                      @foreach($about_page->staff as $staff)

                        <div class="col-sm-4">

                            <div class="feature feature-8">
                              <img class="image img-circle grayscale" src="{{url('/').$staff->image->path}}" width="140" height="140" >

                                <h5>{{$staff->name}}</h5> <span>{{$staff->quote}} &amp;{{$staff->designation}}</span> </div>
                        </div>

                        @endforeach

                    </div>
                    <a href="{{route('company-profile')}}">
                       Read More`
                      </a>
                </div>
            </section>


            <section class="text-center  bg--secondary" id="resources">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8">
                          <h1>{{$resource_page->title}}</h1>
                          <p class="lead"> {{$resource_page->description}}</p>
                        </div>
                    </div>
                </div>
            </section>
            <section class="text-center  bg--secondary">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="tabs-container" data-content-align="left">
                                <ul class="tabs">

                                    @foreach($resource_page->resource_tabs as $tab_key => $resource_tab)

                                      @if($tab_key==0)
                                      <li class="active">

                                      @else
                                    <li>
                                      @endif

                                        <div class="tab__title"> <span class="h5">{{$resource_tab->name}} </span> </div>
                                        <div class="tab__content">

                                          <ul>
                                          @foreach($resource_tab->resources as $resource)
                                            <li>
                                              <div class="row">
                                            <div class="col-md-8">
                                             <b>{{$resource->name}}</b> <br> {{$resource->description}}
                                           </div>

                                            <div class="col-md-4">
                                              <a href="{{ action('FrontPageController@download',$resource->id) }}"> <button type="submit" class="btn btn--primary type--uppercase">Download</button> </a>

                                            </div>
                                          </div>
                                          </li>
                                          <br>







                                          @endforeach
                                            </ul>

                                        </div>
                                    </li>

                                    @endforeach

                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="switchable imagebg" data-overlay="4" id="contact">
                <div class="background-image-holder"> <img alt="background" src="img/hero-1.jpg"> </div>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-5">
                            <div class="map-container border--round" data-maps-api-key="AIzaSyCfo_V3gmpPm1WzJEC9p_sRbgvyVbiO83M" data-address="Waste Management Corporation Ltd., Malé, Male" data-marker-title="Waste Management Corporation Ltd., Malé, Male" data-map-style="[{&quot;featureType&quot;:&quot;landscape&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFBB00&quot;},{&quot;saturation&quot;:43.400000000000006},{&quot;lightness&quot;:37.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.highway&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FFC200&quot;},{&quot;saturation&quot;:-61.8},{&quot;lightness&quot;:45.599999999999994},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.arterial&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:51.19999999999999},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;road.local&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#FF0300&quot;},{&quot;saturation&quot;:-100},{&quot;lightness&quot;:52},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;water&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#0078FF&quot;},{&quot;saturation&quot;:-13.200000000000003},{&quot;lightness&quot;:2.4000000000000057},{&quot;gamma&quot;:1}]},{&quot;featureType&quot;:&quot;poi&quot;,&quot;stylers&quot;:[{&quot;hue&quot;:&quot;#00FF6A&quot;},{&quot;saturation&quot;:-1.0989010989011234},{&quot;lightness&quot;:11.200000000000017},{&quot;gamma&quot;:1}]}]" data-map-zoom="16" data-map-info-window="Something"></div>
                        </div>
                        <div class="col-sm-6 col-xs-12">
                            <div class="text-block">
                              @foreach($contact_page->contacts as $contact)

                              <h1>{{$contact->name}}</h1>
                              <p class="lead" >
                                  @if($contact->address){{$contact->address}}<br> @endif
                                  @if($contact->postal_code){{$contact->postal_code}}<br>@endif
                                  @if($contact->email){{$contact->email}}<br>@endif
                                  @if($contact->country){{$contact->country}}<br>@endif
                                  @if($contact->phone_no){{$contact->phone_no}}<br>@endif
                                  @if($contact->fax_no){{$contact->fax_no}}<br>@endif
                                  @if($contact->additional_info){{$contact->additional_info}}<br>@endif
                              </p>

                                @endforeach
                            </div>
                            <div class="row">
                                <form class="form-email" data-success="Thanks for your enquiry, we'll be in touch shortly." data-error="Please fill in all fields correctly.">
                                    <div class="col-sm-6 col-xs-12"> <label>Your Name:</label> <input type="text" name="Name" class="validate-required"> </div>
                                    <div class="col-sm-6 col-xs-12"> <label>Email Address:</label> <input type="email" name="Email" class="validate-required validate-email"> </div>
                                    <div class="col-sm-12 col-xs-12"> <label>Message:</label> <textarea rows="3" name="Message" class="validate-required"></textarea> </div>
                                    <div class="col-sm-5 col-md-4 col-xs-6"> <button type="submit" class="btn btn--primary type--uppercase">Send Enquiry</button> </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

@endsection
