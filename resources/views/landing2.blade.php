@extends('layouts.minimal')

@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                  <h1>WAMCO functions</h1>
                  <ul>
                    <li><ul><b><a href="{{ url('slideshows') }}"> SlideShow Functions </a></b>
                      <li>Create SlideShow migration: slideshow-name , boolean-> set as frontpage slider</li>
                      <li>Create Slideshow Details migration : Slide Image, Slide Title, Slide Description, Order, slideshow-name, belongsTo ( slideshow )
                    </ul></li>
                    <li><a href="{{ url('pages') }}" > Pages </a> </li>
                    <li><ul><b><a href="{{ url('services') }}">Services</a></b>
                      <li>Create Services migration: services-page-title ,service-page-description</li>
                      <li>Create Service Details migration : service-image, service-title, service-description, belongsTo ( services )</li>
                    </ul></li>

                    <li><ul><b><a href="{{ url('about') }}"> About </a></b>
                      <li>Create about migration: about-page-title ,about-page-description</li>
                      <li>Create about people details migration : people-image, people-designation, people-description , belongsTo ( about )</li>
                    </ul></li>

                    <li><ul><b><a href="{{ url('admin/posts') }}"> Blog</a></b>
                      <li>Create category migration: category-title ,category-description</li>
                      <li>Create article  migration : article-title, article-content,   belongsTo ( category ), belongsTo ( username )</li>
                      <li>Create comments   migration : commentor, boolean approved, comment-details, belongsTo ( article tab ) </li>

                    </ul></li>

                    <li><ul><b><a href="{{ url('resources') }}"> Resource </a></b>
                      <li>Create resource migration: resources-page-title ,resources-page-description</li>
                      <li>Create resource tab migration : tab-name, belongsTo ( resources )</li>
                      <li>Create resource tab details migration : title,description, link,  belongsTo ( resources tab )</li>

                    </ul></li>


                    <li><ul><b><a href="{{ url('contacts') }}"> Contact </a></b>
                      <li>Create contact migration: contact-page-title ,contact-page-description, contact-address, contact-no,  sendTo mail </li>
                    </ul></li>

                    <li><ul><b>Users</b>
                      <li>Create users migration: username, password, email, belongsTo ( usergroup ) </li>
                    </ul></li>



                  </ul>

                </div>
            </div>
        </div>
    </div>
@endsection
