<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Category</title>
    <!-- Main style -->
    <link rel="stylesheet" href="map/css/mapped-locations.css">

    <style media="screen">
      .headroom-sticky.sticky-scroll .navbar.navbar-default {
        background: #a3ccff;
      }
    </style>


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div id="rq-page-loading">
    <div class="rq-loading-icon"><i class="ion-load-c"></i></div>

</div>
<div id="main-wrapper">

    <div class="rq-page-content"> <!-- start page content -->
        <div class="container-fluid">
            <div class="rq-category-container">
                <div class="row">
                    <div class="col-md-8 col-md-push-4 rq-category-map-contains">
                        <div class="rq-contact-us-map">
                            <button class="rq-btn  show-result-btn">Show Result</button>
                            <header class="header">
                                <div class="headroom-sticky sticky-scroll">
                                    <nav class="navbar navbar-default">
                                        <div class="container-fluid">
                                            <!-- Brand and toggle get grouped for better mobile display -->
                                            <div class="navbar-header">
                                                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                                                        data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                                    <span class="sr-only">Toggle navigation</span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                    <span class="icon-bar"></span>
                                                </button>
                                                <a href="{{ url('/')}}" style="color:#7eced2; font-weight:900;font-size:20px; margin:20px;">
                                                  WAMCO
                                               </a>
                                                <!-- <a class="navbar-brand" href="/"><img src="logo/logo.png" alt="" style="max-height: 100px; max-width: 100px"/></a> -->
                                            </div>

                                            <!-- Collect the nav links, forms, and other content for toggling -->
                                            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                                                <ul class="nav navbar-nav navbar-right">
                                                    <li class=""><a href="{{url('/')}}#">HOME</a></li>
                                                    <li class=""><a href="{{url('/')}}#services">SERVICES</a></li>
                                                    <li class=""><a href="{{url('/')}}#about">ABOUT US</a></li>
                                                    <li class=""><a href="{{url('/')}}#resources">RESOURCES</a></li>
                                                    <li class=""><a href="{{url('/')}}#contact">CONTACT US</a></li>
                                                    <li class=""><a href="{{ url('/blog')}}">BLOG</a></li>
                                                    <li class=""><a href="{{url('/wizard')}}">WASTE WIZARD</a></li>
                                                    <li class=""><a href="{{url('/collection-date')}}">COLLECTION DATES</a></li>
                                                    <li class=""><a href="{{ url('/shop')}}">SHOP</a></li>
                                                </ul>
                                            </div><!-- /.navbar-collapse -->
                                        </div><!-- /.container-fluid -->
                                    </nav>
                                </div>
                            </header> <!-- end header -->

                            <div id="map"></div>
                        </div>
                    </div>
                    <div class="col-md-4 rq-category-sidebar-bg">
                        <div class="rq-category-sidebar">
                            <div class="rq-category-sidebar-top clearfix">

                                Mapped Location


                            </div>

                            <div class="rq-category-result" id="rq-category-result">
                                <p class="rq-category-results">{{ count($locations) }} Location found!</p>
                            @foreach($locations as $index => $location)
                                    <div class="rq-category-result-items">
                                    <a  class="wow fadeIn">
                                        <div class="rq-category-result-single wow fadeIn">
                                                    <div class="rq-result-img">
                                                        @if ($location->image)
                                                            <input type="hidden" id="imgPath" value="{{ $location->image->path }}">
                                                            <img src="{{ starts_with($location->image->path, ['http://', 'https://']) ? '' : '' }}{{ $location->image->path }}" alt="" >@endif
                                                    </div>

                                                <div class="rq-result-text">
                                                    <input type="hidden" id="name" value="{{ $location->name }}">
                                                    <input type="hidden" id="address" value="{{ $location->address }}">
                                                    <h3>{{ $location->address }}</h3>
                                                    <p class="rq-listing-item-address">{{ $location->description }}</p>
                                                    <hr>
                                                    <input type="hidden" id="lati" value="{{ $location->latitude }}">
                                                    <input type="hidden" id="longi" value="{{ $location->longitude }}">
                                                    <button type="button"  id="checkpoint" class="rq-btn rq-btn-primary btn-mini border-radius " onclick="getDirection({!! $location->latitude !!},{!! $location->longitude !!} )">locate</button>

                                            </div>


                                        </div>

                                    </a>

                                </div>
                                @endforeach
                                {{--<div class="rq-center-text rq-category-load-more wow fadeIn">--}}
                                    {{--<a href="#">Load more listing</a>--}}
                                {{--</div>--}}
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </div> <!-- end /.container-fluid -->
    </div> <!-- /.page-content -->
</div> <!-- end #main-wrapper -->

<footer class="rq-footer">

</footer>

<script src="map/js/jquery.min.js" type="text/javascript"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyBF0FPDHlurGkDKua7PfZjpD2fr2rQsRw0&libraries=places"></script>
<script src="map/js/bootstrap.min.js" type="text/javascript"></script>
<script src="map/js/masonry.pkgd.min.js" type="text/javascript"></script>
<script src="map/js/selectize.min.js"></script>
<script src="map/js/jquery.sticky.js" type="text/javascript"></script>
<script src="map/js/jquery-ui.min.js" type="text/javascript"></script>
<script src="map/js/wow.min.js"></script>
<script src="map/js/scripts.js" type="text/javascript"></script>
<script type="text/javascript">
    window.onload = function() {
      $.getScript("map/js/map.js", function(){
        getDirection(lati=4.1746503,longi=73.5099285);
      });

    };

    var locationObj = <?php echo json_encode($locations); ?>;
    var info = new Array();
    var locations = new Array();

    $.getScript("map/js/map.js", function(){});


</script>
</body>
</html>
