@extends('layouts.shop')
@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li class="active">Shop</li>
</ol>


@include('partials.alerts')


@if( isset($title))
<h1>{{$title}}</h1>

@else
<h1>Products</h1>
@endif

@if($products!= null)

<div class="row">

  @foreach($products as $product)


    <div class="col-sm-2 col-lg-2 col-md-2">
        <div class="thumbnail">
        @if(count($product->product_images) > 1 )
          <div id="myCarousel-{{$product->name}}". class="carousel slide" data-ride="carousel">
            <!-- Indicators -->


            <ol class="carousel-indicators">
              @foreach($product->product_images as $key => $product_image)

              <li data-target="#myCarousel-{{$product->name}}" data-slide-to="{{$key}}"></li>


              @endforeach
            </ol>


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="{{url('/').$product->product_images[0]->image->path}}" width="420" height="150">
              </div>
              <?php $product->product_images->shift(); ?>

              @foreach($product->product_images as $product_image)

              <div class="item">
                <img src="{{url('/').$product_image->image->path}}" width="420" height="150">
              </div>

              @endforeach

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel-{{$product->name}}" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel-{{$product->name}}" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


          @else
          <img src="{{url('/').$product->product_images[0]->image->path}}" width="420" height="150">

          @endif
            <div class="caption">
                <h4 class="pull-right">MVR: {{$product->price}}</h4>

                <h4><a href="{{route('shop-product',$product->id)}}">{{$product->name}}</a>
                </h4>

                <p>{{$product->description}}</p> <br>


            </div>
            <div class="ratings caption">

              @if(isset($product->category))
              <p>
                <b>{{$product->category->title}}</b>
              </p>
              @endif



                {!! Form::open(['action' => ['CartController@add', $product->id ],  'method' => 'patch' ,'style' => 'display:inline;']) !!}

                {{ csrf_field()}}
                <button class="btn btn-default btn-block" type="submit"> Add To Cart </button>

                {{ Form::close( )}}

{{$product->updated_at->diffForHumans()}}

            </div>
        </div>
    </div>
    @endforeach


</div>
  @endif

  {{ $products->links() }}



@endsection
