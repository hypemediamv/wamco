@extends('layouts.shop')
@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li class="active">Cart</li>
</ol>


<h1> Order Details </h1>
@if($user->cart != '[]')

  <table class="table">
    <thead>
    <th class="col-xs-8">Item</th><th class="col-xs-1">Quantity</th> <th class="col-xs-1" >Cost</th>
    </thead>
    <tbody>
    @foreach($user->cart->cart_products as $product)
      <tr>

        <td > {{$product->product->name}}  </td>
        <td>{{$product->quantity}}</td>
        <td>{{ $product->product->price * $product->quantity}}</td>

      </tr>
    @endforeach

    </tbody>
  </table>
@else
<div class="alert alert-warning">

Your Order is empty
</div>

@endif

<div class="row">
  <div class="col-md-10"></div>
  <div class="col-md-2">
<h4> Total Cost :{{ $user->cart->total }}</h4>
</div></div>

<div class="well">
  <div class="form-group">
<label for="usr"> Name:</label>
<input type="text" class="form-control" id="usr">
</div>
<div class="form-group">
<label for="pwd"> Email:</label>
<input type="text" class="form-control" id="pwd">
</div>

<div class="form-group">
<label for="pwd">Phone Number:</label>
<textarea class="form-control"></textarea>
</div>

    <button class="btn btn-success" type="submit"> Request Order </button>
  </div>

@endsection
