@extends('layouts.front2')

@section('content')
<a id="start"></a>
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>Cart Overview</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/shop">Shop</a>
                        </li>
                        <li>Cart Overview</li>
                    </ol>
                    <hr>
                    @if (session()->has('success_message'))
                    <div class="notification pos-right pos-top col-sm-4 col-md-3" data-animation="from-top" data-autoshow="200"  data-autohide="3000">
                        <div class="boxed boxed--border border--round box-shadow">
                            <div class="text-block">
                                <h5>Cart items</h5>
                                <p>
                                    {{ session()->get('success_message') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif

                    @if (session()->has('error_message'))
                    <div class="notification pos-right pos-top col-sm-4 col-md-3" data-animation="from-top" data-autoshow="200" data-autohide="3000">
                        <div class="boxed boxed--border border--round box-shadow">
                            <div class="text-block alert alert-danger">
                                <h5>Cart item quantity error</h5>
                                <p>
                                  {{ session()->get('error_message') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif

                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>


    <section>
        <div class="container">
           @if(sizeof(Cart::content()) == 0)
           <span class="lead"> The cart is empty!</span>
           <span class="" style="margin-top: 15px">
                <a class="btn btn--lg btn--primary" href="{{ action('ShopController@index') }}" style="text-decoration:none">
                    <span class="btn__text">Continue Shopping</span>
                </a>
           </span>
           @else
            <!-- <form class="cart-form"> -->
                <div class="row">
                  @foreach(Cart::content() as $item)
                    <div class="col-sm-4">
                        <div class="product-1">
                            <div class="product__controls">
                                <div class="col-xs-3">
                                    <label>Quantity:</label>
                                </div>
                                <div class="col-xs-6">
                                  <div class="input-select">
                                    <select class="quantity" data-id="{{ $item->rowId }}">
                                        <option {{ $item->qty == 1 ? 'selected' : '' }}>1</option>
                                        <option {{ $item->qty == 2 ? 'selected' : '' }}>2</option>
                                        <option {{ $item->qty == 3 ? 'selected' : '' }}>3</option>
                                        <option {{ $item->qty == 4 ? 'selected' : '' }}>4</option>
                                        <option {{ $item->qty == 5 ? 'selected' : '' }}>5</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-xs-3 text-right">
                                    <form action="{{ url('shop/cart', [$item->rowId]) }}" method="POST" class="">
                                        {!! csrf_field() !!}
                                        <input type="hidden" name="_method" value="DELETE">
                                        <button class="checkmark checkmark--cross bg--error" value="X"> </button>
                                    </form>

                                </div>
                            </div>
                            <img alt="Image" src="{{ $item->options->has('image') ? $item->options->image : ''  }}" />
                            <div>
                                <h5>{{ $item->name }}</h5>
                                <span> {{ $item->options->has('description') ? $item->options->description : '' }}</span>
                            </div>
                            <div>
                                <span class="h4 inline-block">MVR {{ $item->price }}</span>
                            </div>
                        </div>
                    </div>
                    <!--end item-->
                    @endforeach
                </div>
                <!--end of row-->
                <!-- <div class="row">
                    <div class="col-md-2 col-md-offset-10 text-right text-center-xs">
                        <button type="submit" class="btn">Update Cart</button>
                    </div>
                </div> -->
                <!--end of row-->
                <div class="row mt--2">
                    <div class="col-sm-12">
                        <div class="boxed boxed--border cart-total">
                            <div>
                                <div class="col-xs-6">
                                    <span class="h5">Cart Subtotal:</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <span>MVR {{ Cart::subtotal() }}</span>
                                </div>
                            </div>
                            <hr />
                            <div>
                                <div class="col-xs-6">
                                    <span class="h5">Total:</span>
                                </div>
                                <div class="col-xs-6 text-right">
                                    <span class="h5">MVR {{  Cart::total() }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--end of row-->
                <div class="row">
                    <div class="col-md-2 col-md-offset-10 text-right text-center-xs">
                      <form action="{{ action('CartController@order') }}" method="post" class="">
                        {!! csrf_field() !!}
                        <button type="submit" class="btn btn--primary">Proceed Checkout &raquo;</button>
                      </form>
                    </div>
                </div>
                <!--end of row-->
            <!-- </form> -->
            <!--end cart form-->
            @endif
        </div>
        <!--end of container-->
    </section>
    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        <i class="stack-interface stack-up-open-big"></i>
    </a>

@endsection

@section('footer-scripts')
<script>
    (function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('.quantity').on('change', function() {
            var id = $(this).attr('data-id');
            $.ajax({
              type: "PATCH",
              url: '{{ url("/shop/cart") }}' + '/' + id,
              data: {
                'quantity': this.value,
              },
              success: function(data) {
                console.log(data);
                window.location.href = '{{ url('/shop/cart') }}';
              }
            });
        });
    })();
</script>
@endsection
