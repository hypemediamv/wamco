@extends('layouts.front2')

@section('content')
<a id="start"></a>
<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <h1>Checkout</h1>
                <ol class="breadcrumbs">
                    <li>
                        <a href="#">Home</a>
                    </li>
                    <li>
                        <a href="#">Shop</a>
                    </li>
                    <li>Checkout</li>
                </ol>
                <hr>
            </div>
        </div>
        <!--end of row-->
    </div>
    <!--end of container-->
</section>
<section>
    <div class="container">
      @if(sizeof(Cart::content()) == 0)
      <span class="lead"> The cart is empty!</span>
      <span class="" style="margin-top: 15px">
           <a class="btn btn--lg btn--primary" href="{{ action('ShopController@index') }}" style="text-decoration:none">
               <span class="btn__text">Continue Shopping</span>
           </a>
      </span>
      @else
        <!-- <form class="cart-form"> -->
            <div class="row">
              @foreach(Cart::content() as $item)
                <div class="col-sm-4">
                    <div class="product">
                        <span class="label">QTY: {{ $item->qty }}</span>
                        <img alt="Image" src="{{ $item->options->has('image') ? $item->options->image : ''  }}" />
                        <div>
                            <h5>{{ $item->name }}</h5>
                        </div>
                        <div>
                            <span class="h4 inline-block">MVR {{ $item->price }}</span>
                        </div>
                    </div>
                </div>
                <!--end item-->
                @endforeach
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-2 col-md-offset-10 text-right text-center-xs">
                    <a href="{{ url ('/shop/cart')}}">
                      <button type="submit" class="btn">Revise Cart</button>
                    </a>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-sm-12 col-md-8 col-md-offset-2 cart-customer-details">
                    <h4>Billing Details</h4>
                    <p>Already a customer?
                        <a href="#">Log in here</a>
                    </p>
                    <div class="row">
                        <div class="col-sm-6">
                            <label>First Name:</label>
                            <input type="text" name="firstname" />
                        </div>
                        <div class="col-sm-6">
                            <label>Last Name:</label>
                            <input type="text" name="lastname" />
                        </div>
                        <div class="col-sm-6">
                            <label>Company Name:</label>
                            <input type="text" name="company" placeholder="Company name / self employeed"/>
                        </div>
                        <div class="col-sm-6">
                            <label>Email Address:</label>
                            <input type="email" name="email" />
                        </div>
                        <div class="col-sm-6">
                            <label>Country:</label>
                            <input type="country" name="country" value="Maldives" disabled="disabled" readonly/>
                        </div>
                        <div class="col-sm-6">
                            <label>State:</label>
                            <div class="input-select">
                                <select name="state">
                                    <option selected="" value="Default">Select State</option>
                                    <option value="1">Male' city</option>
                                    <option value="2">Hulhumale' city</option>
                                    <option value="3">Viligilli</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-12">
                            <label>Home Address:</label>
                            <input type="text" name="address" placeholder="Apartment / Suite No / House Name" />
                            <input type="text" name="street" placeholder="Street Address" />
                        </div>
                        <div class="col-sm-12">
                            <div class="input-checkbox">
                                <div class="inner"></div>
                                <input type="checkbox" name="account" />
                            </div>
                            <span>
                                <a href="#">Create an account</a>
                            </span>
                        </div>
                        <div class="col-sm-12">
                            <label>Additional instructions (optional):</label>
                            <textarea rows="4" name="instructions"></textarea>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
            </div>
            <!-- <div class="row">
                <div class="col-md-10 col-sm-3 text-right text-center-xs">
                  <div class="input-checkbox input-checkbox--switch">
                  	<div class="inner"></div>
                  	<input type="checkbox" name="agree" />
                  </div>
                  <span>I have read and agree to the <a href="">terms and conditions</a> </span>
                </div>
            </div> -->


            <!--end of row-->
            <div class="row mt--2">
                <div class="col-sm-12">
                    <div class="boxed boxed--border cart-total">
                        <div>
                            <div class="col-xs-6">
                                <span class="h5">Cart Subtotal:</span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <span>MVR {{ Cart::subtotal() }}</span>
                            </div>
                        </div>
                        <hr />
                        <div>
                            <div class="col-xs-6">
                                <span class="h5">Total:</span>
                            </div>
                            <div class="col-xs-6 text-right">
                                <span class="h5">MVR {{  Cart::total() }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--end of row-->
            <div class="row">
                <div class="col-md-4 col-md-offset-8 col-sm-3 col-sm-offset-9 text-right text-center-xs">
                  <div class="input-checkbox input-checkbox--switch">
                  	<div class="inner"></div>
                  	<input type="checkbox" name="agree" />
                  </div>
                  <span>I have read and agree to the <a href="">terms and conditions</a> </span>
                  <button type="submit" class="btn btn--primary type--uppercase">Checkout</button>
                </div>
            </div>
            <!--end of row-->
        <!-- </form> -->
        <!--end checkout form-->
        @endif
    </div>
    <!--end of container-->
</section>
<a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
    <i class="stack-interface stack-up-open-big"></i>
</a>
@endsection
