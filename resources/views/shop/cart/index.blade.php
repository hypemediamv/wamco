@extends('layouts.shop')
@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/')}}">Home</a></li>
  <li class="active">Cart</li>
</ol>


<h1> Your Cart Details </h1>
@if($user->cart != '[]')
@if($user->cart_products!= null)

  <table class="table">
    <thead>
    <th class="col-xs-8">Item</th> <th class="col-xs-1" > Cost Per Item </th> <th class="col-xs-1">Quantity</th> <th class="col-xs-1" >Total Cost</th><th> Function </th>
    </thead>
    <tbody>

    @foreach($user->cart->cart_products as $product)
      <tr>
        {!! Form::open(['action' => ['CartController@update', $product->id ],  'method' => 'patch' ,'style' => 'display:inline;']) !!}

        <td > {{$product->product->name}}  </td>
        <td>{{$product->product->price}} </td>
        <td><input type="number" name="quantity" class="form-control" value="{{$product->quantity}}"></td>
        <td>{{ $product->product->price * $product->quantity}}</td>



        <td>
          {{ csrf_field()}}

          <a href="#"> <button  name="update" type="submit" class="btn btn-success " style="display:inline-block;" >Update</button> </a>
          <a href="#"> <button  name="delete" type="submit" class="btn btn-danger " style="display:inline-block;" >Remove</button> </a>


        </td>
        {{Form::close()}}

      </tr>
    @endforeach

    </tbody>
  </table>

  @endif

@else
<div class="alert alert-warning">

Your Cart is empty
</div>

@endif

@if($user->cart!= null)
<div class="row well">
  @if($user->cart->total > 0)

  <div class="col-md-8"></div>
  <div class="col-md-2">
    <h4> Total Cost :{{ $user->cart->total }}</h4>
  </div>
  <div class="col-md-2">
    <a href="{{ action('CartController@order')}}"><button type="button" class="btn btn-success btn-block"> Order </button></a>
  </div>


  @else
    Cart is empty!
  @endif
</div>

@endif

@endsection
