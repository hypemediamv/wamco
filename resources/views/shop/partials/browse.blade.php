<div class="well well-sm">
  <h4>Browse</h4>
  <div class="row">

<div class="col-lg-6">
  <h5>Search</h5>

<div class="input-group">
      <input type="text" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">Go!</button>
      </span>
    </div>
</div>
<div class="col-lg-6">

<h5>By Category</h5>

<div class="btn-group" role="group" aria-label="...">
<a href="{{url('/shop')}}">   <button type="button" class="btn btn-default">  All </button> </a>

@foreach($categories as $category)
 <a href="{{route('shop-category',$category->id)}}"> <button type="button" class="btn btn-default">   {{$category->title}}</button></a>
@endforeach
</div>
</div>
</div>

</div>
