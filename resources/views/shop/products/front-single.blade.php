@extends('layouts.front2')

@section('content')
    <a id="start"></a>
    @foreach($product as $item)
        <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h1>{{ $item->name }}</h1>
                    <ol class="breadcrumbs">
                        <li>
                            <a href="/">Home</a>
                        </li>
                        <li>
                            <a href="/shop">Shop</a>
                        </li>
                        <li>{{ $item->name }}</li>
                    </ol>
                    <hr>
                    @if (session()->has('error_message'))
                    <div class="notification pos-right pos-top col-sm-4 col-md-3" data-animation="from-top" data-autoshow="200" data-autohide="3000">
                        <div class="boxed boxed--border border--round box-shadow">
                            <div class="text-block alert alert-danger">
                                <h5>Quantity Error</h5>
                                <p>
                                  {{ session()->get('error_message') }}
                                </p>
                            </div>
                        </div>
                    </div>
                    @endif
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>

    <section class="space--lg">
        <div class="container">
            <div class="row">
                    <div class="col-sm-7 col-md-6">
                        <div class="slider border--round boxed--border" data-paging="true" data-arrows="true">
                            <ul class="slides">
                                @foreach($item->product_images as $product_image)
                                <li>
                                    <img alt="Image" src="{{ $product_image->image->path }}" />
                                </li>
                                @endforeach
                            </ul>
                        </div>
                        <!--end slider-->
                    </div>
                    <div class="col-sm-5 col-md-4 col-md-offset-1">
                        <h2>{{ $item->name }}</h2>
                        <div class="text-block">
                            <span class="h4 inline-block">MVR {{ $item->price }}</span>
                        </div>
                        <p>
                            {!! $item->description !!}
                        </p>
                        <ul class="accordion accordion-2 accordion--oneopen">
                            <li class="active">
                                <div class="accordion__title">
                                    <span class="h5">Shipping Info</span>
                                </div>
                                <div class="accordion__content">
                                    <p>
                                        Generally ships between 2 - 4 working days from order confirmation. Orders only confirmed once payment has successfully processed. Payments can take longer to process (approx. 4 days).
                                    </p>
                                </div>
                            </li>
                        </ul>
                        <!--end accordion-->
                        {!! Form::open(['action' => 'CartController@store',  'method' => 'post']) !!}
                        {{--<form action="{{ 'CartController@add', $item->id  }}" method="patch">--}}
                            {{ csrf_field()}}
                            <input type="hidden" name="id" value="{{ $item->id }}">
                            <div class="col-sm-8 col-md-6 input-number">
                            <input type="number" name="quantity" min="1" max="5" value="1" placeholder="QTY" />
                          	<div class="input-number__controls">
                          		<span class="input-number__increase"><i class="stack-up-open"></i></span>
                          		<span class="input-number__decrease"><i class="stack-down-open"></i></span>
                          	</div>
                            </div>

                            <div class="col-sm-4 col-md-6">
                                <button type="submit" class="btn btn--primary">Add To Cart</button>
                            </div>
                        {{--</form>--}}
                        {!! Form::close() !!}
                    </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
    @endforeach

    <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
        <i class="stack-interface stack-up-open-big"></i>
    </a>
@endsection
