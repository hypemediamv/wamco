@extends('layouts.shop')
@section('content')
<style>

.carousel-inner {
  position: relative;
  width: 100%;
  min-height: 300px;
  }

 .carousel-control.right {
  right: 0;
  left: auto;
  background-image: none !important;
  background-repeat: repeat-x;
}
 .carousel-control.left {
  left: 0;
  right: auto;
  background-image: none !important;
  background-repeat: repeat-x;
}
#carousel-example-generic {
    margin: 20px auto;
    width: 100%;
}

#carousel-custom {
    margin: 20px auto;
    width: 400px;
}
#carousel-custom .carousel-indicators {
    margin: 10px 0 0;
    overflow: auto;
    position: static;
    text-align: left;
    white-space: nowrap;
    width: 100%;
    overflow:hidden;
}
#carousel-custom .carousel-indicators li {
    background-color: transparent;
    -webkit-border-radius: 0;
    border-radius: 0;
    display: inline-block;
    height: auto;
    margin: 0 !important;
    width: auto;
}
#carousel-custom .carousel-indicators li img {
    display: block;
    opacity: 0.5;
}
#carousel-custom .carousel-indicators li.active img {
    opacity: 1;
}
#carousel-custom .carousel-indicators li:hover img {
    opacity: 0.75;
}
#carousel-custom .carousel-outer {
    position: relative;
}
.carousel-indicators li img {
  width: 52px;}

  </style>
<ol class="breadcrumb">

  <li><a href="{{url('/')}}">Home</a></li>
  <li><a href="{{url('/shop')}}">Shop</a></li>
  <li class="active"> {{$product->name}}</li>
</ol>




<div class ="row">
<div class="col-md-4">

               <div class="thumbnail-class-removed">
                 <div class="container-fluid">

                 <h2>{{$product->name}} | <b> MVR {{$product->price}} </b> </h2>

                 <b>  {{$product->category->title}}</b><br><br>
                 <p>
                   {{$product->description}}
                 </p>

               </div>
                 <div id='carousel-custom' class='carousel slide' data-ride='carousel'>
                     <div class='carousel-outer'>
                         <!-- me art lab slider -->
                         <div class='carousel-inner '>
                             <div class='item active'>
                               <img src="{{url('/').$product->product_images[0]->image->path}}" width="600" height="150">
                             </div>

                             @foreach($product->product_images as $product_image)
                             <div class="item">
                               <img src="{{url('/').$product_image->image->path}}" width="600" height="150">
                             </div>

                             @endforeach
                             <script>
                   $("#zoom_05").elevateZoom({ zoomType    : "inner", cursor: "crosshair" });
                 </script>
                         </div>

                         <!-- sag sol -->
                         <a class='left carousel-control' href='#carousel-custom' data-slide='prev'>
                             <span class='glyphicon glyphicon-chevron-left'></span>
                         </a>
                         <a class='right carousel-control' href='#carousel-custom' data-slide='next'>
                             <span class='glyphicon glyphicon-chevron-right'></span>
                         </a>
                     </div>

                     <!-- thumb -->
                     <ol class='carousel-indicators mCustomScrollbar meartlab'>
                         <li data-target='#carousel-custom' data-slide-to='0' class='active'> <img src="{{url('/').$product->product_images[0]->image->path}}" width="100" >
                 </li>
                 <?php $product->product_images->shift(); ?>

                         @foreach($product->product_images as $product_image)
                         <li data-target='#carousel-custom' data-slide-to='1'><img src="{{url('/').$product_image->image->path}}" width="100" ></li>

                         @endforeach

                     </ol>
                 </div>
                 {!! Form::open(['action' => ['CartController@add', $product->id ],  'method' => 'patch' ,'style' => 'display:inline;']) !!}

                 {{ csrf_field()}}
                 <button class="btn btn-success" type="submit"> Add To Cart </button>

                 {{ Form::close( )}}

 {{$product->updated_at->diffForHumans()}}



               </div>
</div>

</div>



@endsection
