@extends('layouts.front2')

@section('content')
    <a id="start"></a>
              
    <section class="text-center imagebg" data-overlay="4">
        <div class="background-image-holder"> <img alt="background" src="{{url('/')}}/img/landing-5.jpg"> </div>
        <div class="container">
            <div class="row">
                <div class="col-sm-10 col-md-8">
                    <h1>Shop</h1>
                    <p class="lead"> Products available in our store </p>
                </div>
            </div>
        </div>
    </section>

    <!-- By Zone -->
    <section class="space--sm">

                <div class="container">
                  <span class="pull-right" style="margin-top: 35px">
                       <a class="btn btn--lg btn--primary" href="{{ action('CartController@index') }}" style="text-decoration:none;">
                           <span class="btn__text">view cart</span>
                       </a>
                  </span>

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="masonry masonry--tiles">
                                <div class="masonry-filter-container">
                                    <span>By Zone:</span>
                                    <div class="masonry-filter-holder">
                                        <div class="masonry__filters" data-filter-all-text="All Zones">

                                        </div>
                                    </div>


                                </div>
                                <div class="row">
                                    <div class="masonry__container">
                                        {{--<div class="masonry__item col-md-3 col-sm-6">Zone intro text</div>--}}

                                        {{--{{ dd(($shop_zones)) }}--}}
                                    @foreach($shop_zones as $shop_zone)
                                        @if(count($shop_zone->products) > 0)
                                            @foreach($shop_zone->products as $product)
                                                <div class="masonry__item col-md-3 col-sm-6" data-masonry-filter="{{$shop_zone->name}}">
                                                    <div class="product product--tile bg--secondary text-center">
                                                        <a href="#">
                                                             <img alt="Image" src="{{$product->product_images[0]->image->path}}" />
                                                        </a>
                                                        <a class="block" href="#">
                                                            <div>
                                                                <h5>{{$product->name}}</h5>
                                                                <!-- <span> 18MP DSLR Camera</span> -->
                                                            </div>
                                                            <div>
                                                                <span class="h4 inline-block">MVR {{$product->price}}</span>
                                                            </div>
                                                            <div>
                                                                <a class="btn" href="{{ action('ShopController@show', [$product->slug]) }}">
                                                                    <span class="btn__text">view item</span>
                                                                </a>
                                                            </div>
                                                        </a>
                                                    </div>
                                                </div>
                                                @endforeach
                                            @endif
                                        @endforeach
                                        <!--end item-->
                                    </div>
                                    <!--end masonry container-->
                                </div>
                                <!--end of row-->
                            </div>
                            <!--end masonry-->
                        </div>
                    </div>
                    <!--end of row-->


                </div>
                <!--end of container-->
    </section>
    <!-- By Zone -->
    <section class="space--sm">
        <div class="container">
            <!-- By category -->
            <div class="row">
                <div class="col-sm-12">
                    <div class="masonry masonry--tiles">
                        <div class="masonry-filter-container">
                            <span>By Category:</span>
                            <div class="masonry-filter-holder">
                                <div class="masonry__filters" data-filter-all-text="All Categories"></div>
                            </div>

                        </div>
                        <div class="row">
                            <div class="masonry__container">
                                {{--<div class="masonry__item col-md-3 col-sm-6">Category intro text</div>--}}
                                @foreach($categories as $category)
                                    @if(count($category->products) > 0)
                                        @foreach($category->products as $product)
                                            <div class="masonry__item col-md-3 col-sm-6" data-masonry-filter="{{$category->title}}">
                                                    <div class="product product--tile bg--secondary text-center">
                                                        <a href="#">
                                                            <img alt="Image" src="{{$product->product_images[0]->image->path}}" />
                                                        </a>
                                                        <a class="block" href="#">
                                                            <div>
                                                                <h5>{{$product->name}}</h5>
                                                                <!-- <span> 18MP DSLR Camera</span> -->
                                                            </div>
                                                            <div>
                                                                <span class="h4 inline-block">MVR {{$product->price}}</span>
                                                            </div>
                                                            <div>
                                                                <a class="btn" href="{{ action('ShopController@show', [$product->slug]) }}">
                                                                    <span class="btn__text">view item</span>
                                                                </a>
                                                            </div>
                                                        </a>
                                                    </div>
                                            </div>
                                        @endforeach
                                     @endif
                                @endforeach
                                <!--end item-->
                            </div>
                            <!--end masonry container-->
                        </div>
                        <!--end of row-->
                    </div>
                    <!--end masonry-->
                </div>
            </div>
            <!--end of row-->

        </div>
        <!--end of container-->
    </section>


        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
@endsection
