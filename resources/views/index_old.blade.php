<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>WAMCO : Waste Management Corporation</title>
	<meta name="Resource-type" content="Document" />
	<link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" media="all" >

	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/jquery.fullPage.css') }}" />
	<link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/scroll.css') }}" />

	<!-- Bootstrap -->

	<link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" media="all" >
	<!--[if IE]>
		<script type="text/javascript">
			 var console = { log: function() {} };
		</script>
	<![endif]-->

	<!-- Jquery -->
	<script type="text/javascript" src="{{ asset('plugins/jquery/js/jquery.min.js') }}"></script>
	<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
	<script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
	<!-- <script type="text/javascript" src="{{ asset('plugins/scrolling/js/scrolloverflow.js') }}"></script> -->
	<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

 <script type="text/javascript" src="{{ asset('plugins/scrolling/js/jquery.fullPage.js') }}"></script>
	<script type="text/javascript" src="{{ asset('plugins/scrolling/js/examples.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function() {

			$('ul.tabs li').click(function(){
		var tab_id = $(this).attr('data-tab');

		$('ul.tabs li').removeClass('current');
		$('.tab-content').removeClass('current');

		$(this).addClass('current');
		$("#"+tab_id).addClass('current');
	});

			var old_array = [];

			$( "li" ).each(function( index ) {
				old_array.push($(this).data('menuanchor') );
			  console.log( index + ": " + $( this ).data('menuanchor') );
			});

			//Sanitize the old_array to remove undefined elements
			var new_array = $.map(old_array, function (el) {
			    return el !== '' ? el : null;
			});

			console.log(new_array);

			var rgb = [];

			presets = ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'];


			for(var i = 0; i < new_array.length; i++){
       var random = presets[Math.floor(Math.random() * presets.length)];
				rgb.push(random);

			}





			$('#fullpage').fullpage({
				// sectionsColor: ['#1bbc9b', '#4BBFC3', '#7BAABE', 'whitesmoke', '#ccddff'],
				sectionsColor: ['white', 'whitesmoke', '#1bbc9b', 'whitesmoke', '#7BAABE'],
				//sectionsColor:rgb,
				anchors:new_array,
				menu: '#menu',
        slidesNavigation: true,
        // verticalCentered: false,
				afterLoad: function(anchorLink, index){

					//section 2
					if(index == 2){
						//moving the image
						// $('#section1').find('img').delay(500).animate({
						// 	left: '0%'
						// }, 1500, 'easeOutExpo');

						// $('#section1').find('img').first().fadeIn(1800, function(){
						// 	$('#section1').find('img').last().fadeIn(1800);
						// });;


						// $('#section1').find('p').first().fadeIn(1800, function(){
						// 	$('#section1').find('p').last().fadeIn(1800);
						// });;

					}

					//section 3
					if(anchorLink == '3rdPage'){
						//moving the image
						$('#section2').find('.intro').delay(500).animate({
							left: '0%'
						}, 1500, 'easeOutExpo');
					}
				}
			});

		});



	</script>

	<style>
    .section{
  		background-size: cover;
  	}
  	.slide{
  		background-size: cover;
  	}

		/*#section1 img{
			left: 130%;
			position:relative;
		}*/
		/*#section1 p{
			display:none;
		}*/
		#section2 .intro{
			left: -130%;
			position:relative;
		}
    /*Adding background for the slides
  	* --------------------------------------- */
  	/*#slide1{
  		background-image: url(images/bg4.jpg);
  		padding: 6% 0 0 0;
  	}
  	#slide2{
  		background-image: url(images/bg5.jpg);
  		padding: 6% 0 0 0;
  	}*/
		body{
			margin-top: 100px;
			font-family: 'Trebuchet MS', serif;
			line-height: 1.6
		}
		.container{
			width: 800px;
			margin: 0 auto;
		}



		ul.tabs{
			margin: 0px;
			padding: 0px;
			list-style: none;
		}
		ul.tabs li{
			background: none;
			color: #222;
			display: inline-block;
			padding: 10px 15px;
			cursor: pointer;
		}

		ul.tabs li.current{
			background: #ededed;
			color: #222;
		}

		.tab-content{
			display: none;
			background: #ededed;
			padding: 15px;
		}

		.tab-content.current{
			display: inherit;
		}

		.wamco_slide{
			padding-top:20%;
		}

		.wamco_slide h1{
			font-size: 90px;
			font-weight: 900;
			color:white;
			text-shadow: -1px 0 black, 0 3px black, 1px 0 black, 0 -1px black;
		}

		.wamco_slide h2{
			font-size: 40px;
			color:white;
			text-shadow: -1px 0 black, 0 3px black, 1px 0 black, 0 -1px black;


		}
	</style>

</head>
<body>
<!-- id menu came wtih navigation sidebar -->
	<nav class="navbar navbar-default navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#home">WAMCO</a>
			</div>
			<div id="navbar" class="navbar-collapse collapse">
				<ul class="nav navbar-nav">


				</ul>
				<ul  class="nav navbar-nav navbar-right" id="patas">
					<li data-menuanchor="home"><a href="#home"></i>Home</a></li>

					@foreach($pages as $page)
					<li data-menuanchor="{{$page->name}}"><a href="#{{$page->name}}">{{$page->title}}</a></li>

					@endforeach
					<li><a href="{{url('/blog')}}">Blog</a></li>
					<li><a href="{{url('/shop')}}">Shop</a></li>

					<li class="dropdown">
							<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
									{{ Auth::user()->name }} <span class="caret"></span>
							</a>

							<ul class="dropdown-menu" role="menu">
								<li class="Dashboard"><a href="/dashboard"></i>Dashboard</a></li>

									<li>
											<a href="{{ url('/logout') }}"
													onclick="event.preventDefault();
																	 document.getElementById('logout-form').submit();">
													Logout
											</a>

											<form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
													{{ csrf_field() }}
											</form>
									</li>
							</ul>
					</li>




				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>



<div id="fullpage">

	<div class="section " id="section0">

	@if($slideshow->slides != null)

	@foreach($slideshow->slides as $key => $slide)

  <div class="slide" id="slide{{$key}}" data-anchor="slide{{$key}}" style="background-image: url({{$slide->slide_image->image->path}});">
		<div class="wamco_slide">
			<h1>{{$slide->slide_name}}</h1>
			<h2>{{$slide->slide_description}}</h2>

	</div>


  </div>
		@endforeach


		@endif

	</div>


@foreach($pages as $key => $page)

	<div class="section" id="section">
		<div class="intro">
			<!--<img src="images/1.png" alt="Cool" />-->

			<h1>{{$page->title}} </h1>
			<p> {{$page->description}}</p>
			<br><br>
			<div class="container">
			<div class="row">

			@if($page->services != null)
			<br><br>
			@foreach($page->services as $service)
			<div class="col-md-4">
			<img src="{{url('/').$service->image->path}}" width="200px;" height="200px;">
			<h2>{{$service->name}}</h2>
			<p>{{$service->description}}</p>
			<a href="{{route('service-single',$service->id)}}"> Read More </a>
			</div>
			@endforeach
			@endif


			@if($page->staff != null)
			@foreach($page->staff as $staff)
			<div class="col-md-4">
			<img src="{{url('/').$staff->image->path}}" width="200px;" height="200px;">
			<h2>{{$staff->name}}</h2>
			<h3>{{$staff->designation}}</h3>
			<p>{{$staff->quote}}</p>
			</div>
			@endforeach
			@endif

			@if($page->resource_tabs != null)
     <div class="container" style="text-align:left;">

			@foreach($page->resource_tabs as $tab_key => $resource_tab)


		<ul class="tabs">
			<li class="tab-link current" data-tab="tab-{{$tab_key}}" >{{$resource_tab->name}}</li>
		</ul>
		<div id="tab-{{$tab_key}}" class="tab-content container current">

   @foreach($resource_tab->resources as $resource)
	 <div class="col-md-8">
		 <h3>{{$resource->name}}</h3>
		 <p>{{$resource->description}}</p>

	 </div>

	 <div class="col-md-4">
		 <br>
		 {!! Form::open(['action' => ['ResourceController@download', $resource->id]]) !!}
		 <button type="submit" class="btn btn-success btn-block"  style="display:inline-block;" >Download</button>
		 {{Form::close() }}

	 </div>

		@endforeach
	</div>

		@endforeach

	</div>


			@endif

			@if($page->contacts != null)
			@foreach($page->contacts as $contact)
			<div class="col-md-4" style="text-align:left">
			<h1>{{$contact->name}}</h1>
			<h3>{{$contact->address}}</h3>
			<p>{{$contact->phone_no}}</p>
			</div>

			<div class="col-md-8" style="text-align:left">

				<form>
			  <div class="form-group">
						<label for="name">Name:</label>
						<input type="text" class="form-control" id="pwd">
						<br>
			     <label for="email">Email address:</label>
			     <input type="email" class="form-control" id="email">
					 <br>
						<label for="subject">Subject:</label>
						<input type="text" class="form-control" id="subject">
						<br>

						<label for="message">Message:</label>
						<textarea rows="4" cols="50" class="form-control"  style=" max-width: 100%; max-height:100%;resize:none;"></textarea>						<br>

			  </div>


			  <button type="submit" class="btn btn-default">Submit</button>
			</form>


			</div>
			@endforeach
			@endif


			</div>
		</div>
		</div>
	</div>



@endforeach


</div>


</body>
</html>
