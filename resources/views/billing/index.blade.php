@extends('layouts.front2')

@section('content')
    <section>
        <div class="container">
            <h1>Bills</h1>

            <div class="row">
              
                <div class="col-md-12">
                  @if(count($billings) == null)
                  <p>User has no bills</p>
                  @else

                  <h2>Bills for {{$billings[0]->user->name}}</h2>


                    <table class="table">
                      <thead>
                      <th>Bill Service</th> <th>Bill Content</th> <th>Bill Date</th> <th>Bill User </th>
                      </thead>
                      <tbody>
                      @foreach($billings as $billing)
                        <tr>

                          <td>{{$billing->billingService->service_name}}</td>
                          <td >{!! $billing->content !!}


                            </td>
                          <td>
                            {{$billing->biling_date}}
                          </td>



                          <td>
                            {{$billing->user->first_name}}
                            {{$billing->user->last_name}}
                            {{$billing->user->name}}

                          </td>


                        </tr>
                      @endforeach
                      </tbody>
                    </table>
                  @endif

                </div>
            </div>
        </div>
    </section>

@endsection
