@extends('layouts.minimal')
@section('content')



  {{ Form::open(['action' => 'SlideshowController@store'])  }}
<!-- Slidehosw Creator-->
<div class="panel panel-primary" >
  <div class="panel-heading">Create SlideShow  </div>
  <div class="panel-body">

    <div class="form-group">
      <label for="slideshow_name">SlideShow Name:</label>
      <input type="text" name="slideshow_name"class="form-control" id="slideshow_name" placeholder="Enter Slide Name">
    </div>

    <div class="checkbox">
      <label><input type="checkbox" name="frontpage"> FrontPage</label>
    </div>
    <button type="submit" class="btn btn-default">Submit</button>
    <span class="alert alert-info" style="padding:8px;">
      <strong>Info!</strong> Create a slide show to add slides.
    </span>
  {{Form::close() }}
  </div>


</div>
<!-- Slidehosw Creator-->

<!-- Slidehosw Assignment-->
{{ Form::open(['action' => 'SlideshowController@assign'])  }}
<div class="panel panel-primary">
  <div class="panel-heading">Assign Slideshow  </div>
  <div class="panel-body">

  <div class="form-group">
    <label for="slideshow_name">Atoll:</label>
    {{ Form::select('slideshow_id',$slideshows->pluck('english_official_atoll_name','id'), null,['class' => "form-control"]) }}
  </div>

  <div class="checkbox form-group">
    <label><input type="checkbox" name="frontpage"> FrontPage</label>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>
  <span class="alert alert-info" style="padding:8px;">
    <strong>Info!</strong> Assigned a slide show from created slide shows.
  </span>
{{Form::close() }}

</div></div>
<!-- Slidehosw Assignment-->



<br>
<div class="panel panel-primary">
  <div class="panel-heading">Slideshows </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Slidehow Name</th><th>Front Page</th>
    </thead>
    <tbody>
      @if($slideshows!= null)
      @foreach($slideshows as $slideshow)
      <tr>
        <td><a href="{{ url('admin/slideshow/'.$slideshow->id ) }}"> {{$slideshow->slideshow_name}} </a></td>
        <td>{{$slideshow->frontpage}}</td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>


@endsection
