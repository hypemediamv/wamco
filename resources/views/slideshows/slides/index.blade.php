@extends('layouts.minimal')
@section('content')




  <h3><a href="/admin/slideshows"> {{$slideshow->slideshow_name}}  Slides </a></h3>
{{-- @include('slideshows.preview')  --}}

  <!-- Slide Creator-->

{{ Form::open(['action' => 'SlideController@store', 'files' => true])  }}
<input type="hidden" name="slideshow_id" value="{{$slideshow->id}}" >

<div class="panel panel-primary">
  <div class="panel-heading">Create Slide  </div>
  <div class="panel-body">

  <div class="form-group">
    <label for="sllide_name">Slide Name</label>
    <input type="text" name="slide_name"class="form-control" id="sllide_name" placeholder="Enter slide name">
  </div>



  <div class="form-group">
    <label for="slide_description">Slide Description:</label>
    <input type="text" name="slide_description"class="form-control" id="slide_description" placeholder="Enter slide description">
  </div>

  <div class="form-group @if ($errors->has('slide_image')) has-error @endif">
    <label for="slide_image">Slide Image</label><span class="label label-warning">Warning! Minimum Resolution Supported: 1920 x 1080</span>
    <input type="file" id="featuredImg" name="slide_image" required>
    @if ($errors->has('slide_image')) <p class="help-block">{{ $errors->first('slide_image') }}</p> @endif

  </div>


  <button type="submit" class="btn btn-default">Submit</button>
{{Form::close() }}

</div></div>
<!-- Slides Creator-->

<!--Existing Sllides in Slideshow-->
<div class="panel panel-primary">
  <div class="panel-heading">Slides </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Slide Name</th><th>Slide Description</th><th>Slide Image</th><th>Delete</th>
    </thead>
    <tbody>
      @if($slideshow->slides!= null)
      @foreach($slideshow->slides as $slide)
      <tr>
        <td> {{$slide->slide_name}} </td>
        <td>{{$slide->slide_description}}</td>
        <td> <img src="{{url('/').$slide->slide_image->image->path}}" width="100px;"></td>
        <td>
          {!! Form::open(['action' => ['SlideController@destroy', $slide->id],  'method' => 'delete']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
          {{Form::close() }}
        </td>
      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>





@endsection
