
  <!-- End SlidesJS Required -->

  <!-- SlidesJS Required: These styles are required if you'd like a responsive slideshow -->


  <!-- SlidesJS Required: Start Slides -->
  <!-- The container is used to define the width of the slideshow -->
  <div class="container">
    <div id="slides">
      @foreach($slideshow->slides as $slide)
      <img src="{{url('/').$slide->slide_image->image->path}}" height='40' weight='40'>
      @endforeach
    </div>
  </div>
  <!-- End SlidesJS Required: Start Slides -->

  <!-- SlidesJS Required: Link to jQuery -->
  <script>
      $(function() {
        $('#slides').slidesjs({
          width: 40,
          height: 40
        });
      });
    </script>
