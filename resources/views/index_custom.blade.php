@extends('layouts.front')

@section('content')
<div id="fullpage">

	<div class="section " id="section0">

		@if($slideshow->slides != null)

			@foreach($slideshow->slides as $key => $slide)


				<div class="slide" id="slide{{$key}}" data-anchor="slide{{$key}}" style="background-image: url( {{  url( $slide->slide_image->image->path ) }});">
					<div class="wamco_slide">
						<h1>{{$slide->slide_name}}</h1>
						<h2>{{$slide->slide_description}}</h2>

					</div>

				</div>
			@endforeach


		@endif

	</div>


	@foreach($pages as $key => $page)

		<div class="section" id="section" style="background-color:{{$page->background_color}};">
			<div class="intro">
				<!--<img src="images/1.png" alt="Cool" />-->
				<h2 class="wamco-headline">{{$page->title}} </h2>

				<p> {{$page->description}}</p>
				<br><br>
				<div class="container">
					<div class="row">

						@if($page->services != null)
							<br><br>
							@foreach($page->services as $service)
								<div class="col-md-4 feature">
									<img src="{{url('/').$service->image->path}}" width="200px;">
									<h3>{{$service->name}}</h3>
									<div class="title_border"></div>
									<p>{{$service->description}}</p>
									<a href="{{route('service-single',$service->id)}}">
										<button class="btn btn-success btn-block" type="button">   Read More </button>
									</a>
								</div>

							@endforeach
						@endif


						@if($page->staff != null)
							@foreach($page->staff as $staff)
								<div class="col-md-4">


									<div class="wamco-box">
										<div class="icon">
											<img class="image img-circle grayscale" src="{{url('/').$staff->image->path}}" width="140" height="140" >
											<div class="info">
												<h3 class="wamco-bold">{{$staff->name}}</h3>
												<h3>{{$staff->designation}}</h3>
												<p>{{$staff->quote}}</p>
											</div>
										</div>
										<div class="space"></div>
									</div>

								</div>

							@endforeach
							<br>
						@endif

						@if($page->resource_tabs != null)
							<div class="container" style="text-align:left;">



								@foreach($page->resource_tabs as $tab_key => $resource_tab)


									<ul class="tabs">
										<li class="tab-link current" data-tab="tab-{{$tab_key}}" >{{$resource_tab->name}}</li>
									</ul>

									<div id="tab-{{$tab_key}}" class="tab-content container current">

										@foreach($resource_tab->resources as $resource)
											<div class="col-md-8">
												<h3>{{$resource->name}}</h3>
												<p>{{$resource->description}}</p>

											</div>

											<div class="col-md-4">
												<br>
												{!! Form::open(['action' => ['ResourceController@download', $resource->id]]) !!}
												<button type="submit" class="btn btn-success btn-block"  style="display:inline-block;" >Download</button>
												{{Form::close() }}

											</div>
										@endforeach
									</div>
								@endforeach
							</div>
							
						@endif

						@if($page->contacts != null)
							@foreach($page->contacts as $contact)
								<div class="container">

									<div class="col-md-4" style="text-align:left">
										<h1>{{$contact->name}}</h1>
										<p>{{$contact->address}}</p>
										<p>{{$contact->postal_code}}</p>
										<p>{{$contact->email}}</p>
										<p>{{$contact->country}}</p>
										<p>{{$contact->phone_no}}</p>
										<p>{{$contact->fax_no}}</p>
										<p>{{$contact->additional_info}}</p>


									</div>

									<div class="col-md-8" style="text-align:left">

										<form>
											<div class="form-group">
												<label for="name">Name:</label>
												<input type="text" class="form-control" id="pwd">
												<br>
												<label for="email">Email address:</label>
												<input type="email" class="form-control" id="email">
												<br>
												<label for="subject">Subject:</label>
												<input type="text" class="form-control" id="subject">
												<br>

												<label for="message">Message:</label>
												<textarea rows="4" cols="50" class="form-control"  style=" max-width: 100%; max-height:100%;resize:none;"></textarea>						<br>

											</div>


											<button type="submit" class="btn btn-default">Submit</button>
										</form>


									</div>



								</div>
							@endforeach


						@endif


					</div>
				</div>
			</div>


		</div>



	@endforeach

</div>
@endsection
