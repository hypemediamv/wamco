@extends('layouts.minimal')

@section('content')
    <div class="container row">

      <h1>Welcome to Dashboard</h1>

        <div class="col-md-6">



                  <p><a href="{{ url('admin/slideshows') }}"><h3> Manage Slide Shows</h3></a>
                  Manage Slideshow,Add Slides,  Set Slideshow as Front Page,
                  </p>
                  <p><a href="{{ url('admin/pages') }}"><h3> Manage Pages</h3></a>
                    Manage Pages such as Service, Contact, etc here.

                  </p>
                <p><a href="{{ url('admin/posts') }}"><h3> Manage Blog</h3></a>
                  Blog's posts can be manipulated and configured here
                </p>
                  <p><a href="{{ url('admin/post/categories') }}"> <h3> Manage Blog Categories</h3></a>
                    Blog Categories can be added/removed here
                  </p>
                  <p><a href="{{ url('admin/services') }}"><h3> Manage Services</h3></a>
                    Servie Page and Service Descriptions can be managed here.
                  </p>
                  <p><a href="{{ url('admin/about') }}"><h3> Manage About</h3></a>
                    About Provides the functionality to add staff and staff quotes in front page.
                  </p>

                  <p><a href="{{ url('admin/resources') }}"><h3> Manage Resources</h3></a>
                    Add Resource Tabs and File Downloads here
                  </p>
                  <p><a href="{{ url('admin/contacts') }}"><h3> Manage Contacts</h3></a>
                    Add Contact Details Here
                  </p>

        </div>

            <div class="col-md-6">


                      <p><a href="{{ url('shop') }}"><h3> Manage Shop</h3></a>
                        Browse Products related to Shop
                      </p>

                      <p><a href="{{ url('admin/shop/products') }}"><h3> Manage Products</h3></a>
                      Manage shop related products
                      </p>


                      <p><a href="{{ url('admin/shop/categories') }}"><h3> Manage Shop Categories</h3></a>
                        Manage Shop Categories
                      </p>

                <p><a href="{{ url('admin/mapped-locations') }}"><h3> Manage Mapped Locations</h3></a>
                    Manage Mapped Locations
                </p>

                <p><a href="{{ url('admin/settings/social') }}"><h3> Manage Social Media Settings</h3></a>
                    Manage Social Media Settings
                </p>

            </div>
    </div>
@endsection
