@extends('layouts.minimal')
@section('content')

  <h3><a href="{{ url('admin/pages')}}">Page</a> > <a href="{{ url('admin/resources')}}" >Resource </a > >  Add Resource</h3>

<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Add Resource Tab </div>
  <div class="panel-body">

{{ Form::open(['action' => 'ResourceController@store_tab'])  }}

  <div class="form-group">
    <label for="page_name">Resource  Tab Name:</label>
    <input type="text" name="resource_tab_name" class="form-control" placeholder="Enter a resource tab name" required>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>
  <span class="alert alert-info" style="padding:8px;">
      <strong>Info!</strong> Create a resource tab name to add a resource.
  </span>

</div>
</div>

{{Form::close() }}

<!-- Add resource Tab-->

<div class="panel panel-primary">
  <div class="panel-heading">  Add Resource Details </div>
  <div class="panel-body">

{{ Form::open(['action' => 'ResourceController@store', 'files' => true])  }}


<div class="form-group">
  <label for="resource_tab">Resource Tab:</label>
  {{ Form::select('resource_tab_id', $resource_tabs->pluck('name','id') , null, array('class' => 'form-control' , 'required')) }}
</div>

<input type="hidden" name="page_id" value="{{$resource_page->id}}">
  <div class="form-group">
    <label for="page_name">Resource  Name:</label>
    <input type="text" name="resource_name"class="form-control" placeholder="Enter a resource name" required>
  </div>
  <div class="form-group">
    <label for="page_description">Resource Description:</label>
    <input type="text" name="resource_description"class="form-control" placeholder="Enter a resource description" required>
  </div>
  <div class="form-group">
    <label for="page_title">Resource Icon (optional):</label>
    <input type="file" name="resource_icon" id="featuredImg">
  </div>

  <div class="form-group">
    <label for="document">Document:</label><span class="label label-warning">Warning! Maximum File Size: 2MB</span>
    <input type="file" name="document" id="documentFile" required>
  </div>

  <button type="submit" class="btn btn-default">Submit</button>

</div>
</div>
{{Form::close() }}

<!-- End resource Tab-->


@if(count($resource_page->resources) > 0)
<div class="panel panel-primary">
  <div class="panel-heading">Resources </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Resource Name</th><th>Resource Description</th><th>Resource Image</th> <th>Resource Tab</th> <th>Action</th>
    </thead>
    <tbody>
      @if($resource_page->resources!= null)
      @foreach($resource_page->resources as $resource)
      <tr>
        <td> {{$resource->name}} {{$resource->id}} </td>
        <td>{{$resource->description}}</td>
        <td>  @if($resource->icon != null )<img src="{{url('/').$resource->icon->path}}" height="100"> @endif </td>
        <td>
          {{ $resource->resource_tab->name }}

        </td>
        <td>
          <a href="{{ action('ResourceController@edit',$resource->id) }}"> <button type="submit" class="btn btn-warning btn-block">Edit</button> </a><br>
          {!! Form::open(['action' => ['ResourceController@destroy', $resource->id],  'method' => 'DELETE']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
          {{Form::close() }}<br>
          <!-- {!! Form::open(['action' => ['FrontPageController@download', $resource->id]]) !!}
          <button type="submit" class="btn btn-success btn-block"  style="display:inline-block;" >Download</button>
          {{Form::close() }} -->
        </td>

      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>
@endif


@endsection
