@extends('layouts.minimal')
@section('content')

  <h3><a href="{{ url('admin/pages')}}">Page</a> > <a href="{{ url('admin/resources')}}" >Resource </a > >  Edit Resource</h3>
<br>


<div class="panel panel-primary">
  <div class="panel-heading">  Edit Resource Details </div>
  <div class="panel-body">



    {{ Form::open(['action' => ['ResourceController@update', $resource->id], 'files' => true, 'method' => 'patch'])  }}
    {{ csrf_field()  }}

    <div class="form-group">
  <label for="resource_tab">Resource Tab:</label>
  {{ Form::select('resource_tab_id', $resource_tabs->pluck('name','id') , $resource->resource_tab_id, array('class' => 'form-control' , 'required')) }}
</div>

  <div class="form-group">
    <label for="page_name">Resource  Name:</label>
    <input type="text" name="resource_name" value="{{ $resource->name }}" class="form-control" placeholder="Enter a resource name" required>
  </div>
  <div class="form-group">
    <label for="page_description">Resource Description:</label>
    <input type="text" name="resource_description" value="{{ $resource->description }}" class="form-control" placeholder="Enter a resource description" required>
  </div>


  <button type="submit" class="btn btn-default">Submit</button>

</div>
</div>
{{Form::close() }}


@endsection
