@extends('layouts.minimal')
@section('content')
<link rel="stylesheet" href="{{ url('plugins/trumbowyg/ui/trumbowyg.min.css') }}">

  <h3><a href="/admin/pages">Page</a> > <a href="{{ url('admin/services')}}" >Service </a >  > Add Service</h3>
<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Add Service Details </div>
  <div class="panel-body">

{{ Form::open(['action' => 'ServiceController@store', 'files' => true])  }}

<input type="hidden" name="page_id" value="{{$service_page->id}}">
  <div class="form-group">
    <label for="page_name">Service  Name:</label>
    <input type="text" name="service_name" placeholder="Enter service name" class="form-control" required>
  </div>
  <div class="form-group">
    <label for="page_description">Service Description:</label>
    <input type="text" name="service_description"class="form-control" placeholder="Enter service description" required>
  </div>
  <div class="form-group">
    <label for="service_detail">Service Detail:</label>
    <textarea class="form-control" name="service_detail" id="editor" required></textarea>

  </div>

  <div class="form-group">
    <label for="page_title">Service image:</label>
    <input type="file" id="featuredImg" name="service_image" required>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}

@if(count($service_page->services) > 0)
<div class="panel panel-primary">
  <div class="panel-heading">Services </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Service Name</th><th>Service Description</th><th>Service Image</th> <th>Action</th>
    </thead>
    <tbody>
      @if($service_page->services!= null)
      @foreach($service_page->services as $service)
      <tr>
        <td> {{$service->name}} </td>
        <td>{{$service->description}}</td>
        <td> <img src="{{url('/').$service->image->path}}" height="100"></td>
        <td>

          <a href="{{ action('ServiceController@edit',$service->id) }}">
            <button class="btn btn-warning btn-block" > Edit </button>
          </a><br>
          {!! Form::open(['action' => ['ServiceController@destroy', $service->id],  'method' => 'delete']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
          {{Form::close() }}

        </td>

      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>
@endif


@endsection
