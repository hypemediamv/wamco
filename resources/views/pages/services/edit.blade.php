@extends('layouts.minimal')
@section('content')
<link rel="stylesheet" href="{{ url('plugins/trumbowyg/ui/trumbowyg.min.css') }}">

  <h3><a href="/admin/pages">Page</a> > <a href="{{ url('admin/services')}}" >Service </a >  > Edit Service</h3>
<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Edit Service Details </div>
  <div class="panel-body">

{{ Form::open(['action' => ['ServiceController@update', $service->id], 'files' => true, 'method' => 'put'])  }}

  <div class="form-group">
    <label for="page_name">Service  Name:</label>
    <input type="text" name="service_name" value="{{ $service->name }}" placeholder="Enter service name" class="form-control" required>
  </div>
  <div class="form-group">
    <label for="page_description">Service Description:</label>
    <input type="text" name="service_description" class="form-control" value="{{ $service->description }}" placeholder="Enter service description" required>
  </div>
  <div class="form-group">
    <label for="service_detail">Service Detail:</label>
    <textarea class="form-control" name="service_detail" id="editor"  required> {{ $service->detail }}</textarea>

  </div>

    <div class="form-group">
      <label for="service_image">Service Image</label>
      @if(!empty($service) && !empty($service->image_id))
        <br />
        <img src="{{ $service->image->path }}" style="max-width:50%;" alt="">
      @endif
      <input type="file" id="featuredImg" name="service_image" class="" >
    </div>

  <button type="submit" class="btn btn-default">Update</button>

</div>

</div>

{{Form::close() }}


@endsection
