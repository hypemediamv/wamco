@extends('layouts.front')
@section('content')
        	<script type="text/javascript">
        		$(document).ready(function() {



              $('#fullpage').fullpage({
                anchors: ['firstPage'],
                sectionsColor: ['whitesmoke'],
                autoScrolling: false,
                css3: true,
                fitToSection: false
              });


        		});



        	</script>

        	<style>

            /* Style for our header texts
            * --------------------------------------- */
            h1{
              font-size: 5em;
              font-family: arial,helvetica;
              color: black;
              margin:0;
              padding:40px 0 0 0;
            }
            .intro p{
              color: black;
              padding:40px 0 0 0;
            }
            .wrap{
              margin-left: auto;
              margin-right: auto;
              width: 980px;
              position: relative;
              padding: 20px 0 20px 0;
            }
            .wrap h1{
              font-size: 2.3em;
              color: black;
              padding: 30px 0 10px  0;
            }
            .wrap p{
              font-size: 16px;
              padding:  0 0 10px 0;
            }
            .box{
              display: block;
              background: #f2f2f2;
              border:1px solid #ccc;
              padding: 20px;
              margin:20px 0;
            }

            /* Centered texts in each section
            * --------------------------------------- */
            .section{
              text-align:left;

            }

            .wamco_service{
              padding-top: 3%;
            }

            .wamco_service p{
              font-size: 20px;
            }

            .wamco_service_description{
              padding-top: 3%;
            }


        	</style>




          <div >
          	<div class="section">

              <div class="container wamco_service">
                <div class="row">



                  <div class="col-md-4 wamco_service">

                    <img src="{{url('/').$service->image->path}}" width="100%">
                  </div>

                  <div class="col-md-8 ">

              <h1>{{$service->name}}</h1>
              <p>{{$service->description}}</p>
              <h6>{{$service->updated_at->diffForHumans()}}</h6>


              <div class="wamco_service_description">

                <ol class="breadcrumb">
      <li><a href="{{url('/') }}">Home</a></li>
      <li><a href="{{url('/#Service') }}">Service</a></li>
      <li class="active">{{$service->name}}</li>
      </ol>




              <p style="font-size:16px;">{{$service->detail}}</p>
            </div>
          </div>


            </div>
          </div>

            </div>



          	</div>
@endsection
