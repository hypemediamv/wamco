@extends('layouts.front2')

@section('content')
    <section>
        <div class="container">
            <h1>{{$service->name}}</h1>

            <div class="row">

                <div class="col-sm-6">
                    <p class="lead">{{$service->description}}</p><br>
                    {!! $service->detail !!}                                    </div>
                <div class="col-sm-6">
                    <div class="boxed boxed--lg boxed--border bg--secondary"> <img alt="image" src="{{url('/').$service->image->path}}"  class="border--round">
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
