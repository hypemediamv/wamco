@extends('layouts.minimal')
@section('content')
<h3> <a href="{{ url('/admin/dashboard')}}">All Pages</a> </h3><br>


<div class="row">
  <div class="col-md-3"><a href="{{ url('/admin/services ')}}" ><button type="submit" class="btn btn-block btn-success"> Service </button> </a></div>
  <div class="col-md-3"><a href="{{ url('/admin/about ')}}" ><button type="submit" class="btn btn-block btn-success"> About </button> </a></div>
  <div class="col-md-3"><a href="{{ url('/admin/contacts ')}}" ><button type="submit" class="btn btn-block btn-success"> Contact </button> </a></div>
  <div class="col-md-3"><a href="{{ url('/admin/resources ')}}" ><button type="submit" class="btn btn-block btn-success"> Resources </button> </a></div>

</div>
<br>

@if( Session::get('edit_page') != null)

  {{ Form::open(['action' => 'PageController@store'])  }}
<!-- Service Creator-->
<div class="panel panel-primary">
  <div class="panel-heading">   @if( Session::get('edit_page') != null)   Edit  @else Create  @endif Service  </div>
  <div class="panel-body">

    @if( Session::get('edit_page') != null)
  <input type="hidden" name="page_id" value="{{Session::get('edit_page')->id}}" >
    @endif

  <div class="form-group">
    <label for="page_name">  Name:</label>
    <input type="text" name="page_name"class="form-control" id="page_name" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->name : null }}">
  </div>
  <div class="form-group">
    <label for="page_description">  Description:</label>
    <input type="text" name="page_description"class="form-control" id="page_description" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->description : null }}">
  </div>
  <div class="form-group">
    <label for="page_title">  Title:</label>
    <input type="text" name="page_title"class="form-control" id="page_title" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->title : null }}">
  </div>

  @if(Session::get('edit_page')->page_type_id == 2)
  <div class="form-group">
    <label for="background_color"> Company Profile:</label>
    <textarea id="summernote" name="background_color" class="form-control" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->background_color : null }}"></textarea>
  </div>
  @else
  <div class="form-group">
    <label for="background_color">Page Background Color:</label>
    <input type="text" name="background_color" placeholder="Enter page background color" class="form-control" id="background_color" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->background_color : null }}">
  </div>
  @endif




  <button type="submit" class="btn btn-success">Submit</button>
{{Form::close() }}

@if( Session::get('edit_page') != null)
{!! Form::open(['action' => ['PageController@destroy', Session::get('edit_page')->id],  'method' => 'delete', 'style'=>'display:inline-block' ]) !!}
{{ csrf_field()  }}
<button type="submit" class="btn btn-danger">Delete</button>
{{Form::close() }}

 <a href="{{ url('admin/pages') }}">  <button type="submit" class="btn btn-default" style="display:inline-block">CLOSE</button> </a>
@endif
</div></div>
<!-- Service Creator-->
@endif



<!--Display-->
@if($pages->isEmpty() == false  &&  Session::get('edit_page') == null)
<div class="panel panel-primary">
  <div class="panel-heading"> Pages </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Page Name</th><th>Page Title</th> <th> Page Description</th> <th>Type</th> <th >Edit</th>
    </thead>
    <tbody>
      @if($pages!= null)
      @foreach($pages as $page)
      <tr>
        <td ><a href="{{ url('admin/pages/'.$page->id) }}"> {{$page->name}} </a> </td>
        <td>{{$page->title}}</td>
        <td>{{$page->description}}</td>
        <td> <b>{{$page->page_type->name}} </b></td>

        <td>
          <a href="{{ url('admin/pages/'.$page->id.'/edit') }}"> <button type="button" class="btn btn-success btn-block" style="display:inline-block;" >Edit</button> </a>
        </td>

      </tr>
      @endforeach

      @endif
    </tbody>
  </table>

  </div>
</div>
@endif

<!--Display-->

@endsection
