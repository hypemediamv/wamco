@extends('layouts.minimal')
@section('content')

  <h3><a href="/admin/pages">Page</a> > <a href="/admin/about/" > About </a >  > Edit Staff</h3>
<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Edit Staff Details </div>
  <div class="panel-body">

    {{ Form::open(['action' => ['StaffController@update', $staff->id], 'files' => true, 'method' => 'put'])  }}

  <div class="form-group">
    <label for="page_name">Staff  Name:</label>
    <input type="text" name="staff_name" class="form-control" value="{{ $staff->name  }}"  placeholder="Enter staff name" required>
  </div>
  <div class="form-group">
    <label for="page_description">Staff Designation:</label>
    <input type="text" name="staff_designation" class="form-control" value="{{ $staff->designation }}" placeholder="Enter staff designation" required>
  </div>
  <div class="form-group">
    <label for="staff_quote">Staff Quote:</label>
    <input type="text" name="staff_quote" class="form-control" value="{{ $staff->quote }}" placeholder="Enter staff quote" required>
  </div>

    <div class="form-group">
      <label for="staff_image">Staff Image</label>
      @if(!empty($staff) && !empty($staff->image_id))
        <br />
        <img src="{{ $staff->image->path }}" style="max-width:50%;" alt="">
      @endif
      <input type="file" id="featuredImg" name="staff_image" class="" >
    </div>

    <button type="submit" class="btn btn-default">Update</button>

  </div>

</div>

{{Form::close() }}


@endsection
