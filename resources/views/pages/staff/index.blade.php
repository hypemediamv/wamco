@extends('layouts.minimal')
@section('content')

  <h3><a href="/admin/pages">Page</a> > <a href="/admin/about/" > About </a >  > Add Staff</h3>
<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Add Staff Details </div>
  <div class="panel-body">

{{ Form::open(['action' => 'StaffController@store', 'files' => true])  }}

<input type="hidden" name="page_id" value="{{$staff_page->id}}">
  <div class="form-group">
    <label for="page_name">Staff  Name:</label>
    <input type="text" name="staff_name"class="form-control" placeholder="Enter staff name" required>
  </div>
  <div class="form-group">
    <label for="page_description">Staff Designation:</label>
    <input type="text" name="staff_designation"class="form-control" placeholder="Enter staff designation" required>
  </div>
  <div class="form-group">
    <label for="staff_quote">Staff Quote:</label>
    <input type="text" name="staff_quote"class="form-control" placeholder="Enter staff quote" required>
  </div>

  <div class="form-group">
    <label for="page_title">Staff image:</label>
    <input type="file" name="staff_image" id="featuredImg" required>
  </div>
  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}

@if(count($staff_page->staff) > 0)
<div class="panel panel-primary">
  <div class="panel-heading">Staffs  </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Staff Name</th><th>Staff Designation</th><th> Staff Quote </th><th>Staff Image</th> <th>Actions</th>
    </thead>
    <tbody>
      @if($staff_page->staff!= null)
      @foreach($staff_page->staff as $staff)
      <tr>
        <td> {{$staff->name}} </td>
        <td>{{$staff->designation}}</td>
        <td>{{$staff->quote}}</td>
        <td> <img src="{{url('/').$staff->image->path}}" height="100"></td>
        <td>
          <a href="{{ action('StaffController@edit', $staff->id) }}">
            <button class="btn btn-warning btn-block" > Edit </button>
          </a><br>
          {!! Form::open(['action' => ['StaffController@destroy', $staff->id],  'method' => 'delete']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
          {{Form::close() }}

        </td>

      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>
@endif


@endsection
