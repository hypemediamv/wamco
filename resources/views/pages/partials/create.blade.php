@extends('layouts.minimal')
@section('content')
<h3> <a href="{{ url('admin/pages') }}">Pages</a> > <a href=" {{ url('admin/'.$link) }}" >{{$type}} </a> @if( Session::get('edit_page') != null) >  Edit @endif</h3><br>
@if($pages->isEmpty() == true || Session::get('edit_page') != null)
  {{ Form::open(['action' => 'PageController@store'])  }}
<!-- Service Creator-->
<div class="panel panel-primary">
  <div class="panel-heading">   @if( Session::get('edit_page') != null)   Edit @else Create  @endif {{$type}}   </div>
  <input type="hidden" value="{{$type}}" name="page_type" >
  <div class="panel-body">

    @if( Session::get('edit_page') != null)
  <input type="hidden" name="page_id" value="{{Session::get('edit_page')->id}}" >
    @endif

  <div class="form-group">
    <label for="page_name">{{$type}}  Page Name:</label>
    <input type="text" name="page_name"class="form-control" id="page_name" placeholder="Enter page name" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->name : null }}">
  </div>
  <div class="form-group">
    <label for="page_description">{{$type}}  Page Description:</label>
    <input type="text" name="page_description"class="form-control" id="page_description" placeholder="Enter page description" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->description : null }}">
  </div>
  <div class="form-group">
    <label for="page_title">{{$type}}  Page Title:</label>
    <input type="text" name="page_title"class="form-control" id="page_title" placeholder="Enter page title" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->title : null }}">
  </div>

  <div class="form-group">
    <label for="background_color">{{$type}}  Page Background Color:</label>
    <input type="text" name="background_color" placeholder="Enter page background color" class="form-control" id="background_color" value="{{ Session::get('edit_page') != null ? Session::get('edit_page')->background_color : null }}">
  </div>



  <button type="submit" class="btn btn-success">Submit</button>
{{Form::close() }}

@if( Session::get('edit_page') != null)
{!! Form::open(['action' => ['PageController@destroy', Session::get('edit_page')->id],  'method' => 'delete', 'style'=>'display:inline-block' ]) !!}
{{ csrf_field()  }}
<button type="submit" class="btn btn-danger">Delete</button>
{{Form::close() }}

 <a href="services">  <button type="submit" class="btn btn-default" style="display:inline-block">CLOSE</button> </a>
@endif
</div></div>
<!-- Service Creator-->
@endif



<!--Display-->
@if($pages->isEmpty() == false  &&  Session::get('edit_page') == null)
<div class="panel panel-primary">
  <div class="panel-heading">{{$type}}  Pages </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th>Page Name</th><th> Page Description</th><th>Page Title</th> <th >Edit</th><th >Delete</th>
    </thead>
    <tbody>
      @if($pages!= null)
      @foreach($pages as $page)
      <tr>
        <td ><a href="{{ url('admin/'.$link.'/'.$page->id )}}"> {{$page->name}} </a> </td>
        <td>{{$page->description}}</td>
        <td>{{$page->title}}</td>
        <td>
          <a href="{{ url('admin/pages/'.$page->id.'/edit')}}"> <button type="button" class="btn btn-success btn-block" style="display:inline-block;" >Edit</button> </a>
        </td>
        <td>
           {!! Form::open(['action' => ['PageController@destroy', $page->id],  'method' => 'delete']) !!}
           {{ csrf_field()  }}
           <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
           {{Form::close() }}
        </td>

      </tr>
      @endforeach

      @endif
    </tbody>
  </table>

  </div>
</div>
@endif

<!--Display-->

@endsection
