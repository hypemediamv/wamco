@extends('layouts.minimal')
@section('content')


  <h3><a href="{{ url('admin/pages')}}">Page</a> > <a href="{{ url('admin/contacts') }}" >{{$contact_page->name}} </a >   > Add Contact</h3>
<br>


<div class="panel panel-primary">
  <div class="panel-heading">  Add Contact Details </div>
  <div class="panel-body">

{{ Form::open(['action' => 'ContactController@store'])  }}
<input type="hidden" name="page_id" value="{{$contact_page->id}}">
  <div class="form-group">
    <label for="contact_name">  Name:</label>
    <input type="text" name="contact_name" class="form-control" placeholder="Enter company name" required>
  </div>
  <div class="form-group">
    <label for="contact_address">Address:</label>
    <input type="text" name="contact_address" class="form-control" placeholder="Enter company address" required>
  </div>
  <div class="form-group">
    <label for="phone_no">Phone Number:</label>
    <input type="text" name="phone_no" class="form-control" placeholder="Enter company phone number" required>
  </div>  <div class="form-group">

    <label for="phone_no">email:</label>
    <input type="email" name="email" class="form-control" placeholder="Enter company email" required>
  </div>
    <div class="form-group">
    <label for="postal_code">Postal Code: <em class="text-muted">optional</em></label>
    <input type="text" name="postal_code" class="form-control" placeholder="Enter company postal code">
  </div>
    <div class="form-group">
    <label for="country">Country:</label>
    <input type="text" name="country" class="form-control" placeholder="Enter company country" required>
  </div>

  <div class="form-group">
    <label for="fax_no">Fax Number:</label>
    <input type="text" name="fax_no" class="form-control" placeholder="Enter company fax number" required>
  </div>

  <div class="form-group">
    <label for="additional_info">Additional Info:<em class="text-muted">optional</em></label>
    <input type="text" name="additional_info" class="form-control" placeholder="Enter company adiitional info">
  </div>

  <button type="submit" class="btn btn-default">Save</button>

</div>

</div>

{{Form::close() }}
@if(count($contact_page->contacts) > 0)
<div class="panel panel-primary">
  <div class="panel-heading">Contacts </div>
  <div class="panel-body">
    <table class="table">
      <thead>
      <th> Name</th>
      <th>Address</th>
      <th>Email</th>
      <th>Country</th>
      <th>Phone No</th>
      <th>Fax No</th>
      <th>Additional Info</th>
      <th>Delete</th>
    </thead>
    <tbody>
      @if($contact_page->contacts!= null)
      @foreach($contact_page->contacts as $contact)
      <tr>
        <td> {{$contact->name}} </td>
        <td>{{$contact->address}}</td>
        <td>{{$contact->email}}</td>
        <td>{{$contact->country}}</td>
        <td>{{$contact->phone_no}}</td>
        <td>{{$contact->fax_no}}</td>
        <td>{{$contact->additional_info}}</td>
        <td>

          <a href="{{ action('ContactController@edit',$contact->id) }}">
            <button class="btn btn-warning btn-block" > Edit </button>
          </a><br>


          {!! Form::open(['action' => ['ContactController@destroy', $contact->id],  'method' => 'delete']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger btn-block"  style="display:inline-block;" >Delete</button>
          {{Form::close() }}

        </td>

      </tr>
      @endforeach
      @endif
    </tbody>
  </table>

  </div>
</div>
@endif


@endsection
