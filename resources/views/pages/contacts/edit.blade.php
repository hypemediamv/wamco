@extends('layouts.minimal')
@section('content')


  <h3><a href="{{ url('admin/pages')}}">Page</a> > <a href="{{ url('admin/contacts') }}" > Contact </a> > Edit Contact</h3>
<br>

<div class="panel panel-primary">
  <div class="panel-heading">  Edit Contact Details </div>
  <div class="panel-body">

{{ Form::open(['action' => ['ContactController@update', $contact->id], 'method' => 'patch'])  }}
    {{ csrf_field()  }}

  <div class="form-group">
    <label for="contact_name">  Name:</label>
    <input type="text" name="contact_name" class="form-control" value="{{ $contact->name }}"  placeholder="Enter company name" required>
  </div>
  <div class="form-group">
    <label for="contact_address">Address:</label>
    <input type="text" name="contact_address" class="form-control" value="{{$contact->address  }}" placeholder="Enter company address" required>
  </div>
  <div class="form-group">
    <label for="phone_no">Phone Number:</label>
    <input type="text" name="phone_no" class="form-control" value="{{ $contact->phone_no }}" placeholder="Enter company phone number" required>
  </div>  <div class="form-group">

    <label for="phone_no">email:</label>
    <input type="email" name="email" class="form-control" value="{{ $contact->email }}" placeholder="Enter company email" required>
  </div>
    <div class="form-group">
    <label for="postal_code">Postal Code: <em class="text-muted">optional</em></label>
    <input type="text" name="postal_code" class="form-control" value="{{ $contact->postal_code  }}" placeholder="Enter company postal code">
  </div>
    <div class="form-group">
    <label for="country">Country:</label>
    <input type="text" name="country" class="form-control" value="{{  $contact->country}}" placeholder="Enter company country" required>
  </div>

  <div class="form-group">
    <label for="fax_no">Fax Number:</label>
    <input type="text" name="fax_no" class="form-control" value="{{ $contact->fax_no }}" placeholder="Enter company fax number" required>
  </div>

  <div class="form-group">
    <label for="additional_info">Additional Info:<em class="text-muted">optional</em></label>
    <input type="text" name="additional_info" class="form-control" value="{{ $contact->additional_info  }}" placeholder="Enter company adiitional info">
  </div>

  <button type="submit" class="btn btn-default">Update</button>

</div>

</div>

{{Form::close() }}


@endsection
