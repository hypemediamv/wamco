@extends('layouts.front2')

@section('content')
<a id="start"></a>
<div class="main-container">
<section class="text-center imagebg" data-gradient-bg="#22313F,#34495E,#2C3E50,#67809F">
    <!-- <div class="background-image-holder"> <img alt="background" src="{{url('/')}}/img/landing-16.jpg"> </div> -->
    <div class="container">
        <div class="row">
            <div class="col-sm-10 col-md-8">
                <h1>About us</h1>
            </div>
        </div>
    </div>
</section>
<div class="main-container">

            <section class="text-center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2">
                            <p class="lead">
                                {!! $about_company[0]->background_color!!}
                            </p>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
@endsection
