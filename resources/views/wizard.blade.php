@extends('layouts.front2')

@section('content')
<section class="cover imagebg height-70 text-center" data-overlay="4">
         <div class="background-image-holder"><img alt="background" src="img/wizard-bg3.jpg"></div>
         <div class="container pos-vertical-center">
             <div class="row">


                 <div class="col-sm-10 col-md-8">
                     <h1>Waste Wizard</h1>
                     <p class="lead">The Waste Wizard is an online waste sorting tool that provides information on how to properly handle and dispose of over 1500 different items </p>
                     <div class="boxed boxed--lg bg--white text-left">
                         <form class="form--horizontal">
                             <div class="col-sm-12">
                               <!-- <input type="text" name="search" placeholder="Type search keywords here"> -->
                               Search
                               <select class="js-data-example-ajax">
                                 <option >Type search keywords here ...</option>
                               </select>

                              </div>
                             <!-- <div class="col-sm-4"> <button type="submit" class="btn btn--primary type--uppercase">Search</button> </div> -->
                         </form>
                     </div>
                 </div>
             </div>
         </div>
     </section>
     <!-- Trigger the modal with a button -->


     <div class="modal-instance">
     	<a class="btn modal-trigger" href="#" id="trigger_modal" style="display:none;">
     		<span class="btn__text">
     			TRIGGER MODAL
     		</span>
     	</a>
     	<div class="modal-container">
        <div class="modal-content">
                            <div class="feature-large bg--white border--round">
                              <div class="row">
                              <div class="col-md-6"><div class="result_content"></div>
                          </div>
                              <div class="col-md-6">
                                <br><br>

                                   <h2 id="result_heading"> </h2>
                                  <hr class="short">
                                  <p class="lead" id="result_description">
                                  </p></div>


                              </div>


                            </div>

                            </div>
                        <div class="modal-close modal-close-cross"></div></div>



     	</div>
     </div><!--end of modal instance-->
@endsection
