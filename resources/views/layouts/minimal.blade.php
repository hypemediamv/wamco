<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>{{ config('app.name', 'Laravel') }}</title>
    <!-- Latest compiled and minified CSS | Bootstrap -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" media="all" >

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
    integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Jquery -->
    <!-- moment js -->
    <script type="text/javascript" src="{{ asset('plugins/moment/moment.min.js') }}" ></script>

    <!-- bootstrap datetime picker -->

    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" media="all">

    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-tokenfield/css/bootstrap-tokenfield.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-tokenfield/css/tokenfield-typeahead.min.css') }}">

    <!--Bootstrap File Input -->
    <link rel="stylesheet" href="{{ asset('plugins/custom-file-input/css/fileinput.css') }}">


    <link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.css') }}">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/simple-sidebar.css') }}" rel="stylesheet">

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>

<body>
<div id="wrapper">

        <!-- Sidebar -->
        @include('layouts.navigation')

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="">
                      <div id="menu-toggle"> <i class="fa fa-list fa-2x" aria-hidden="true"></i> </div>
						<div id="message-box">
							{!! Notifications::byGroup('0')->toHTML() !!}
						</div>
                    @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>

<!-- /#wrapper -->

    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('plugins/jquery/js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

	<script type="text/javascript" src="{{ asset('plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>

    <!--  Bootrap file inout -->
    <script src="{{ asset('plugins/custom-file-input/js/fileinput.js') }}"></script>

    <!--  Elevate Zoom-->
    <script src="{{ asset('plugins/elevatezoom/js/elevatezoom.js') }}"></script>

	<script src="{{ asset('plugins/bootstrap-tokenfield/bootstrap-tokenfield.min.js') }}"></script>

  <!-- include summernote css/js-->
  <link rel="stylesheet" href="{{ asset('plugins/summernote/summernote.css') }}">
	<script src="{{ asset('plugins/summernote/summernote.js') }}"></script>

  <!--trumbowyg-->
	<script src="{{ asset('plugins/trumbowyg/trumbowyg.min.js') }}"></script>
  <script src="{{ asset('plugins/trumbowyg/plugins/table/trumbowyg.table.js') }}"></script>
	<script src="{{ asset('js/post.js') }}"></script>
	<script src="{{ asset('js/minimal.js') }}"></script>

<script>
    $(document).ready(function () {
      $('#summernote').summernote({
        dialogsInBody: true,
        dialogsFade: true,           // Add fade effect on dialogs
        height: 300,                 // set editor height
        minHeight: null,             // set minimum height of editor
        maxHeight: null,             // set maximum height of editor
        focus: true                  // set focus to editable area after initializing summernote
      });
        window.setTimeout(function() {
            $("#message-box").fadeTo(500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 2000);
    });
</script>

</body>


</html>
