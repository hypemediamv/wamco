<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>WAMCO : Waste Management Corporation</title>

    <!-- Bootstrap core CSS -->



    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" media="all" >
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/jquery.fullPage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/scroll.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" media="all" >
    <!-- <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/navbar-top-fixed.css') }}" media="all" > -->
    <!-- <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/sticky-footer.css') }}" media="all" > -->

    <!-- WAMCO Custom Style Sheet-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontpage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/wamco_services.css') }}" />

    <style>
        /* Footer styles
-------------------------------------------------- */
        html {
            position: absolute;
            min-height: 100%;
        }
        body {
            /* Margin bottom by footer height */
            margin-bottom: 60px;
/*            font-family: "Times New Roman", Georgia, Serif;
*/
        }
        #footer {
            position: absolute;
            bottom: 0;
            width: 100%;
            /* Set the fixed height of the footer here */
            height: 40px;
            margin: 0 auto;
            background:#1BA39C;
                /* color overlay */
                    /*linear-gradient(*/
                            /*rgba(0, 134, 139, 0.2),*/
                            /*rgba(0, 0, 0, 0.6)*/
                    /*)*/
                        /* image to overlay */
                    /*url(https://source.unsplash.com/category/nature/1600x600);*/
        }


        /* Custom footer CSS
        -------------------------------------------------- */

        /*.container {*/
            /*width: auto;*/
            /*max-width: 680px;*/
            /*padding: 0 15px;*/
        /*}*/
        .container .text-muted {
            margin: 20px 0;
        }
        .footertext {
            color: #ffffff;
        }

    </style>


</head>

<body>

@include('layouts.partials.front-navigation')

@yield('content')
<!--
<div id="footer">
    <div class="container">
        <div class="row">
            <p><center><p class="footertext">&copy; WAMCO 2016 - {{ date('Y') }}</p></center></p>
        </div>
    </div>
</div>
 -->


                <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('plugins/scrolling/js/jquery.fullPage.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/frontpage.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/smooth-scroll.js') }}"></script>

        <script>
        $('ul.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

    smoothScroll.init();


        </script>
</body>
</html>
