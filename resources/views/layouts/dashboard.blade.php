<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    {{--<link href="/css/app.css" rel="stylesheet">--}}

    <!-- Jquery -->
    <!-- moment js -->
    <script type="text/javascript" src="/plugins/moment/moment.min.js"></script>
    <!-- Bootstrap -->
    <script type="text/javascript" src="/plugins/bootstrap/js/bootstrap.min.js"></script>
    <!-- bootstrap datetime picker -->
    <script type="text/javascript" src="/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"></script>
    <link rel="stylesheet" href="/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" media="all">

    <!-- SlidesJS Required: Link to jquery.slides.js -->
    <script src="/plugins/slide-js/jquery.slides.min.js"></script>
    <!-- Custom CSS -->
    <link href="/plugins/scrolling/css/scroll.css" rel="stylesheet">
    {{--<script type="text/javascript" src="/plugins/jquery/js/jquery.min.js"></script>--}}

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]-->
    {{--<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>--}}
    {{--<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>--}}
    <!--[endif]-->
    @yield('custom-header-scripts') <!-- for custom css and js header scripts -->

    <!-- Scripts -->
    <script>
        window.Laravel = <?php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); ?>
    </script>
</head>
<body>
    @include('layouts.top-nav')
    <!-- end of top nav -->

    <!-- start side bar -->
    <div class="template-page-wrapper">

        @include('layouts.navigation')

        {{--<div class="container">--}}
            <div class="templatemo-content-wrapper">
                <div class="templatemo-content">

                    <ol class="breadcrumb">
                        <li><a href="#">Admin Panel</a></li>
                        <li><a href="#">Dashboard</a></li>
                    </ol>
                    <div class="well">
                        <div>
                            {!! Notifications::byGroup('0')->toHTML() !!}
                        </div>
                        @yield('content')
                    </div>

                </div>
            </div>

        <footer class="templatemo-footer">
            <div class="templatemo-copyright">
                <p>Copyright &copy; 2016 Waste Management Corporation Pvt Ltd</p>
            </div>
        </footer>
    </div>

    <!-- Jquery -->
    <script src="/plugins/scrolling/js/jquery.easing.min.js"></script>
    <script src="/js/dashboard.js"></script>
    <script src="/plugins/jquery/js/jquery.min.js"></script>
    @yield('custom-bottom-scripts') <!-- for custom css and js header scripts -->

    <!-- Scripts -->
    {{--<script src="/js/app.js"></script>--}}
</body>
</html>
