<div id="sidebar-wrapper">
    <ul class="sidebar-nav ">
        <li class="sidebar-brand" >
            <a href="{{ url('/') }}" target="_blank" style="color:#36D7B7">
              <img border="0" alt="Wamco" src="/logo/logo.png" width="50%"><i class="fa fa-long-arrow-right"> Wamco</i>
            </a>
        </li>

        <li><a href="{{ url('admin/slideshows') }}"><span class="glyphicon glyphicon-expand"></span> Manage Slide Shows</a></li>

        <li><a href="{{ url('admin/pages') }}"><i class="fa fa-book"></i> Manage Pages</a></li>
        <li><a href="{{ url('admin/services') }}"><i class="fa fa-cogs"></i> Manage Services</a></li>
        <li><a href="{{ url('admin/about') }}"><i class="fa fa-info"></i> Manage About</a></li>
        <li><a href="{{ url('admin/resources') }}"><i class="fa fa-download"></i> Manage Resources</a></li>
        <li><a href="{{ url('admin/contacts') }}"><span class="glyphicon glyphicon-list-alt"></span> Manage Contacts</a></li>

        <li><a href="{{ url('admin/post/categories') }}"> <span class="fa fa-th-large" aria-hidden="true"></span> Manage Blog Categories</a></li>
        <li><a href="{{ url('admin/posts') }}"><i class="fa fa-th-large"></i> Manage Blog</a></li>
        <li><a href="{{ url('admin/tags') }}"><i class="fa fa-tags"></i> Manage Tags</a></li>

        <li>
            <a href="{{ url('admin/shop/categories') }}"> <span class="glyphicon glyphicon-th-large" aria-hidden="true"></span> Manage Shop Categories</a>
        </li>

        <li>
            <a href="{{ action('ShopZoneController@index') }}"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Manage Shop Zones</a>
        </li>

        <li>
            <a href="{{ url('admin/shop/products') }}"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Manage Products</a>
        </li>

        <li>
            <a href="{{ url('admin/shop/products/all') }}"> <span class=" glyphicon glyphicon-shopping-cart" aria-hidden="true"></span> Manage Shop</a>
        </li>

        <li>
            <a href="{{ url('admin/billing-services') }}"> <span class=" glyphicon glyphicon-list-alt" aria-hidden="true"></span> Manage Billing services</a>
        </li>

        <li>
            <a href="{{ url('admin/billings') }}"> <span class=" glyphicon glyphicon-list-alt" aria-hidden="true"></span> Manage User Billings</a>
        </li>

        <li>
            <a href="{{ url('admin/mapped-locations') }}"><span class="glyphicon glyphicon-map-marker" aria-hidden="true"></span> Manage Mapped Locations</a>
        </li>

        <li>
            <a href="{{ action('WasteWizardDataBinController@index') }}"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Manage Bin </a>
        </li>

        <li>
            <a href="{{ action('WasteWizardDataController@index') }}"><span class="glyphicon glyphicon-th" aria-hidden="true"></span> Manage Wizard Data</a>
        </li>

        <li>
            <a href="{{ action('CollectionDateZoneController@index') }}"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Manage Collection Zones</a>
        </li>

        <li>
            <a href="{{ action('CollectionDateSiteController@index') }}"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Manage Collection Sites</a>
        </li>

        <li>
            <a href="{{ action('CollectionDateController@index') }}"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Manage Collection Dates</a>
        </li>
        <li>

        <a href="{{ action('UserVerificationController@index') }}"><span class="glyphicon glyphicon-calendar" aria-hidden="true"></span> Manage User Verification</a>
      </li>

        <li>
            <a href="{{ url('admin/settings/social') }}"><span class="glyphicon glyphicon-cog" aria-hidden="true"></span>  Manage Social Media Settings</a>
        </li>

        <li>
            <a href="{{ url('/logout') }}"
               onclick="event.preventDefault();
                      document.getElementById('logout-form').submit();">
                <i class="fa fa-sign-out"></i>Sign Out
            </a>

            <form id="logout-form" action="{{ url('/logout') }}" method="POST" style="display: none;">
                {{ csrf_field() }}
            </form>
        </li>

    </ul>
</div>
<!-- /#sidebar-wrapper -->
