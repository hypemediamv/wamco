<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, shrink-to-fit=no, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>WAMCO e-Shop</title>

    <!-- Bootstrap Core CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" media="all" >

    <!--WYSIWYG TRUMBOW-->
    <link rel="stylesheet" href="{{ asset('plugins/trumbowyg/ui/trumbowyg.min.css') }}" media="all" >

    <!--Bootstrap File Input -->
    <link rel="stylesheet" href="{{ asset('plugins/custom-file-input/css/fileinput.css') }}" media="all" >



    <!--Font Awesome-->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" media="all" >

    <!-- Custom CSS -->
    <link rel="stylesheet" href="{{ asset('css/shop-sidebar.css') }}" media="all" >
    <link rel="stylesheet" href="{{ asset('css/shop-navbar.css') }}" media="all" >


    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<style>
.wamco_logo{
  padding:20px;
  background-color: whitesmoke;
}

.wamco_distance{
  padding-right:10px;
}
.wamco_shift{
  margin-right:5px;
}
</style>

</head>

<body>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">



    <div id="wrapper">

        <!-- Sidebar -->
        <div id="sidebar-wrapper">
            <ul class="sidebar-nav">
            <a href="{{ url('/shop')}}" ><img  class="wamco_logo" src="{{asset('logo/logo.png')}}" width="100%"></a>



                <li>
                  <h3>Categories</h3>
                </li>

                @if($categories!= null)
                @foreach($categories as $category)
                <li>
                    <a href="{{ route('shop-category',$category->id) }}">{{$category->title}}</a>
                </li>

                @endforeach
                @endif

                <li>

                  <h4><a href="{{ action('CartController@index')}}">Your Cart

                    @if(isset($signed_user))

                    @if($signed_user->cart!= null)

<span class="label label-default">{{$signed_user->cart->cart_products->sum('quantity')}}</span>

@endif
                    @endif
                  </h4></a>

                </li>


            </ul>
        </div>
        <!-- /#sidebar-wrapper -->
        <div class="wamco-navbar">
                <div class="row row1">

                </div>
                <div class="row row2">
                  @if(isset($signed_user))
                  <h5> Logged in as </h5> {{$signed_user->name}}
                  @endif
                  <div class="row">

                    <br>
                    <a href ="{{ url('/')}}">
                    <button class="wamco-menu col-md-1 wamco_shift">
                      <span class="glyphicon glyphicon-home" aria-hidden="true"></span>
                    </button></a>

                    <button class="wamco-menu col-md-1 wamco_shift" id="menu-toggle">
                      <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </button>
                    {!! Form::open(['method'=>'GET','route'=>['shop-search'] ])  !!}

                  <input class="wamco-navbar-input col-md-3" type="" placeholder="Search for Products" name="q">
                    <button class="wamco-navbar-button col-md-1">
                      <svg width="15px" height="15px">
                        <path d="M11.618 9.897l4.224 4.212c.092.09.1.23.02.312l-1.464 1.46c-.08.08-.222.072-.314-.02L9.868 11.66M6.486 10.9c-2.42 0-4.38-1.955-4.38-4.367 0-2.413 1.96-4.37 4.38-4.37s4.38 1.957 4.38 4.37c0 2.412-1.96 4.368-4.38 4.368m0-10.834C2.904.066 0 2.96 0 6.533 0 10.105 2.904 13 6.486 13s6.487-2.895 6.487-6.467c0-3.572-2.905-6.467-6.487-6.467 "></path>
                      </svg>
                    </button>

                    {!! Form::close()  !!}



                <div class="col-md-6">
                  <div style="display:inline-block" class="wamco_distance"><h4>By Category </h4> </div>

                <div class="btn-group" role="group" aria-label="...">
                <a href="{{url('/shop')}}">   <button type="button" class="btn btn-default">  Categories </button> </a>

                @foreach($categories as $category)
                 <a href="{{route('shop-category',$category->id)}}"> <button type="button" class="btn btn-default">   {{$category->title}}</button></a>
                @endforeach
                </div>
                </div>
                </div>




                </div>
        </div>
        <!-- Page Content -->
        <div id="page-content-wrapper">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-lg-12">

                      @yield('content')
                    </div>
                </div>
            </div>
        </div>
        <!-- /#page-content-wrapper -->

    </div>
    <!-- /#wrapper -->




    <!-- jQuery -->
    <script type="text/javascript" src="{{ asset('plugins/jquery/js/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

    <!--trumbowyg-->
    <script type="text/javascript" src="{{ asset('plugins/trumbowyg/trumbowyg.min.js') }}"></script>

    <!--  Bootrap file inout -->
    <script type="text/javascript" src="{{ asset('plugins/custom-file-input/js/fileinput.js') }}"></script>


    <!--  Elevate Zoom-->
    <script type="text/javascript" src="{{ asset('plugins/elevatezoom/js/elevatezoom.js') }}"></script>

    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
    });



    $(function(){
        $('#editor').trumbowyg();
    });

    $("#js_image").fileinput({
        showUpload: false,
        maxFileCount: 5,
        showRemove: false,
        previewFileType: 'image',
        initialPreviewCount: 5,
        autoReplace: true,
        allowedFileTypes: ['image'],
        previewTemplates: {
            image: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
            '   <img src="{data}" style="max-width: 10%;" class="" title="{caption}" alt="{caption}">\n' +
            '</div>\n',
            generic: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
            '   {content}\n' +
            '</div>\n'
        }
    });

    $("#js_image2").fileinput({
        'allowedFileExtensions' : ['jpg', 'png','gif'],
        showRemove:false,
        showUpload:false,
    });




    </script>


</body>

</html>
