<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>WAMCO : Waste Management Corporation</title>

    <!-- Bootstrap core CSS -->



    <!-- Custom styles for this template -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}" media="all" >
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/jquery.fullPage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/scrolling/css/scroll.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}" media="all" >
    <!-- <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/navbar-top-fixed.css') }}" media="all" > -->
    <!-- <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/sticky-footer.css') }}" media="all" > -->

    <!-- WAMCO Custom Style Sheet-->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/frontpage.css') }}" />
    <link rel="stylesheet" type="text/css" href="{{ asset('css/wamco_services.css') }}" />

  

</head>

<body>

@include('layouts.partials.front-navigation')

@yield('content')
<!--
<div id="footer">
    <div class="container">
        <div class="row">
            <br>
            <div class="col-md-4">
                <center>
                    <img src="http://oi60.tinypic.com/w8lycl.jpg" class="img-circle" alt="the-brains">
                    <br>
                    <h4 class="footertext">Programmer</h4>
                    <p class="footertext">You can thank all the crazy programming here to this guy.<br>
                </center>
            </div>
            <div class="col-md-4">
                <center>
                    <img src="http://oi60.tinypic.com/2z7enpc.jpg" class="img-circle" alt="...">
                    <br>
                    <h4 class="footertext">Artist</h4>
                    <p class="footertext">All the images here are hand drawn by this man.<br>
                </center>
            </div>
            <div class="col-md-4">
                <center>
                    <img src="http://oi61.tinypic.com/307n6ux.jpg" class="img-circle" alt="...">
                    <br>
                    <h4 class="footertext">Designer</h4>
                    <p class="footertext">This pretty site and the copy it holds are all thanks to this guy.<br>
                </center>
            </div>
        </div>
        <div class="row">
            <p><center><a href="#">Contact Stuff Here</a> <p class="footertext">Copyright 2014</p></center></p>
        </div>
    </div>
</div>
 -->


                <!-- Bootstrap core JavaScript
    ================================================== -->
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.9.1/jquery-ui.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>
        <script type="text/javascript" src="{{ asset('plugins/scrolling/js/jquery.fullPage.js') }}"></script>
        <script type="text/javascript" src="{{ asset('js/frontpage.js') }}"></script>

        <script>
        $('ul.nav li.dropdown').hover(function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn(500);
        }, function() {
          $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut(500);
        });

        </script>
</body>
</html>
