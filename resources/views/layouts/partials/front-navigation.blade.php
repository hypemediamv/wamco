<!-- Fixed navbar -->
<nav class="navbar navbar-default navbar-fixed-top wamco_navigation">
    <div class="container" >
        <div class="navbar-header page-scroll">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="{{ url('/')}}"><h3 class="wamco-brand">WAMCO</h3></a>
            @include('partials.social-links')
        </div>
        <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav"></ul>
            <ul class="nav navbar-nav navbar-right wamco-navbar">
                <li data-menuanchor="home"><a href="{{ url('/#home')}}"></i>Home</a></li>
                @foreach($pages as $page)

                    @if($page->page_type->name == 'service')
                    <li  data-menuanchor="{{$page->name}}" class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$page->title}} <b class="caret"></b></a>
                            <ul class="dropdown-menu">

                         @foreach($page->services as $service)
                         <li> <a href="{{route('service-single',$service->id)}}">{{$service->name}}</a></li>

                      @endforeach
                    </ul>
                    </li>
                    @else
                    <li data-menuanchor="{{$page->name}}"><a href="{{ url('/#'.$page->name) }}">{{$page->title}}</a></li>


                    @endif

                @endforeach



                <li><a href="{{url('/blog')}}">Blog</a></li>
                <li><a href="{{url('/shop')}}">Shop</a></li>

            </ul>
        </div><!--/.nav-collapse -->
    </div>
</nav>
