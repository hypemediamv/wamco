<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>WAMCO | Waste Management Corporation</title>
        <meta name="description" content="Main Page" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- <link href="{{ url('/')}}/stack/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="{{ url('/')}}/stack/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/stack/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/stack/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/stack/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/stack/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/stack/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/css/select2.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/css/select2-bootstrap.css" rel="stylesheet" type="text/css" media="all" /> -->

        <link href="{{ url('/')}}/public/stack/css/stack-interface.css" rel="stylesheet" type="text/css" media="all">
        <link href="{{ url('/')}}/public/stack/css/socicon.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/stack/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/stack/css/flickity.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/stack/css/stack-interface.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/stack/css/theme-serpent.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/stack/css/custom.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/css/select2.css" rel="stylesheet" type="text/css" media="all" />
        <link href="{{ url('/')}}/public/css/select2-bootstrap.css" rel="stylesheet" type="text/css" media="all" />

        <link href="https://fonts.googleapis.com/css?family=Open+Sans:200,300,400,400i,500,600,700" rel="stylesheet">
        <style>

        .select2-result-repository { padding-top: 4px; padding-bottom: 3px; }
        .select2-result-repository__avatar { float: left; width: 60px; margin-right: 10px; }
        .select2-result-repository__avatar img { width: 100%; height: auto; border-radius: 2px; }
        .select2-result-repository__meta { margin-left: 70px; }
        .select2-result-repository__title { color: black; font-weight: bold; word-wrap: break-word; line-height: 1.1; margin-bottom: 4px; }
        .select2-result-repository__forks, .select2-result-repository__stargazers { margin-right: 1em; }
        .select2-result-repository__forks, .select2-result-repository__stargazers, .select2-result-repository__watchers { display: inline-block; color: #aaa; font-size: 11px; }
        .select2-result-repository__description { font-size: 13px; color: #777; margin-top: 4px; }
        .select2-results__option--highlighted .select2-result-repository__title { color: white; }
        .select2-results__option--highlighted .select2-result-repository__forks, .select2-results__option--highlighted .select2-result-repository__stargazers, .select2-results__option--highlighted .select2-result-repository__description, .select2-results__option--highlighted .select2-result-repository__watchers { color: #c6dcef; }

        </style>

    </head>
    <body data-smooth-scroll-offset="77">
        <div class="nav-container" >
            <!-- <div class="via-1490879511031" via="via-1490879511031" vio="Our Services"> -->
                <div class="bar bar--sm visible-xs">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-3 col-sm-2">
                 <!--                <a href="index.html"> <img class="logo logo-dark" alt="logo" src="img/logo-dark.png"> <img class="logo logo-light" alt="logo" src="img/logo-light.png"> </a> -->
                                                     <a href="{{ url('/')}}"><h4 style="color:#7eced2; font-weight:900;">WAMCO </h4> </a>

                            </div>
                            <div class="col-xs-9 col-sm-10 text-right">
                                <a href="#" class="hamburger-toggle" data-toggle-class="#menu1;hidden-xs hidden-sm"> <i class="icon icon--sm stack-interface stack-menu"></i> </a>
                            </div>
                        </div>
                    </div>
                </div>
                <nav id="menu1" class="bar bar-1 hidden-xs bar--absolute bar--transparent" data-scroll-class="100px:pos-fixed" style="padding-top:0.8em; padding-bottom:0.5em">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-1 col-sm-2 hidden-xs">
                                <div class="bar__module">
                                      <a href="{{ url('/')}}" style="color:#7eced2; font-weight:900;font-size:20px;">
                                      WAMCO
                                   </a>
                                    <!-- <a href="{{ url('/')}}"><h4 style="color:#7eced2; font-weight:900;">WAMCO </h3> </a> -->
                                </div>
                            </div>
                            <div class="col-md-11 col-sm-12 text-right text-left-xs text-left-sm">
                                <div class="bar__module">
                                    <ul class="menu-horizontal text-left">
                                        <li> <a  data-scroll href="{{url('/')}}#">
                                        Home
                                    </a> </li>
                                        <li> <a  data-scroll  href="{{url('/')}}#services">
                                        Services
                                    </a> </li>
                                        <li> <a  data-scroll  href="{{url('/')}}#about">
                                        About
                                    </a> </li>
                                        <li> <a  data-scroll  href="{{url('/')}}#resources">
                                        Resources
                                    </a> </li>
                                        <li> <a  data-scroll  href="{{url('/')}}#contact">
                                        Contact
                                    </a> </li>
                                        <li> <a  href="{{ url('/blog')}}">
                                                Blog
                                    </a> </li>

                                    <li> <a  href="{{ url('/login') }}">
                                            Bills
                                </a> </li>

                                @if( Auth::check())

                                <li>
                                  <a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">Logout</a>

            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>

                                </li>

                                @else

                                <li>
                                  <a href="{{ url('/login') }}">
                            Log In
                                  </a>

                                </li>

                                                                <li>
                                                                  <a href="{{ url('/register') }}">
                                                            Register
                                                                  </a>

                                                                </li>

                                @endif
                                        <!-- <li class="dropdown"> <span class="dropdown__trigger">
                                        Services
                                    </span>
                                            <div class="dropdown__container">
                                                <div class="container">
                                                    <div class="row">
                                                        <div class="dropdown__content col-md-2">
                                                            <ul class="menu-vertical">
                                                                <li> <a href="#">Single Link</a> </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </li> -->

                                    </ul>
                                </div>
                                <div class="bar__module">
                                    <a class="btn btn--sm type--uppercase" href="{{url('/wizard')}}"> <span class="btn__text">
                                    Waste Wizard
                                </span> </a>
                                   <a class="btn btn--sm type--uppercase" href="{{url('/collection-date')}}"> <span class="btn__text">
                                    Collection Date
                                    <a class="btn btn--sm type--uppercase" href="{{url('/zone-locations')}}"> <span class="btn__text">
                                    Mapped Locations
                                </span> </a>
                                    <a class="btn btn--sm btn--primary type--uppercase" href="{{ url('/shop')}}"> <span class="btn__text">
                                    Shop
                                </span> </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </nav>
            <!-- </div> -->
        </div>
        <div class="main-container">

          @yield('content')

          <footer class="footer-3 text-center-xs space--xs">
              <div class="container">
                  <div class="row">
                      <div class="col-sm-6">
                          <img alt="Image" class="logo" src="/logo/wamco.png" />
                          <ul class="list-inline list--hover">
                              <li>
                                  <!-- <a href="#"> -->
                                      <span class="type--fine-print">3000581</span>
                                  <!-- </a> -->
                              </li>
                              <li>
                                  <!-- <a href="#"> -->
                                      <span class="type--fine-print">info@wamco.com.mv</span>
                                  <!-- </a> -->
                              </li>
                          </ul>
                      </div>
                      <div class="col-sm-6 text-right text-center-xs">
                          @include('partials.social-links')
                      </div>
                  </div>
                  <!--end of row-->
                  <div class="row">
                      <div class="col-sm-6">
                          <p class="type--fine-print">
                              Managing waste in nation wide
                          </p>
                      </div>
                      <div class="col-sm-6 text-right text-center-xs">
                          <span class="type--fine-print">&copy;
                              <span class="update-year"></span> WAMCO Inc.</span>
                          <a class="type--fine-print" href="#">Privacy Policy</a>
                          <a class="type--fine-print" href="#">Legal</a>
                      </div>
                  </div>
                  <!--end of row-->
              </div>
              <!--end of container-->
          </footer>

        </div>
        <!-- <script src="{{ url('/')}}/stack/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/')}}/stack/js/flickity.min.js"></script>
        <script src="{{ url('/')}}/stack/js/parallax.js"></script>
        <script src="{{ url('/')}}/stack/js/isotope.min.js"></script>
        <script src="{{ url('/')}}/stack/js/countdown.min.js"></script>
        <script src="{{ url('/')}}/stack/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/')}}/stack/js/scripts.js"></script>
        <script src="{{ url('/')}}/stack/js/granim.min.js"></script>
        <script src="{{ url('/')}}/js/select2.js"></script> -->


        <script src="{{ url('/')}}/public/stack/js/jquery-3.1.1.min.js"></script>
        <script src="{{ url('/')}}/public/stack/js/flickity.min.js"></script>
        <script src="{{ url('/')}}/public/stack/js/parallax.js"></script>
        <script src="{{ url('/')}}/public/stack/js/isotope.min.js"></script>
        <script src="{{ url('/')}}/public/stack/js/countdown.min.js"></script>
        <script src="{{ url('/')}}/public/stack/js/smooth-scroll.min.js"></script>
        <script src="{{ url('/')}}/public/stack/js/scripts.js"></script>
        <script src="{{ url('/')}}/public/stack/js/granim.min.js"></script>
        <script src="{{ url('/')}}/public/js/select2.js"></script>

        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        @yield('footer-scripts')
        <script>
    smoothScroll.init();

    $(".js-data-example-ajax").select2();



    function formatRepo (data) {

          var markup = "<div class='select2-result-repository clearfix'>" ;

          if(data.bin){

            if(data.bin.image){
              markup +=  "<div class='select2-result-repository__avatar'><img src='"+ data.bin.image.path +"' /></div>" +
                        "<div class='select2-result-repository__meta'>" ;
            }


          }


            if(data.name){
              markup +=   "<div class='select2-result-repository__title'>" + data.name + "</div>";
            }

            if(data.description){
              markup += "<div class='select2-result-repository__description'>" + data.description + "</div>";
            }

          markup += "</div></div>";

          return markup;
        }

        function formatRepoSelection (data) {

          console.log(data);
          // $(".result_content").html("<img  id='result_image' alt='image' src='"+data.bin.image.path+"'>");

          $("#result_heading").html(data.name);
          $("#result_description").html(data.description);


          $("#trigger_modal").trigger("click");

          return data.name || data.description;
        }



        $(".js-data-example-ajax").select2({
          theme: "bootstrap",


          ajax: {
            url: "http://localhost:8000/wizard/api",
            // url: "https://api.github.com/search/repositories",
            dataType: 'json',
            delay: 250,
            data: function (params) {
              return {
                q: params.term, // search term
                page: params.page
              };
            },
            processResults: function (data, params) {

              // parse the results into the format expected by Select2
              // since we are using custom formatting functions we do not need to
              // alter the remote JSON data, except to indicate that infinite
              // scrolling can be used
              console.log(data);

              // $(".result_content").html("<img  id='result_image' alt='image' src='/img/green_bin.png'>");





// $("#result_image").attr("src",data[0].bin.image.path);
// $("#result_image").attr("src","/img/green_bin.png");



              console.log(data.items[0].bin.image.path);
              params.page = params.page || 1;

              return {
                results: data.items,
                pagination: {
                  more: (params.page * 30) < data.total_count
                }
              };
            },
            cache: true
          },
          escapeMarkup: function (markup) { return markup; }, // let our custom formatter work
          minimumInputLength: 1,
          templateResult: formatRepo, // omitted for brevity, see the source of this page
          templateSelection: formatRepoSelection // omitted for brevity, see the source of this page
        });



/*
        $('.js-data-example-ajax').on("select2:selecting", function(e) {


 $("#trigger_modal").trigger("click");
    //        $("#trigger_modal a").click(function() {
    //  //Do stuff when clicked
    //         });

        });

        */
</script>

    </body>

</html>
