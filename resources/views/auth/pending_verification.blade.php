@extends('layouts.front2')

@section('content')
<div class="main-container">
           <section class="height-100 bg--dark text-center">
               <div class="container pos-vertical-center">
                   <div class="row">
                       <div class="col-sm-12">
                           <h1 class="h1--large">Pending Verification for <b>{{$user->name or ''}} </b></h1>
                           <p class="lead">
                              Your Account is pending for Verification, Please Try again Later
                           </p>
                           <a href="{{ url('/')}}">Go back to home page</a>
                       </div>
                   </div>
                   <!--end of row-->
               </div>
               <!--end of container-->
           </section>
       </div>
@endsection
