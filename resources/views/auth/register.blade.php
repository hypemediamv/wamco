@extends('layouts.front2')

@section('content')
<div class="main-container">
    <section class="imageblock switchable feature-large height-100">
        <div class="imageblock__content col-md-6 col-sm-4 pos-right">
            <div class="background-image-holder">
                <img alt="image" src="img/inner-7.jpg" />
            </div>
        </div>
        <div class="container pos-vertical-center">
            <div class="row">
                <div class="col-md-5 col-sm-7">
                    <h2>Create a Wamco account</h2>
                    <p class="lead"></p>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/register') }}">
                        {!! csrf_field() !!}
                        <div class="row">

                          <div class="col-xs-6">
                              <input type="text" name="first_name" placeholder="First Name" />
                          </div>

                          <div class="col-xs-6">
                              <input type="text" name="last_name" placeholder="Last Name" />
                          </div>

                          <div class="col-xs-12">
                              <input type="text" name="name" placeholder="Username" />
                          </div>


                            <div class="col-xs-12">
                              <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}"  placeholder="Email" required>
                            </div>


                            <div class="col-xs-12">
                              <input id="password" type="password" class="form-control" name="password"  placeholder="Password" required>
                            </div>

                            <div class="col-xs-12">
                              <input id="password-confirm" type="password" class="form-control" name="password_confirmation"   placeholder="Confirm Password" required>
                            </div>


                            <div class="col-xs-12">
                                <button type="submit" class="btn btn--primary type--uppercase">Create Account</button>
                            </div>
                            <div class="col-xs-12">
                                <!-- <span class="type--fine-print">By signing up, you agree to the
                                    <a href="#">Terms of Service</a>
                                </span> -->
                            </div>
                        </div>

                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>

                        @endif

                        @if ($errors->has('password'))
                            <span class="help-block">
                                <strong>{{ $errors->first('password') }}</strong>
                            </span>
                        @endif


                        <!--end row-->
                    </form>
                </div>
            </div>
            <!--end of row-->
        </div>
        <!--end of container-->
    </section>
</div>
@endsection
