@extends('layouts.front2')

<style media="screen">
  .login {
    border: solid 1pt gray;
    padding: 30px;
    margin-top: 25%;
    border-radius: 1%;

  }
  .login-header {
    background-color: #5bc0be;
    border-radius: 1%;
    padding: 2px;
    margin: -25px -25px 0px -25px;
  }
</style>
@section('content')
<div class="container-fluid">
  <div class="col-xs-12 col-md-12">
    <div class="row">
        <div class="col-xs-10 col-xs-offset-1 col-md-4 col-md-offset-4">
          <div class="login">
                <div class="login-header">
                  <h4 class="text-center"> Waste Management Corporation Administrative Portal </h3> <p class="text-center text-muted"> Please enter the correct credentials to login </p>
                </div>
                    <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="control-label">E-Mail Address</label>

                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="control-label">Password</label>

                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                        </div>

                        <!-- <div class="form-group">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="remember"> Remember Me
                                    </label>
                                </div>
                        </div> -->

                        <div class="form-group">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>
                        </div>
                        <div class="form-group">
                          <a class="btn btn-link btn-block" href="{{ url('/password/reset') }}">
                              Forgot Your Password?
                          </a>
                        </div>

                    </form>
              </div>
        </div>
    </div>
  </div>
</div>
@endsection
