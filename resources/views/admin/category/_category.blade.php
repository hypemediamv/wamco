


@if(Session::get('edit_category') != null )
<div class='well'>
<h3>Edit {{$title}}</h3>



  {!! Form::open(['action' => 'CategoryController@updateOrDelete',  'method' => 'post']) !!}
  {{ csrf_field()  }}

<input type="hidden" name="category_type_id" value={{$category_type_id}}>
<input type="hidden" name="category_id" value= "{{Session::get('edit_category')->id}}">
  <div class="form-group">
    <label for="name">  Name:</label>
    <input type="text" name="name"class="form-control" required value="{{Session::get('edit_category')->title}}" placeholder="Enter a category name">
  </div>
  <div class="form-group">
    <label for="contact_address">Description:</label>
    <input type="text" name="description" class="form-control" required value="{{Session::get('edit_category')->description}}" placeholder="Enter a category description">
  </div>

  <button name-"update" type="submit" class="btn btn-default">Update</button>
  <button  name="delete" type="submit" class="btn btn-danger">Delete</button>

</div>

</div>
</div>

{{Form::close() }}




@else
<div class='well'>
  <h3>Add {{$title}}</h3>

  {{ Form::open(['action' => 'CategoryController@store'])  }}

  <input type="hidden" name="category_type_id" value={{$category_type_id}}>

    <div class="form-group">
      <label for="name">  Name:</label>
      <input type="text" name="name"class="form-control" required placeholder="Enter a category name">
    </div>
    <div class="form-group">
      <label for="contact_address">Description:</label>
      <input type="text" name="description" class="form-control" required placeholder="Enter a category description">
    </div>

    <button type="submit" class="btn btn-default">Submit</button>
</div>

</div>

</div>

{{Form::close() }}

@endif

@if($categories!= null)

  <table class="table">
    <thead>
    <th>Name</th><th>Description</th> <th >Edit</th>
    </thead>
    <tbody>
    @foreach($categories as $category)
      <tr>
        <td >{{$category->title}}  </a> </td>
        <td>{{$category->description}}</td>


        <td>
          <a href="{{ action('CategoryController@edit',$category->id)}}">  <button type="button" class="btn btn-success " style="display:inline-block;" >Edit</button> </a>

        </td>

      </tr>
    @endforeach
    </tbody>
  </table>
@endif
