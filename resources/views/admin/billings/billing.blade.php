@extends('layouts.minimal')
@section('content')

<h3>Add Billing </h3>

{!! Form::open(['action' => 'BillingController@store'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('billing_service_id')) has-error @endif">
    <label for="billing_service_id">Billing Service Name</label>
    {{ Form::select('billing_service_id',$billing_services->pluck('service_name','id'), null,['class' => 'form-control select2']) }}
    @if ($errors->has('billing_service_id')) <p class="help-block">{{ $errors->first('billing_service_id') }}</p> @endif

  </div>

  <div class="form-group @if ($errors->has('content')) has-error @endif">
    <label for="content">Content</label>
    <textarea class="form-control" name="content" id="summernote" required></textarea>
    @if ($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif

  </div>

  <div class="form-group">
      <label for="biiling_date">Billing Date</label>

      <div class="input-group date">
          <input type="date"

                 value="{{ (!empty($billing->billing_date) ? $billing->biiling_date : date('Y-m-d H:i:s')) }}"
                 name="biiling_date"
                 class="form-control" />
          <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
      </div>
  </div>

  <div class="form-group @if ($errors->has('billing_user_id')) has-error @endif">
    <label for="billing_user_id">Billing User</label>
    {{ Form::select('billing_user_id',$users->pluck('name','id'), null,['class' => 'form-control select2']) }}
    @if ($errors->has('billing_user_id')) <p class="help-block">{{ $errors->first('billing_user_id') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Save</button>

{!! Form::close() !!}

  @if(count($billings) > 0)

    <table class="table">
      <thead>
      <th>Billing Service</th><th>Billing Date</th><th>Billing User</th><th>Actions</th>
      </thead>
      <tbody>
      @foreach($billings as $row)
        <tr>
          <td >{{ $row->BillingService->service_name }}  </td>
          <td >{{ $row->billing_date }}  </td>
          <td >{{ $row->users->name }}  </td>
          <td>
            <a href="{{ action('BillingController@edit', $row->id) }}">
              <button class="btn btn-warning" > Edit </button>
            </a>
          </td>

          <td>

            {!! Form::open(['action' => ['BillingController@destroy', $row->id],  'method' => 'DELETE']) !!}
            {{ csrf_field()  }}
            <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button>
            {{Form::close() }}

          </td>

        </tr>
      @endforeach
      </tbody>

    </table>
    <div>
      @if($billings->lastPage() > 1)
          {!! $billings->render() !!}
      @endif
    </div>
  @endif

@endsection
