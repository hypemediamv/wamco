@extends('layouts.minimal')
@section('content')

<h3>Edit Billing </h3>

{!! Form::open(['action' => ['BillingController@update', $billing->id], 'method' => 'patch'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('billing_service_id')) has-error @endif">
    <label for="billing_service_id">Billing Service Name</label>
    {{ Form::select('billing_service_id',$billing_services->pluck('service_name','id'), (!empty($billing->BillingService) ? $billing->billing_service_id : null),['class' => 'form-control select2']) }}
    @if ($errors->has('billing_service_id')) <p class="help-block">{{ $errors->first('billing_service_id') }}</p> @endif

  </div>

  <div class="form-group @if ($errors->has('content')) has-error @endif">
    <label for="content">Content</label>
    <textarea class="form-control" name="content" id="summernote" required> {{ $billing->content }}</textarea>
    @if ($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif

  </div>

  <div class="form-group">
      <label for="biiling_date">Billing Date</label>

      <div class="input-group date">
          <input type="text"
                 id="publishedAt"
                 value="{{ (!empty($billing->billing_date) ? $billing->billing_date : date('Y-m-d H:i:s')) }}"
                 name="biiling_date"
                 class="form-control" />
          <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
      </div>
  </div>

  <div class="form-group @if ($errors->has('billing_user_id')) has-error @endif">
    <label for="billing_user_id">Name</label>
    {{ Form::select('billing_user_id',$users->pluck('name','id'), (!empty($billing->users) ? $billing->billing_user_id : null) ,['class' => 'form-control select2']) }}
    @if ($errors->has('billing_user_id')) <p class="help-block">{{ $errors->first('billing_user_id') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Update</button>

{!! Form::close() !!}

@endsection
