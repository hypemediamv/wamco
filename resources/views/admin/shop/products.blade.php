@extends('layouts.minimal')
@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
  <li class="active">{{$products_title}}</li>
</ol>

<h3>Add Product</h3>

@if($categories != '[]')
{{ Form::open(['action' => 'ProductController@store', 'files' => true])  }}


  <div class="form-group">
  <label for="sel1">Zone</label>
  {{ Form::select('shop_zone_id',$zones->pluck('name','id'), null,['class' => 'form-control', 'required' => 'required']) }}
  </div>

  <div class="form-group">
  <label for="sel1">Category</label>
  {{ Form::select('category_id',$categories->pluck('title','id'), null,['class' => 'form-control', 'required' => 'required']) }}
  </div>

  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" required placeholder="Enter a product name">
  </div>

  <div class="form-group">
    <label for="description">Description:</label>
    <input type="textarea" class="form-control"  name="description" required placeholder="Enter a description name">
  </div>

  <div class="form-group">
    <label for="product_images">Image(s)</label><span class="label label-info">Info! A Product can add multiple images</span>
    <input type="file" name="product_images[]" id="js_image2" multiple required/>
  </div>

  <div class="form-group">
    <label for="price">Price:</label>
    <input type="number" class="form-control"  name="price" required placeholder="Enter a product price">
  </div>


  <button type="submit" class="btn btn-default">Submit</button>

  {{ Form::close() }}

@else
<div class="alert alert-warning">
<p> Category must be created to Add Products </p>
</div>
@endif




@if($products!= null)
<br>
<ol class="breadcrumb">
  <li><a href="{{url('/admin/shop')}}">Dashboard</a></li>
  <li><a href="{{url('/admin/shop/products')}}">Products</a></li>
  @if($products_title != 'All Products')
  <li><a href="{{route('admin-products-all')}}"> All Products </a></li>
  @endif
  <li class="active">Recently Added</li>
</ol>


<div class="row">

  @foreach($products as $product)


    <div class="col-sm-2 col-lg-2 col-md-2">
        <div class="thumbnail">
        @if(count($product->product_images) > 1 )
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->


            <ol class="carousel-indicators">
              @foreach($product->product_images as $key => $product_image)

              <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>


              @endforeach
            </ol>


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="{{url('/').$product->product_images[0]->image->path}}" width="320" height="150">
              </div>
              <?php $product->product_images->shift(); ?>

              @foreach($product->product_images as $product_image)

              <div class="item">
                <img src="{{url('/').$product_image->image->path}}" width="320" height="150">
              </div>

              @endforeach

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


          @else
          <img src="{{url('/').$product->product_images[0]->image->path}}" width="320" height="150">

          @endif
            <div class="caption">
                <h4 class="pull-right">MVR: {{$product->price}}</h4>

                <h4><a href="{{route('admin-product',$product->id)}}">{{$product->name}}</a>
                </h4>

                <p>{{$product->description}}</p> <br>


            </div>
            <div class="ratings caption">



                <p>
                 <b>{{$product->category->title}}</b>

                </p>

                <p class="pull-right">
                  {!! Form::open(['action' => ['ProductController@destroy', $product->id],  'method' => 'post']) !!}
                  {{ csrf_field()  }}
                  <button type="submit" class="btn btn-default btn-block" style="display:inline-block">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trash
</button>
{{Form::close() }}
</p>
{{$product->updated_at->diffForHumans()}}

            </div>
        </div>
    </div>
    @endforeach


</div>
  @endif

  {{ $products->links() }}



@endsection
