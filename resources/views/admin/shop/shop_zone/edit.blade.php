@extends('layouts.minimal')
@section('content')



<h3>Edit Shop Zone</h3>

{!! Form::open(['action' => ['ShopZoneController@update', $shop_zone->id], 'method' => 'patch'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('service_name')) has-error @endif">
    <label for="name">Name:</label>
    <input type="text" class="form-control" name="name" value="{{ $shop_zone->name }}" required>
    @if ($errors->has('service_name')) <p class="help-block">{{ $errors->first('service_name') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Update</button>

{!! Form::close() !!}


@endsection
