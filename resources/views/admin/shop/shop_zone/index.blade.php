@extends('layouts.minimal')
@section('content')


<h3>Add Shop Zone </h3>

{{ Form::open(['action' => 'ShopZoneController@store'])  }}


  <div class="form-group @if ($errors->has('name')) has-error @endif">
    <label for="name">  Name:</label>
    <input type="text" name="name"class="form-control" required>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}


@if($shop_zones!= null)

  <table class="table">
    <thead>
    <th>Shop Zone</th><th >Actions</th>
    </thead>
    <tbody>
    @foreach($shop_zones as $shop_zone)
      <tr>
        <td >{{$shop_zone->name}}   </td>

        <td>
          <a href="{{ action('ShopZoneController@edit',$shop_zone->id) }}">
            <button class="btn btn-warning" > Edit </button>
          </a>
        </td>

        <td>
          {!! Form::open(['action' => ['ShopZoneController@destroy', $shop_zone->id],  'method' => 'DELETE']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger" style="display:inline-block;" >Delete</button>
          {{Form::close() }}

        </td>

      </tr>
    @endforeach
    </tbody>
  </table>
@endif

@endsection
