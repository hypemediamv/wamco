@extends('layouts.minimal')
@section('content')

<ol class="breadcrumb">
  <li><a href="{{url('/admin/shop')}}">Dashboard</a></li>
  <li><a href="{{url('/admin/shop/products')}}">Products</a></li>
  @if($products_title != 'All Products')
  <li><a href="{{route('admin-products-all')}}"> All Products </a></li>
  @endif
  <li class="active">{{$products_title}}</li>
</ol>


<div class="well well-sm">
  <h4>Browse</h4>
  <div class="row">

<div class="col-lg-3">
  <h5>Search</h5>

<div class="input-group">
      <input type="text" class="form-control" placeholder="Search for...">
      <span class="input-group-btn">
        <button class="btn btn-default" type="button">Go!</button>
      </span>
    </div>
</div>
<div class="col-lg-9">


<div class="btn-group" role="group" aria-label="...">
  <h5>By Category</h5>

<a href="{{route('admin-products-all')}}">   <button type="button" class="btn btn-default">  All </button> </a>

@foreach($categories as $category)
 <a href="{{route('admin-products-category',$category->id)}}"> <button type="button" class="btn btn-default">   {{$category->title}}</button></a>
@endforeach
</div>


<div class="btn-group" role="group" aria-label="...">
  <h5>By Zone</h5>

@foreach($shop_zones as $shop_zone)
 <a href="{{route('admin-products-zone',$shop_zone->id)}}"> <button type="button" class="btn btn-default">   {{$shop_zone->name}}</button></a>
@endforeach
</div>
</div>
</div>



</div>

<h1>{{$products_title}} </h1>

@if($products!= null)

<div class="row">

  @foreach($products as $product)


    <div class="col-sm-2 col-lg-2 col-md-2">
        <div class="thumbnail">
        @if(count($product->product_images) > 1 )
          <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->


            <ol class="carousel-indicators">
              @foreach($product->product_images as $key => $product_image)

              <li data-target="#myCarousel" data-slide-to="{{$key}}"></li>


              @endforeach
            </ol>


            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
              <div class="item active">
                <img src="{{url('/').$product->product_images[0]->image->path}}" width="420" height="150">
              </div>
              <?php $product->product_images->shift(); ?>

              @foreach($product->product_images as $product_image)

              <div class="item">
                <img src="{{url('/').$product_image->image->path}}" width="420" height="150">
              </div>

              @endforeach

            </div>

            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
              <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
              <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>


          @else
          <img src="{{url('/').$product->product_images[0]->image->path}}" width="420" height="150">

          @endif
            <div class="caption">
                <h4 class="pull-right">MVR: {{$product->price}}</h4>

                <h4><a href="{{route('admin-product',$product->id)}}">{{$product->name}}</a>
                </h4>

                <p>{{$product->description}}</p> <br>


            </div>
            <div class="ratings caption">



                <p>
                  <b>{{$product->category->title}}</b>
                </p>

                <p class="pull-right">
                  {!! Form::open(['action' => ['ProductController@destroy', $product->id],  'method' => 'post']) !!}
                  {{ csrf_field()  }}
                  <button type="submit" class="btn btn-default btn-block" style="display:inline-block">
                  <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trash
</button>
{{Form::close() }}
</p>
{{$product->updated_at->diffForHumans()}}

            </div>
        </div>
    </div>
    @endforeach


</div>
  @endif

  {{ $products->links() }}



@endsection
