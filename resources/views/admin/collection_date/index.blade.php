@extends('layouts.minimal')
@section('content')




<h3>Add Collection Date </h3>

{{ Form::open(['action' => 'CollectionDateController@store'])  }}


  <div class="form-group @if ($errors->has('name')) has-error @endif">
    <label for="name">  Name:</label>
    <input type="text" name="name"class="form-control" required>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif

  </div>

  <div class="form-group">
    <label for="name">  Details:</label>
    <input type="text" name="details"class="form-control" required>
  </div>

  <div class="form-group @if ($errors->has('content')) has-error @endif">
    <label for="name">  Content:</label>
  <textarea id="summernote"  name="content" required></textarea>
  @if ($errors->has('content')) <p class="help-block">{{ $errors->first('content') }}</p> @endif
</div>


    <div class="form-group @if ($errors->has('collection_date_site_id')) has-error @endif">
    <label for="sel1">Site</label>
    {{ Form::select('collection_date_site_id',$collection_date_sites->pluck('name','id'), null,['class' => 'form-control']) }}
    @if ($errors->has('collection_date_site_id')) <p class="help-block">{{ $errors->first('collection_date_site_id') }}</p> @endif

    </div>

  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}


@if($collection_dates!= null)

  <table class="table">
    <thead>
    <th>Name</th><th>Site</th><th>Zone</th><th>Content</th><th >Delete</th>
    </thead>
    <tbody>
    @foreach($collection_dates as $collection_date)
      <tr>
        <td >{{$collection_date->name}}   </td>
        <td >@if($collection_date->collection_date_site!= null){{$collection_date->collection_date_site->name}} @endif </td>
        <td >@if($collection_date->collection_date_site!= null){{$collection_date->collection_date_site->collection_date_zone->name}}   @endif</td>

        <td >{!! $collection_date->content !!}   </td>

        <td>
          {!! Form::open(['action' => ['CollectionDateController@destroy', $collection_date->id],  'method' => 'DELETE']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button>
          {{Form::close() }}

        </td>

      </tr>
    @endforeach
    </tbody>
  </table>
@endif

@endsection
