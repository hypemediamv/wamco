@extends('layouts.minimal')
@section('content')




<h3>Add Collection Date Zone</h3>

{{ Form::open(['action' => 'CollectionDateZoneController@store'])  }}


  <div class="form-group @if ($errors->has('name')) has-error @endif">
    <label for="name">  Name:</label>
    <input type="text" name="name"class="form-control" required>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
  </div>

  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}


@if($collection_date_zones!= null)

  <table class="table">
    <thead>
    <th>Name</th><th >Delete</th>
    </thead>
    <tbody>
    @foreach($collection_date_zones as $zone)
      <tr>
        <td >{{$zone->name}}  </a> </td>


        <td>
          {!! Form::open(['action' => ['CollectionDateZoneController@destroy', $zone->id],  'method' => 'DELETE']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button> </a>
          {{Form::close() }}

        </td>

      </tr>
    @endforeach
    </tbody>
  </table>
@endif

@endsection
