@extends('layouts.minimal')
@section('content')




<h3>Add Collection Date Site</h3>

{{ Form::open(['action' => 'CollectionDateSiteController@store'])  }}


  <div class="form-group @if ($errors->has('name')) has-error @endif">
    <label for="name">  Name:</label>
    <input type="text" name="name"class="form-control" required>
    @if ($errors->has('name')) <p class="help-block">{{ $errors->first('name') }}</p> @endif
  </div>


    <div class="form-group @if ($errors->has('collection_date_zone_id')) has-error @endif">
    <label for="sel1">Zone</label>
    {{ Form::select('collection_date_zone_id',$collection_date_zones->pluck('name','id'), null,['class' => 'form-control']) }}
    @if ($errors->has('collection_date_zone_id')) <p class="help-block">{{ $errors->first('collection_date_zone_id') }}</p> @endif

    </div>

  <button type="submit" class="btn btn-default">Submit</button>

</div>

</div>

{{Form::close() }}


@if($collection_date_sites!= null)

  <table class="table">
    <thead>
    <th>Name</th><th>Zone</th><th >Delete</th>
    </thead>
    <tbody>
    @foreach($collection_date_sites as $site)
      <tr>
        <td >{{$site->name}}  </a> </td>
        <td> @if($site->collection_date_zone!= null){{$site->collection_date_zone->name}}@endif </td>


        <td>
          {!! Form::open(['action' => ['CollectionDateSiteController@destroy', $site->id],  'method' => 'DELETE']) !!}
          {{ csrf_field()  }}
          <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button>
          {{Form::close() }}


        </td>

      </tr>
    @endforeach
    </tbody>
  </table>
@endif

@endsection
