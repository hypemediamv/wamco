@extends('layouts.minimal')

@section('content')
        <div class="panel panel-primary">
            <div class="panel-heading">{{ $title }}</div>
            <div class="panel-body">
                <ul class="list-group">
                    @foreach($tags as $tag)
                        <li class="list-group-item">
                                {{ $tag->tag }}
                            </a>&nbsp;
                            <a href="{{ route('admin-tags-remove', ['tag_id' => $tag->id]) }}" onclick="return confirm('You want to remove this tag?');">
                                <span class="pull-right text-danger"><i class="fa fa-trash"></i></span></a>
                        </li>
                    @endforeach
                </ul>
            </div>

        </div>
@stop