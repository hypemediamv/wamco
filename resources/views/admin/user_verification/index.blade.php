@extends('layouts.minimal')
@section('content')

<h1>User Verification</h1>


  @if($users!= null)

    <table class="table">
      <thead>
       <th> Username  </th> <th>Firt Name</th> <th>Last Name </th> <th> Active Status </th><th>Delete</th>
      </thead>
      <tbody>
      @foreach($users as $user)
        <tr>
          <td>{{$user->name}} </td>
          <td >{{$user->first_name or ''}}  </td>
          <td>
            {{$user->last_name or ''}}
          </td>
          <td>

            @if($user->verified == 0 )
              <b>Inactive </b>
            @else
              <b> Active </b>

            @endif

          </td>

          <td>
            {!! Form::open(['action' => 'UserVerificationController@store',  'method' => 'post']) !!}
            {!! csrf_field()  !!}

            <input type="hidden" name="user_id" value="{{$user->id}}">
            <button type="submit" class="btn btn-default btn-block" style="display:inline-block">
            <span class="glyphicon glyphicon-tick" aria-hidden="true"></span> Inactive/Active
            </button>
            {!! Form::close() !!}
          </td>



        </tr>
      @endforeach
      </tbody>
    </table>
  @endif


@endsection
