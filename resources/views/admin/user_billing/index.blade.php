@extends('layouts.minimal')
@section('content')



<h3>Add Billing Services</h3>

{!! Form::open(['action' => 'BillingServiceController@store'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('service_name')) has-error @endif">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="service_name" >
    @if ($errors->has('service_name')) <p class="help-block">{{ $errors->first('service_name') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Submit</button>

  {!! Form::close() !!}

  @if($billings!= null)

    <table class="table" width="100%">
      <thead>
      <th>Name</th><th>Actions</th>
      </thead>
      <tbody>
      @foreach($billings as $billing)
        <tr>
          <td >{{$billings->service_name}}  </td>

          <td>

            {!! Form::open(['action' => ['BillingServiceController@destroy', $billing->id],  'method' => 'DELETE']) !!}
            {{ csrf_field()  }}
            <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button>
            {{Form::close() }}

          </td>

        </tr>
      @endforeach
      </tbody>


    </table>
    <div>

    </div>
  @endif


@endsection
