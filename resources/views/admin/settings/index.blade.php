@extends('layouts.minimal')

@section('content')

        <h1>{{ $title }}</h1>
        <br/>
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-3">
                @include('admin.partials.settings-menu')
            </div>
            <div class="col-sm-12 col-md-12 col-lg-9">
                <h2>Current Settings</h2>
            </div>
        </div>

@stop