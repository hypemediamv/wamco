@extends('layouts.minimal')

@section('content')
        <form action="{{ $save_url }}" enctype="multipart/form-data" method="post" onsubmit="var text = document.getElementById('summernote').value; if(text.length < 100) { alert('content must be greater than 100 characters');  return false; } return true;">
            {!! csrf_field() !!}
            <div class="row">

                    <div class="panel panel-primary">
                        <div class="panel-heading">{{ $title }}</div>
                        <div class="panel-body">
                            <div class="col-sm-12 col-md-12 col-lg-12">
                                <div class="form-group">
                                    <label for="title">Title</label>
                                    <input id="title" type="text" value="{{ $post->title or old('title') }}" class="form-control" name="title" placeholder="Enter a post title" required>
                                </div>
                                @if(!is_null($post))
                                    <div class="well">
                                        <div>
                                            <span class="text-muted">URL:</span>
                                            <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank">
                                                {{ route('view', ['slug' => $post->slug]) }}
                                            </a>
                                        </div>
                                        <div>
                                            <hr />
                                            <div>
                                                <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank"
                                                   class="btn btn-sm btn-info">View</a>
                                                <a href="{{ route('admin-post-edit', ['post_id' => $post->id]) }}"
                                                   class="btn btn-sm btn-warning">Edit</a>
                                                @if($post->status == 'active')
                                                    <a href="{{ route('admin-post-to-draft', ['post_id' => $post->id]) }}"
                                                       class="btn btn-sm btn-warning">To Draft</a>
                                                @else
                                                    <a href="{{ route('admin-post-to-active', ['post_id' => $post->id]) }}"
                                                       class="btn btn-sm btn-success">Publish</a>
                                                @endif
                                                @if($post->status != 'deleted')
                                                    <a href="{{ route('admin-post-to-deleted', ['post_id' => $post->id]) }}"
                                                       class="btn btn-sm btn-danger">Delete</a>
                                                @else
                                                    <a href="{{ route('admin-post-to-draft', ['post_id' => $post->id]) }}"
                                                       class="btn btn-sm btn-success">Recover</a>
                                                    <a href="{{ route('admin-post-to-delete-forever', ['post_id' => $post->id]) }}"
                                                       class="btn btn-sm btn-danger">Delete Forever</a>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                @endif
                                <div class="form-group">
                                    <label for="content">Content</label>
                                    <textarea id="summernote" name="content" minlength="100" required>{!! $post->content or old('content')  !!} </textarea>
                                </div>
                                <div class="form-group">
                                    <label for="category">Categories</label>
                                    <span><a href="{{ route('postCategory') }}" class="btn btn-link btn-primary"> Add New Categories</a></span>
                                    <select name="category_id" id="category" class="form-control">
                                    @foreach($categories as $category)
                                            <option value="{{ $category->id }}"
                                                    {{ (!empty($post) && $post->category_id == $category->id) ? 'selected' : '' }}>
                                                {{ $category->title }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="featuredImg">Featured Image</label> <span class="label label-info">info!. image dimension =  width:1600 pixels & height 1067 pixels</span>
                                    @if(!empty($post) && !empty($post->image_id))
                                        <br />
                                        <img src="{{ $post->image->path }}" max-width="50%" alt="">
                                    @endif
                                    <input type="file" id="featuredImg" name="featuredImg" required class="" >
                                </div>
                                <div class="form-group">
                                    <label for="status">Status</label>
                                    <select name="status" id="status" class="form-control">
                                        <option value="draft" {{ (!empty($post) && $post->status == 'draft') ? 'selected' : '' }}>Draft</option>
                                        <option value="active" {{ (!empty($post) && $post->status == 'active') ? 'selected' : '' }}>Active</option>
                                        <optgroup label="Additional">
                                            <option value="deleted" {{ (!empty($post) && $post->status == 'deleted') ? 'selected' : '' }}>Deleted</option>
                                        </optgroup>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="publishedAt">Published at</label>

                                    <div class="input-group date">
                                        <input type="text"
                                               id="publishedAt"
                                               value="{{ (!empty($post) ? $post->published_at : date('Y-m-d H:i:s')) }}"
                                               name="published_at"
                                               class="form-control" />
                                        <span class="input-group-addon"><span class="fa fa-calendar"></span></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="tags">Tags <em class="text-muted">Optional</em></label>
                                    <input type="text"
                                           id="tags"
                                           name="tags"
                                           value="{{ (!empty($post) ? $post->tags->implode('tag', ', ') : old('tags'))  }}"
                                           class="form-control"
                                           placeholder="Add more tags by pressing tabs">

                                    <div class="well well-sm tags-list">
                                        <a class="btn btn-link" onclick="$('#popularTags').slideToggle(100);">Click to see Popular Tags </a>
                                        <div id="popularTags" style="display:none;">
                                            @foreach($tags as $tag)
                                                <a href="#{{ $tag->tag }}" class="add-tag" data-tag="{{ $tag->tag }}">{{ $tag->tag }}</a>
                                            @endforeach
                                        </div>
                                    </div>
                                </div>
                                <hr/>
                                <div>
                                    <input type="submit" value="Save" class="btn btn-sm btn-success" >
                                    <a href="{{ route('admin-posts') }}" class="btn btn-sm btn-default">Cancel</a>
                                </div>
                            </div>

                        </div>

                    </div>


            </div>
        </form>

@stop
