<tr class="{{ in_array($post->status, ['draft', 'deleted']) ? 'active' : '' }}">
    <td>
        <div>
            {{ $post->id }}
        </div>
    </td>
    <td>
        <div >
            <a href="{{ route('admin-post-edit', ['post_id' => $post->id]) }}">{!! highlight_str($post->title, $q) !!}</a>
        </div>
        <div class="row">
            <div class="">
                <div >
                    <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank"
                       class="btn btn-success btn-sm">View</a>
                    <a href="{{ route('admin-post-edit', ['post_id' => $post->id]) }}"
                       class="btn btn-warning btn-sm">Edit</a>
                    @if($post->status == 'active')
                        <a href="{{ route('admin-post-to-draft', ['post_id' => $post->id]) }}"
                           class="btn btn-warning btn-sm">To Draft</a>
                    @else
                        <a href="{{ route('admin-post-to-active', ['post_id' => $post->id]) }}"
                           class="btn btn-success btn-sm">Publish</a>
                    @endif
                    @if($post->status != 'deleted')
                        <a href="{{ route('admin-post-to-deleted', ['post_id' => $post->id]) }}"
                           class="btn btn-danger btn-sm">Delete</a>
                    @else
                        <a href="{{ route('admin-post-to-draft', ['post_id' => $post->id]) }}"
                           class="btn btn-success btn-sm">Recover</a>
                        <a href="{{ route('admin-post-to-delete-forever', ['post_id' => $post->id]) }}"
                           class="btn btn-sm btn-danger">Delete Forever</a>
                    @endif
                </div>
            </div>

        </div>
    </td>
    <td>
        <div >
            <div class="post-category text-muted">
                <div class="dropdown">
                    <a id="drop3" class="dropdown-toggle btn btn-xs btn-{{ ($post->category_id == '1' ? 'danger' : 'info') }}"
                       aria-expanded="false" aria-haspopup="true"
                       role="button" data-toggle="dropdown" href="#">
                        {{ $post->category->title }}
                        <span class="caret"></span>
                    </a>
                    <ul class="dropdown-menu" aria-labelledby="dLabel">
                        @foreach($categories as $category)
                            @if($category->id == $post->category_id) @endif
                            <li><a href="{{ route('admin-post-to-category', ['post_id' => $post->id, 'category_id' => $category->id]) }}">{{ $category->title }}</a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </td>
    <td>
        <div>
            {{ date('Y.m.d H:i', strtotime($post->published_at)) }}
        </div>
        <div>
            <small class="text-muted">{{ hdate($post->published_at) }}</small>
        </div>
    </td>
    <td>
        {{ ucwords($post->status) }}
    </td>
</tr>
