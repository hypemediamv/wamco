@extends('layouts.minimal')

@section('content')
    <div class="panel panel-primary">
        <div class="panel-heading"> Manage {{ $title }}</div>
        <div class="panel-body">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-12">
                    <a href="{{ route('admin-posts-new') }}" class="btn btn-lg btn-success"><i class="fa fa-plus"> New Post</i></a>
                    <br />

                    <br />
                    <ul class="nav nav-tabs nav-pills">
                        <li class="{{ $status == 'all' ? 'active' : '' }}">
                            <a href="{{ route('admin-posts') }}">All Posts</a>
                        </li>
                        <li class="{{ $status == 'draft' ? 'active' : '' }}">
                            <a href="{{ route('admin-posts', ['status' => 'draft']) }}">Drafts</a>
                        </li>
                        <li class="{{ $status == 'deleted' ? 'active' : '' }}">
                            <a href="{{ route('admin-posts', ['status' => 'deleted']) }}">Deleted</a>
                        </li>
                    </ul>
                    <div class="panel panel-info">
                        <div class="panel-body">
                            <table class="table table-hover">
                                <thead class="text-info">
                                <tr>
                                    <th>ID</th>
                                    <th>Post</th>
                                    <th>Category</th>
                                    <th>Published At</th>
                                    <th>Status</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($posts as $post)
                                    @include('admin.posts.partials._post')
                                @endforeach
                                </tbody>
                            </table>
                            <div>
                                @if($posts->lastPage() > 1)
                                    {!! $posts->render() !!}
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>
@stop
