@extends('layouts.minimal')
@section('content')
    <ul class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li class="active">Mapped Services</li>
        <li><a class="btn btn-sm btn-primary" href="{{route('admin-mapped-location-create')}}"><i class="fa fa-plus-circle"></i> Add New</a></li>
    </ul>

    <div class="panel panel-info">
        <div class="panel-body">
            <table class="table table-hover">
                <thead class="text-info">
                <tr>
                    <th>Name</th>
                    <th>Address</th>
                    <th>Description</th>
                    <th>Latitude</th>
                    <th>Longitude</th>
                    <th>Contact</th>
                    <th>Actions</th>

                </tr>
                </thead>
                <tbody>
                @if (count($mapped_location) >0)
                @foreach($mapped_location as $location)
                    <tr>
                        <td>{{ $location->name }}</td>
                        <td>{{ $location->address }}</td>
                        <td>{{ $location->description }}</td>
                        <td>{{ $location->latitude }}</td>
                        <td>{{ $location->longitude }}</td>
                        <td>{{ $location->contact_name }}<br>{{ $location->number }}<br> {{ $location->email }}<br>
                        </td>
                        <td>
                            {{--<a href="{{ route('admin-mapped-location-delete', ['location_id' => $location->id]) }}" class="btn btn-warning btn-block" onclick="return confirm('Are sure you want to remove the location?');">--}}
                                {{--Edit--}}
                            {{--</a>--}}
                            <a href="{{ route('admin-mapped-location-delete', ['location_id' => $location->id]) }}" class="btn btn-danger btn-block" onclick="return confirm('Are sure you want to remove the location?');">
                                Delete
                            </a>
                        </td>
                    </tr>
                @endforeach
                    @else
                    <tr>
                        <td colspan="8">
                            <div class="alert alert-info">
                                <strong>Info!</strong> Location table is empty!.
                            </div>
                        </td>
                    </tr>
                @endif
                </tbody>
            </table>
            <div>
                @if($mapped_location->lastPage() > 1)
                    {!! $mapped_location->render() !!}
                @endif
            </div>
        </div>
    </div>
@endsection