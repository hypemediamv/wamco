@extends('layouts.minimal')

<style>
    /* Always set the map height explicitly to define the size of the div
     * element that contains the map. */
    #map {
        width: 100%;
        height: 445px;
    }

    /* Optional: Makes the sample page fill the window. */

    #description {
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
    }

    #infowindow-content .title {
        font-weight: bold;
    }

    #infowindow-content {
        display: none;
    }

    #map #infowindow-content {
        display: inline;
    }

    .pac-controls label {
        font-family: Roboto;
        font-size: 13px;
        font-weight: 300;
    }

    #pac-input {
        background-color: #fff;
        font-family: Roboto;
        font-size: 15px;
        font-weight: 300;
        margin-right: 0px;
        padding: 0 11px 0 13px;
        margin-top: 8px;
        /*text-overflow: ellipsis;*/
        width: 320px;
    }

</style>
@section('content')
    <ul class="breadcrumb">
        <li><a href="{{url('/admin/dashboard')}}">Dashboard</a></li>
        <li>Mapped Services</li>
        <li class="active">Create</li>
        <li><a class="btn btn-sm btn-default" href="{{route('admin-mapped-locations')}}"><i
                        class="fa fa-long-arrow-left"></i> Back</a></li>
    </ul>

    <form action="{{ route('admin-mapped-location-store') }}" enctype="multipart/form-data" method="post">
        {!! csrf_field() !!}
        <div class="row">
            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-info">
                    <div class="panel-heading">Location information</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="name">Location Name</label>
                            <input id="name" type="text" value="{{  old('name') }}" class="form-control" name="name"
                                   required placeholder="Enter a location title name">
                        </div>

                        <div class="form-group">
                            <label for="description">Location Description</label>
                            <textarea class="form-control" rows="8"  name="description" placeholder="Enter a location description">{{  old('description') }}</textarea>
                        </div>

                        <div class="form-group">
                            <label for="contact_name">Location Contact Name</label>
                            <input id="contact_name" type="text" value="{{  old('contact_name') }}" class="form-control"
                                   name="contact_name" placeholder="Enter a location contact name">
                        </div>

                        <div class="form-group">
                            <label for="number">Location Number</label>
                            <input id="number" type="number" value="{{  old('number') }}" class="form-control"
                                   name="number" placeholder="Enter a location contact number">
                        </div>

                        <div class="form-group">
                            <label for="email">Location Email</label>
                            <input id="email" type="email" value="{{  old('email') }}" class="form-control"
                                   name="email" placeholder="Enter a location contact email">
                        </div>

                    </div>

                </div>

            </div>

            <div class="col-sm-6 col-md-6 col-lg-6">
                <div class="panel panel-info">
                    <div class="panel-heading">Location Address</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="address">Location Address</label>
                            <input id="pac-input"
                                   type="text"
                                   value="Male', Malé, Male, Maldives"
                                   class="form-control controls" name="address" placeholder="Search the places"
                                   required>
                        </div>

                        <div id="map"></div>

                        <div class="form-group">
                            <input type="hidden" id="lat" class="form-control" name="latitude" required>
                        </div>

                        <div class="form-group">
                            <input type="hidden" id="long" class="form-control" name="longitude" required>
                        </div>
                    </div>

                </div>

            </div>


        </div>

        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12">
                <div class="panel panel-info">
                    <div class="panel-heading">Location image</div>
                    <div class="panel-body">
                        <div class="form-group">
                            <label for="locationImage">Location Image</label> <span class="label label-info">Preferred image dimension size = 800 x 800</span>
                            <input type="file" id="featuredImg" name="locationImage" class="">
                        </div>

                        <div>
                            <input type="submit" value="Save" class="btn btn-sm btn-primary">
                            <a href="{{ route('admin-mapped-locations') }}" class="btn btn-sm btn-default">Cancel</a>
                        </div>

                    </div>

                </div>

            </div>


        </div>


    </form>

    <script>
        // This example adds a search box to a map, using the Google Place Autocomplete
        // feature. People can enter geographical searches. The search box will return a
        // pick list containing a mix of places and predicted search terms.

        // This example requires the Places library. Include the libraries=places
        // parameter when you first load the API. For example:
        // <script src="https://maps.googleapis.com/maps/api/js?key=YOUR_API_KEY&libraries=places">

        function initAutocomplete() {
            var map = new google.maps.Map(document.getElementById('map'), {
                center: {lat: 4.1746503, lng: 73.5099285},
                zoom: 15,
                mapTypeId: 'roadmap',
                disableDoubleClickZoom: true,
                scrollwheel: false
            });
            //setting latitude and longitude

            document.getElementById('lat').value = map.center.lat();
            document.getElementById('long').value = map.center.lng();


            // Create the search box and link it to the UI element.
            var input = document.getElementById('pac-input');
            var searchBox = new google.maps.places.SearchBox(input);
            map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

            // Bias the SearchBox results towards current map's viewport.
            map.addListener('bounds_changed', function () {
                searchBox.setBounds(map.getBounds());
            });

            var markers = [];
            // Listen for the event fired when the user selects a prediction and retrieve
            // more details for that place.

            searchBox.addListener('places_changed', function () {
                var places = searchBox.getPlaces();

                if (places.length == 0) {
                    return;
                }

                // Clear out the old markers.
                markers.forEach(function (marker) {
                    marker.setMap(null);
                });
                markers = [];

                // For each place, get the icon, name and location.
                var bounds = new google.maps.LatLngBounds();
                places.forEach(function (place) {
                    if (!place.geometry) {
                        console.log("Returned place contains no geometry");
                        return;
                    }
                    var icon = {
                        url: place.icon,
                        size: new google.maps.Size(71, 71),
                        origin: new google.maps.Point(0, 0),
                        anchor: new google.maps.Point(17, 34),
                        scaledSize: new google.maps.Size(25, 25)
                    };

                    // Create a marker for each place.
                    markers.push(new google.maps.Marker({
                        map: map,
                        icon: icon,
                        title: place.name,
                        position: place.geometry.location
                    }));

                    if (place.geometry.viewport) {
                        // Only geocodes have viewport.
                        bounds.union(place.geometry.viewport);
                    } else {
                        bounds.extend(place.geometry.location);
                    }

                    //setting latitude and longitude after search completed
                    document.getElementById('lat').value = place.geometry.location.lat();
                    document.getElementById('long').value = place.geometry.location.lng();

                });
                map.fitBounds(bounds);
            });
        }

    </script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyCMcC-1_ETR8KQpRhv9nrxNoAPjT7WDy_I&libraries=places&callback=initAutocomplete"
            async defer></script>
@endsection
