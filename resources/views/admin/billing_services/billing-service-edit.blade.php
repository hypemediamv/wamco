@extends('layouts.minimal')
@section('content')



<h3>Edit Billing Services</h3>

{!! Form::open(['action' => ['BillingServiceController@update', $billing_services->id], 'method' => 'patch'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('service_name')) has-error @endif">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="service_name" value="{{ $billing_services->service_name }}" required>
    @if ($errors->has('service_name')) <p class="help-block">{{ $errors->first('service_name') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Update</button>

{!! Form::close() !!}


@endsection
