@extends('layouts.minimal')
@section('content')



<h3>Add Billing Services</h3>

{!! Form::open(['action' => 'BillingServiceController@store'])  !!}
{!! csrf_field()  !!}


  <div class="form-group @if ($errors->has('service_name')) has-error @endif">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="service_name" placeholder="Enter service name" required>
    @if ($errors->has('service_name')) <p class="help-block">{{ $errors->first('service_name') }}</p> @endif

  </div>

  <button type="submit" class="btn btn-default">Save</button>

{!! Form::close() !!}

  @if(count($billings) > 0)

    <table class="table">
      <thead>
      <th>Service Name</th><th>Actions</th>
      </thead>
      <tbody>
      @foreach($billings as $row)
        <tr>
          <td >{{ $row->service_name }}  </td>

          <td>
            <a href="{{ action('BillingServiceController@edit', $row->id) }}">
              <button class="btn btn-warning" > Edit </button>
            </a>
          </td>

          <td>
            {!! Form::open(['action' => ['BillingServiceController@destroy', $row->id],  'method' => 'DELETE']) !!}
            {{ csrf_field()  }}

            <button type="submit" class="btn btn-danger " style="display:inline-block;" >Delete</button>
            {{Form::close() }}

          </td>

        </tr>
      @endforeach
      </tbody>

    </table>
    <div>
      @if($billings->lastPage() > 1)
          {!! $billings->render() !!}
      @endif
    </div>
  @endif


@endsection
