@extends('layouts.minimal')
@section('content')



<h1>Add Waste Wizard Data</h1>

{!! Form::open(['action' => 'WasteWizardDataController@store', 'files' => true])  !!}
{!! csrf_field()  !!}


  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" required>
  </div>

  <div class="form-group">
    <label for="description">Description:</label>
    <input type="textarea" class="form-control"  name="description" required>
  </div>

  <div class="form-group">
  <label for="sel1">Waste Wizard Bin</label>
  {!! Form::select('bin_id',$bins->pluck('name','id'), null,['class' => 'form-control']) !!}
  </div>


  <button type="submit" class="btn btn-default">Submit</button>

  {!! Form::close() !!}


  @if($datas!= null)

    <table class="table">
      <thead>
      <th>Name</th><th>Description</th><th>Bin </th><th>Delete</th>
      </thead>
      <tbody>
      @foreach($datas as $data)
        <tr>
          <td >{{$data->name}}  </a> </td>
          <td>{{$data->description}}</td>
          <td>

            @if($data->bin!= null)
            {{$data->bin->name}}
            @endif
          </td>

          <td>
            {!! Form::open(['action' => ['WasteWizardDataController@destroy', $data->id],  'method' => 'DELETE']) !!}
            {!! csrf_field()  !!}

            <input type="hidden" name="data_id" value="{{$data->id}}">
            <button type="submit" class="btn btn-default btn-block" style="display:inline-block">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trash
            </button>
            {!! Form::close() !!}
          </td>



        </tr>
      @endforeach
      </tbody>
    </table>
  @endif


@endsection
