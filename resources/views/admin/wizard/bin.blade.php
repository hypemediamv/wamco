@extends('layouts.minimal')
@section('content')



<h1>Add Waste Wizard Bin</h1>

{!! Form::open(['action' => 'WasteWizardDataBinController@store', 'files' => true])  !!}
{!! csrf_field()  !!}


  <div class="form-group">
    <label for="name">Name</label>
    <input type="text" class="form-control" name="name" required>
  </div>


  <div class="form-group">
  <label for="sel1">Bin Image</label>
  <input type="file" name="image" id="js_image2"  required/>
  </div>


  <button type="submit" class="btn btn-default">Submit</button>

  {!! Form::close() !!}


  @if($bins!= null)

    <table class="table">
      <thead>
      <th>Name</th> <th>Bin </th><th>Delete</th>
      </thead>
      <tbody>
      @foreach($bins as $bin)
        <tr>
          <td >{{$bin->name}}  </a> </td>
          <td>

 <img src="{{url('/').$bin->image->path}}" width="100px;">

          </td>
          <td>

          </td>

          <td>
            {!! Form::open(['action' => ['WasteWizardDataBinController@destroy', $bin->id],  'method' => 'DELETE']) !!}
            {!! csrf_field()  !!}

            <input type="hidden" name="id" value="{{$bin->id}}">
            <button type="submit" class="btn btn-default btn-block" style="display:inline-block">
            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span> Trash
            </button>
            {!! Form::close() !!}
          </td>



        </tr>
      @endforeach
      </tbody>
    </table>
  @endif


@endsection
