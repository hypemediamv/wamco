@extends('layouts.front2')


@section('content')
<style>
  a { color: #337ab7;}
</style>
  <a id="start"></a>
  <div class="main-container">
      <section class="text-center imagebg" data-overlay="4">
          <div class="background-image-holder"> <img alt="background" src="{{url('/')}}/img/hero-3.jpg"> </div>
          <div class="container">
              <div class="row">
                  <div class="col-sm-10 col-md-8">
                      <h1>News and Events</h1>
                      <p class="lead"> Latest news and announcements </p>
                  </div>
              </div>
          </div>
      </section>

        <div class="main-container">
            <section class="space--sm">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12 col-md-8">
                            <div class="masonry">
                                <div class="masonry-filter-container">
                                    <span>Category:</span>
                                    <div class="masonry-filter-holder">
                                        <div class="masonry__filters" data-filter-all-text="All Categories"></div>
                                    </div>
                                </div>
                                <hr>
                                <div class="row">
                                    <div class="masonry__container">
                                        <div class="masonry__item col-sm-6"></div>
                                    @foreach($posts as $post)
                                        <div class="masonry__item col-sm-6" data-masonry-filter="{{$post->category->slug}}">
                                            <article class="feature feature-1">
                                                <a href="#" class="block">
                                                    @if($post->image!= null)<img alt="Image" src="{{$post->image->path}}" /> @endif
                                                </a>
                                                <div class="feature__body boxed boxed--border">
                                                    <span><?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></span>
                                                    <h5>{{$post->title}}</h5>
                                                    <a href="{{ route('view', ['slug' => $post->slug]) }}">
                                                        Read More
                                                    </a>
                                                </div>
                                            </article>
                                        </div>
                                        @endforeach
                                        <!--end item-->

                                    </div>
                                    <!--end of masonry container-->
                                </div>
                                  <div class="pagination">
                                    <div class="col-xs-6">
                                        <!-- <a class="type--fine-print" href="#">&laquo; Older Posts</a> -->
                                    </div>
                                    <div class="col-xs-6 text-right">
                                        <!-- <a class="type--fine-print" href="#">Newer Posts &raquo;</a> -->
                                    </div>
                                </div>
                            </div>
                            <!--end masonry-->
                        </div>
                        <div class="col-md-4 hidden-sm">
                            <div class="sidebar boxed boxed--border boxed--lg bg--secondary">
                                <div class="sidebar__widget">
                                    <h5>Search site</h5>
                                    @include('blog.partials.search-form')

                                </div>

                                <div class="sidebar__widget">
                                    @include('blog.partials.blog-archived')
                                </div>
                                <!--end widget-->
                            </div>
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>

        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
    </div>
@endsection
