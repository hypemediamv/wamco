@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-9">
                <div class="post-body">
                    @if ($post->image) <img src="{{ starts_with($post->image->path, ['http://', 'https://']) ? '' : '' }}{{ $post->image->path }}" alt="" style="max-width: 100%;" class="post-image">@endif
                    <article>
                        <div class="post-content">
                            <div class="">
                                <h1 class="">{{ $post->title }}</h1>
                                <div class="row">
                                    <div class="col-sm-12 col-md-12 col-lg-12">
                                        <div class="">
                                            {!! $post->content !!}
                                        </div>
                                    </div>
                                </div>
                                <hr />
                                <div>
                                    <div>
                                        <i class="fa fa-square"></i>

                                        <a href="{{ route('category', ['slug' => $post->category->slug]) }}">{{ $post->category->title }}</a>
                                    </div>
                                    <div>
                                        <i class="fa fa-clock-o"></i>
                                        Published date: {{ hdate($post->published_at) }}
                                        <small class="text-muted">({{ date('d.m.Y', strtotime($post->published_at)) }})</small>
                                    </div>
                                    <div>
                                        <i class="fa fa-user"></i>
                                        Author: {{ $post->user->name }}
                                    </div>
                                    <div>
                                        @foreach($post->tags as $tag)
                                            <p class="label label-default">{{ $tag->tag }}&nbsp;</p>
                                        @endforeach
                                    </div>
                                </div>

                            </div>
                            <div class="">
                                @include('blogs.partials.related-posts', ['post' => $post])
                            </div>
                        </div>
                    </article>
                    @include('blogs.partials.comments')
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-3">
                {{--@include('blogs.partials.categories-menu')--}}
            </div>
        </div>
    </div>
@stop

{{--@section('meta')--}}
    {{--<link rel="author" href="{{ $post->user->name or '' }}" />--}}
{{--@stop--}}
