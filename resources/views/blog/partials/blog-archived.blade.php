<h5>Archives</h5>
<ul class="accordion accordion-1">
  @foreach($post_links as $date => $link)
                  <li>
                      <div class="accordion__title">
                          <span class="h5">{{ $date }}</span>
                      </div>
                      @foreach($link as $post)
                      <div class="accordion__content">

                          <a href="{{ route('view', ['slug' => $post->slug]) }}" class="small"> &#x1F517; {{$post->title}}
                          </a>
                      </div>
                      @endforeach
                  </li>
  @endforeach
</ul>
