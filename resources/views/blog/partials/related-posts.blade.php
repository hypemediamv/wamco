@inject('related', 'App\Services\Blog')
<?php
$posts = $related->getRelatedPosts($post->tags, $post->id);
?>
@if($posts->count() > 0)
<div class="container">
    <div class="row text-block">
        <div class="col-sm-12">
            <h3>More recent stories</h3>
        </div>
    </div>
    <!--end of row-->
    <div class="row">
      @foreach ($posts as $post)
        <div class="col-sm-4">
            <article class="feature feature-1">
                <a href="#" class="block">
                  @if($post->image!= null)<img alt="Image" src="{{$post->image->path}}" /> @endif
                </a>
                <div class="feature__body boxed boxed--border">
                    <span><?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></span>
                    <h5>{{ $post->title }}</h5>
                    <a href="{{ route('view', ['slug' => $post->slug]) }}">
                        Read More
                    </a>
                </div>
            </article>
        </div>
      @endforeach
    </div>
    <!--end of row-->
</div>
<!--end of container-->
@endif
