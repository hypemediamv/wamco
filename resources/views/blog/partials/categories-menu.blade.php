<div class="panel panel-info">
    <div class="panel-heading">
        <h3>Categories  {{ $title }}</h3>
    </div>
    <div class="list-group">
        <a href="{{ route('blog-index') }}"
           class="list-group-item {{ (!isset($active_category) ? 'active' : '') }}">
            <span class="badge">{{ $posts_count }}</span>
            All Posts
        </a>
        @foreach($categories as $category)
            <a href="{{ route('category', ['slug' => $category->slug]) }}"
               class="list-group-item {{ (isset($active_category) && $active_category == $category->id) ? 'active' : '' }}">
                {{ $category->title }}
                <span class="badge badge-info">{{ $category->num }}</span>
            </a>
        @endforeach
    </div>
</div>