@extends('layouts.front2')

@section('content')
        <a id="start"></a>
        <div class="main-container">
          <section class="text-center imagebg" data-overlay="4">
              <div class="background-image-holder"> <img alt="background" src="{{url('/')}}/img/hero-3.jpg"> </div>
              <div class="container">
                  <div class="row">
                      <div class="col-sm-10 col-md-8">
                          <h1>News and Events</h1>
                          <p class="lead"> Latest news and announcements </p>
                      </div>
                  </div>
              </div>
          </section>

        <div class="main-container">

            <section>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-10 col-md-8 col-sm-offset-1 col-md-offset-2">
                            <article>
                                <div class="article__title text-center">
                                    <h1 class="h2">{!! $post->title !!}</h1>
                                    <span><?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></span>
                                    <span>in
                                        <a href="#">  {{ $post->category->slug}}</a>
                                    </span>
                                </div>
                                <!--end article title-->
                                <div class="article__body">
                                    @if($post->image!= null) <img alt="Image" src="{{ $post->image->path }}" /> @endif
                                    {!! $post->content !!}
                                </div>
                                <div class="article__share text-center">
                                    <a class="btn bg--facebook btn--icon" href="https://www.facebook.com/sharer/sharer.php?u={{ url('/') }}/view/{{$post->slug }}" target="_blank">
                                        <span class="btn__text">
                                            <i class="socicon socicon-facebook"></i>
                                            Share on Facebook
                                        </span>
                                    </a>
                                    <a class="btn bg--twitter btn--icon" href="https://twitter.com/home?status={{ url('/') }}/view/{{ $post->slug }}" target="_blank">
                                        <span class="btn__text">
                                            <i class="socicon socicon-twitter"></i>
                                            Share on Twitter
                                        </span>
                                    </a>
                                </div>
                            </article>
                            <!--end item-->
                        </div>
                    </div>
                    <!--end of row-->
                </div>
                <!--end of container-->
            </section>
            <section class="bg--secondary">
                @include('blog.partials.related-posts')
            </section>
        </div>
        <!--<div class="loader"></div>-->
        <a class="back-to-top inner-link" href="#start" data-scroll-class="100vh:active">
            <i class="stack-interface stack-up-open-big"></i>
        </a>
@endsection
