<form action="{{ route('blog-index') }}" method="get">
    <div class="input-group" style="margin-top: 30px">
        <input type="text" name="search_blog" class="form-control" placeholder="Search by title or slug" value="{{ $search_blog }}">
      <span class="input-group-btn">
        <button class="btn btn-sm btn-primary" type="submit"><i class="fa fa-search"></i></button>
      </span>
    </div>
</form>
<br />