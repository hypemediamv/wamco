<div class="panel panel-info">
    <div class="panel-heading">
        <h3>Archived</h3>
    </div>
    <div class="panel-group">
        {{--<div class="panel panel-default">--}}
            <?php $index = 0; ?>
            @foreach($post_links as $date => $link)
            <div class="panel-heading">
                <h4 class="panel-title">
                    <a data-toggle="collapse" href="#{{$index}}">{{ $date }}</a>
                    <span class="pull-right badge">{{$link->count()}}</span>
                </h4>
            </div>

                <div id="{{$index}}" class="panel-collapse collapse">
                    @foreach($link as $post)
                    <div class="panel-body">
                        <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank"> {{$post->title}}</a>
                    </div>
                    @endforeach
                    <?php $index++; ?>
                </div>


            @endforeach
        {{--</div>--}}
    </div>
</div>