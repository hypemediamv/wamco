@inject('related', 'App\Services\Blog')
<?php
$posts = $related->getRelatedPosts($post->tags, $post->id);
?>
@if($posts->count() > 0)
    <hr />
    <h4>Related Posts</h4>
    @foreach ($posts as $post)

    <div class="card">
        <div class="card-image">
            <figure class="image is-16by9">
                <a href="{{ route('view', ['slug' => $post->slug]) }}">
                  @if($post->image) <img src="/wamco/{{ $post->image->path }}" alt="" style="max-width: 100%;"> @else <img src="http://bulma.io/images/placeholders/96x96.png" alt="" style="max-width: 100%;"> @endif
                </a>
            </figure>
        </div>
        <div class="card-content">
            <div class="media">
                <div class="media-left">
                    <figure class="image" style="height: 40px; width: 40px;">
                        <img src="http://bulma.io/images/placeholders/96x96.png" alt="Image">
                    </figure>
                </div>
                <div class="media-content">
                    <p class="title is-5">{{ $post->user->name }}</p>
                    <p class="subtitle is-6">{{ '@'.$post->user->name }}</p>
                </div>
            </div>

            <div class="content">
                <a href="{{ route('view', ['slug' => $post->slug]) }}">{{ $post->title }}</a>
                <br>
                <small><?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></small>
            </div>
        </div>
    </div>
        <p></p>
    @endforeach

@endif
