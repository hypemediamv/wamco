<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $post->title }}</title>

    <!-- Bootstrap Core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.3.1/css/bulma.min.css">

    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/blog-front.css') }}">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->

</head>

<body>

<!-- Navigation -->
@include('blogs.partials.nav')

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->

@if($post->image_id)
<header class="intro-header" style="background-image: url({{$post->image->path}})">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>{{ $post->title }}</h1>
                    <p class="post-meta">Posted by {{ $post->user->name }} on <?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></p>
                    <p><span class="small">{{ hdate($post->published_at) }}</span></p>
                    <a href="/blog" style="color:#fff;"><i class="fa fa-reply"> Home</i></a>
                </div>
            </div>
        </div>
    </div>
</header>
@else
<header class="intro-header" style="background-image: url('https://source.unsplash.com/random/1600x900')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="post-heading">
                    <h1>{{ $post->title }}</h1>
                    <p class="post-meta">Posted by {{ $post->user->name }} on <?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?></p>
                    <p><span class="small">{{ hdate($post->published_at) }}</span></p>
                    <a href="/blog" style="color:#fff;"><i class="fa fa-reply"> Home</i></a>
                </div>
            </div>
        </div>
    </div>
</header>
@endif

<!-- Post Content -->
<article>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                {!!  $post->content !!}
            </div>
        </div>
    </div>
</article>

<hr>
<ul class="list-inline text-center">
    <li>
        <a href="https://twitter.com/home?status={{ url('/') }}/view/{{ $post->slug }}" target="_blank" >Share it on twitter
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
        </a>
    </li>
    <li>
        <a href="https://www.facebook.com/sharer/sharer.php?u={{ url('/') }}/view/{{$post->slug }}" target="_blank">Share it on facebook
                                <span class="fa-stack fa-lg">
                                    <i class="fa fa-circle fa-stack-2x"></i>
                                    <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
        </a>
    </li>

</ul>
<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                @include('blogs.partials.related-posts')
                <p class="copyright text-muted">Copyright &copy; WAMCO 2016 - {{date('Y')}}</p>
            </div>
        </div>
    </div>
</footer>


<!-- jQuery -->
<script src="{{ asset('plugins/jquery/js/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Theme JavaScript -->
<script src="{{  asset('js/blog-front.js') }}"></script>

</body>

</html>
