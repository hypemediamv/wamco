<div class="post-regular post-body">
    <a href="{{ route('view', ['slug' => $post->slug]) }}">
        @if ($post->image) <img class="post-image" src="{{ starts_with($post->image->path, ['http://', 'https://']) ? '' : '' }}{{ $post->image->path }}" alt="" >@endif
    </a>
    <div class="row">
        <div class="col-lg-12" >
            <div class="post-content">
                <h2 class="post-title">
                    <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank">{!! highlight_str($post->title, $search_blog) !!}</a>
                </h2>

                <hr />
                <div class="row">
                    <div class="col-lg-12">

                        <div>
                            <i class="fa fa-square"></i>
                            <a href="{{ route('category', ['slug' => $post->category->slug]) }}">{{ $post->category->title }}</a>
                        </div>
                        <div>
                            <i class="fa fa-clock-o"></i>
                            <span class="text-muted">Published on:</span>
                            {{ hdate($post->published_at) }}
                            <small class="text-muted">({{ date('d.m.Y', strtotime($post->published_at)) }})</small>
                        </div>
                        <div>
                            @foreach($post->tags as $tag)
                                <p class="label label-default tag-item">{{ $tag->tag }}&nbsp;</p>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
