<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="this blog front page created using bootstrap">
    <meta name="author" content="shimax">

    <title>WBlog</title>

    <!-- Bootstrap Core CSS -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap/css/bootstrap.min.css') }}">
    <!-- Theme CSS -->
    <link rel="stylesheet" type="text/css" href="{{ asset('css/blog-front.css') }}">

    <!-- Custom Fonts -->
    <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
    <link href='https://fonts.googleapis.com/css?family=Lora:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <!--<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>-->
    <!--<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>-->
    <![endif]-->
    <style>
        .btn {
            padding: 5.5px;
        }
    </style>
</head>

<body>

<!-- Navigation -->
@include('blogs.partials.nav')

<!-- Page Header -->
<!-- Set your background image for this header on the line below. -->
<header class="intro-header" style="background-image: url('https://source.unsplash.com/category/nature/1600x600')">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <div class="site-heading">
                    <h1>News & Events</h1>
                    <hr class="small">
                    <span class="subheading"> Latest Upcoming News & Updates</span>
                </div>
            </div>
        </div>
    </div>
</header>

<!-- Main Content -->
<div class="container">
    <div class="row">
        <div class="col-lg-8 col-md-10">
            @foreach($posts as $post)
            <div class="post-preview">
                <a href="{{ route('view', ['slug' => $post->slug]) }}" target="_blank">
                    <h2 class="post-title">
                        {!! highlight_str($post->title, $search_blog) !!}
                    </h2>
                    <h3 class="post-subtitle">
                        <?php $content = strip_tags($post->content);
                        $displayString = substr($content, 0, 100);
                        ?>
                        {{ $displayString }}...
                    </h3>
                </a>

                <p class="post-meta">Posted by {{ $post->user->name }} on <?php $date = date_create($post->published_at); echo (date_format($date, 'l jS F Y g:ia')); ?><span class="pull-right">{{ hdate($post->published_at) }}</span></p>
            </div>
            <hr>
            @endforeach
            <!-- Pager -->
                <div class="text-center">
                    @if($posts->lastPage() > 1)
                        {!! $posts->render() !!}
                    @endif
                </div>
        </div>
        <div class="col-lg-4 col-md-2">
            <div style="padding-left: 15px;">
                @include('blogs.partials.search-form')
                @include('blogs.partials.categories-menu')
                @include('blogs.partials.blog-archived')

            </div>

        </div>
    </div>
</div>

<hr>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-lg-offset-2 col-md-10 col-md-offset-1">
                <p class="copyright text-muted">Copyright &copy; WAMCO 2016 - {{date('Y')}}</p>
            </div>
        </div>
    </div>
</footer>

<!-- jQuery -->
<script src="{{ asset('plugins/jquery/js/jquery.min.js') }}"></script>

<!-- Bootstrap Core JavaScript -->
<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('plugins/bootstrap/js/bootstrap.min.js') }}"></script>

<!-- Theme JavaScript -->
<script src="{{  asset('js/blog-front.js') }}"></script>

</body>

</html>