@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-8">
                <div class="row" data-equalizer>
                    @foreach($posts as $post)
                        @include('blogs.partials._post', ['post' => $post])
                        <hr>
                    @endforeach
                </div>
                <div class="text-center">
                    @if($posts->lastPage() > 1)
                        {!! $posts->render() !!}
                    @endif
                </div>
            </div>
            <div class="col-sm-12 col-md-12 col-lg-4">
                <div style="padding-left: 15px;">
                    @include('blogs.partials.search-form')
                    @include('blogs.partials.categories-menu')
                    @include('blogs.partials.blog-archived')
                </div>
            </div>
        </div>
    </div>
@stop