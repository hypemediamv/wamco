@if(count($services) > 0)
    <ul class="social-list list-inline list--hover">

        @foreach($services as $service)

            @if($service['service'] == 'facebook')
                <li><a href="{{$service['url']}}" target="_blank">
                        @if($service['show_title'])
                            {{ trans("socials.services.{$service['service']}.title") }}
                        @endif
                        <i class="socicon socicon-facebook icon icon--xs"></i>
                    </a>
                </li>
            @elseif($service['service'] == 'twitter')
                <li><a href="{{$service['url']}}" target="_blank">
                        @if($service['show_title'])
                            {{ trans("socials.services.{$service['service']}.title") }}
                        @endif
                        <i class="socicon socicon-twitter icon icon--xs"></i>
                    </a>
                </li>
            @elseif($service['service'] == 'gplus')
                <li><a href="{{$service['url']}}" target="_blank">
                        @if($service['show_title'])
                            {{ trans("socials.services.{$service['service']}.title") }}
                        @endif
                        <i class="socicon socicon-google icon icon--xs"></i>
                    </a>
                </li>
            @elseif($service['service'] == 'youtube')
                <li><a href="{{$service['url']}}" target="_blank">
                        @if($service['show_title'])
                            {{ trans("socials.services.{$service['service']}.title") }}
                        @endif
                        <i class="socicon socicon-youtube icon icon--xs"></i>
                    </a>
                </li>
            @endif
        @endforeach
    </ul>

@endif