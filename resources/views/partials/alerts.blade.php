@if(Session::get('message') != null)

@if(Session::get('type') == 'success')
<div class="alert alert-success alert-dismissable" id="message-box">
@endif
@if(Session::get('type') == 'danger')
<div class="alert alert-danger alert-dismissable" id="message-box">
@endif
@if(Session::get('type') == 'warning')
<div class="alert alert-warning alert-dismissable">
@endif

      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>

    {!! Session::get('message') !!}

    @if(Session::get('list') != null)
    <ul>
      @foreach(Session::get('list') as $item)

        <li>{{ $item->name }}</li>

      @endforeach
    </ul>

    @endif

</div>

@endif


  @if($errors->any())

  <div class="alert alert-danger alert-dismissable" >
    <strong>
      <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
      Please fix the following issues
  </strong><br>

      @foreach($errors->all() as $error)
        {{$error}} <br>
      @endforeach

  </div>
  @endif
