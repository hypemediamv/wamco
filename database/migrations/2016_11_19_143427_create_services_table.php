<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateServicesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('services', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('name');
            $table->string('description');
            $table->mediumText('detail');
            $table->integer('image_id')->unsigned();
            $table->integer('page_id')->unsigned();
            $table->foreign('page_id')->references('id')->on('pages');
            //add later
            // $table->foreign('image_id')->references('id')->on('images')->onDelete('cascade');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // Schema::dropForeign('images_image_id_foreign');
      // Schema::dropForeign('users_created_by_foreign');
      Schema::dropIfExists('services');
    }
}
