<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('slides', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('slide_name');
            $table->string('slide_description');
            $table->integer('slide_order');
            $table->integer('slideshow_id')->unsigned();
            $table->foreign('slideshow_id')->references('id')->on('slideshows');
            $table->integer('created_by')->unsigned();
            $table->foreign('created_by')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      // $table->dropForeign('users_created_by_foreign');
      // $table->dropForeign('slideshows_slideshow_id_foreign');
      Schema::dropIfExists('slides');
    }
}
