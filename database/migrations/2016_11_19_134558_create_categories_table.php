<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title', 255);
            $table->string('slug', 512);
            $table->integer('category_type_id');
            $table->string('description');
            $table->integer('parent')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
        $category = new \App\Category();
        $category->id = 1;
        $category->title = 'Uncategorized';
        $category->slug = 'uncategorized';
        $category->description = 'uncategorized posts';
        $category->category_type_id = 1;
        $category->save();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        // Schema::dropForeign('users_created_by_foreign');
        Schema::dropIfExists('categories');
    }
}
