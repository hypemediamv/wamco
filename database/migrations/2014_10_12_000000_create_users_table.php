<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });

        $user = new \App\User();
        $user->name = 'wamco';
        $user->email = 'wamco@gmail.com';
        $user->password = bcrypt('654321');
        $user->save();

        $user = new \App\User();
        $user->name = 'Spyhacker';
        $user->email = 'spyhacker@gmail.com';
        $user->password = bcrypt('123456');
        $user->save();


        $password = bcrypt('mr.be3n');

        $data = array(
                array('name'=> 'intelligenced', 'email' => 'intelligenced@gmail.com', 'password' =>$password),
            );

        DB::table('users')->insert($data);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
