<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Cviebrock\EloquentSluggable\Sluggable;
class Product extends Model
{
  use Sluggable;

  public function sluggable()
  {
      return [
          'slug' => [
              'source' => 'name'
          ]
      ];
  }

  public function product_images(){
    return $this->hasMany(ProductImage::class, 'product_id');
  }

  public function shop_zones(){
    return $this->belongsTo(ShopZone::class, 'shop_zone_id');
  }

  public function category(){
    return $this->belongsTo(Category::class, 'category_id');
  }

  public function scopeSearch($query, $str)
  {
      $str = '%' . $str . '%';

      return $query->where('name', 'like', $str)
          ->orWhere('description', 'like', $str);
  }

    //
}
