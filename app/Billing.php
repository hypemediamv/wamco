<?php

namespace App;
use App\User;
use App\BillingService;

use Illuminate\Database\Eloquent\Model;

class Billing extends Model
{
    protected $table = 'billings';

    protected $fillable = ['billing_service_id', 'content', 'billing_date', 'billing_user_id'];

    public function BillingService() {
      return $this->belongsTo(BillingService::Class, 'billing_service_id');
    }


    public function billing_service(){
      return $this->belongsTo(BillingService::class, 'billing_service_id');
    }

    public function user(){
      return $this->belongsTo(User::class, 'billing_user_id');
    }

    public function users() {
      return $this->belongsTo(User::Class, 'billing_user_id');
    }
}
