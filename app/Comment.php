<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comment extends Model
{
  protected $table = 'comments';
  protected $fillable = ['*'];

  public function posts(){
    return $this->belongsTo(Post::class,'post_id');
  }
    //
}
