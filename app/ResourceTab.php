<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ResourceTab extends Model
{
    //
    public function resources(){
      return $this->hasMany(Resource::class, 'resource_tab_id');
    }


}
