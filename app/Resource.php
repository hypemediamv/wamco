<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Resource extends Model
{

  public function page(){
    return $this->belongsTo(Page::class, 'page_id');
  }

  public function resource_tab(){
    return $this->belongsTo(ResourceTab::class, 'resource_tab_id');
  }

  public function document(){
    return $this->belongsTo(Document::class, 'document_id');
  }

  public function icon(){
    return $this->belongsTo(Image::class, 'icon_id');
  }

  public function scopeForPage($query, $where)
  {
    return $query->where('page_id', $where);
  }


    //
}
