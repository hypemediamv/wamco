<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopZone extends Model
{
  public function products(){
    return $this->hasMany(Product::class, 'shop_zone_id');
  }

  public function product_images()
  {
    return $this->hasManyThrough('App\ProductImage', 'App\Product', 'shop_zone_id', 'product_id', 'id');
  }

}
