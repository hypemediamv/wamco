<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PageType extends Model
{

  public function page(){
    return $this->hasOne(Page::class, 'page_type_id');
  }

  
}
