<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WasteWizardDataBin extends Model
{

  public function image(){
    return $this->belongsTo(Image::class, 'image_id');
  }
    //

    public function waste_data(){
      return $this->hasMany(WasteWizardData::class,'waste_wizard_data_bin_id' );
    }
}
