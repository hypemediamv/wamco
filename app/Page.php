<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
  public function page_type(){
    return $this->belongsTo(PageType::class, 'page_type_id');
  }
  public function services(){
    return $this->hasMany(Service::class, 'page_id');
  }

  public function staff(){
    return $this->hasMany(Staff::class, 'page_id');
  }
  public function contacts(){
    return $this->hasMany(Contact::class, 'page_id');
  }

  public function resources(){
    return $this->hasMany(Resource::class, 'page_id');
  }
    //
}
