<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BillingService extends Model
{
    protected $table = 'billing_services';

    protected $fillable = ['service_name'];

    public function billing() {
      return $this->belongsTo('App\BillingService', 'billing_service_id');
    }
    
}
