<?php

namespace App;

use Cache;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Post extends Model
{
    use Sluggable;

    public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'title'
            ]
        ];
    }
    protected $table = 'posts';
    protected $fillable = ['*'];
    public static $_instance = null;

    //GET STATIC INSTANCE CLASS
    public static function i()
    {
        $class = get_called_class();
        if (!static::$_instance) {
            static::$_instance = new $class();
        }

        return static::$_instance;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }

    public function getPostsByCategoryId($category_id, $str = null)
    {
        $posts = $this->with(['category', 'user']);
        if (!empty($category_id)) {
            $posts->where('category_id', $category_id);
        }

        if (!empty($str)) {
            $this->scopeSearch($posts, $str);
        }

        return $posts->active()->sort()->paginate(6);
    }

    public function image(){
        return $this->belongsTo(Image::class,'image_id');
    }

    public function comments(){
        return $this->hasMany(Comment::class,'post_id');
    }



    //GET SLUG
    public function getBySlug($slug)
    {
        return $this->with(['user', 'category', 'tags'])->where('slug', $slug)->first();
    }

    //QUERY SCOPE TO GET ACTIVE POSTS
    public function scopeActive($query)
    {
        return $query->where('status', 'active');
    }

    //QUERY SCOPE TO SORT POSTS IN DESC
    public function scopeSort($query)
    {
        return $query->orderBy('published_at', 'desc');
    }

    //QUERY SCOPE TO GET POST STATUS
    public function scopeByStatus($query, $statuses)
    {
        if (is_array($statuses)) {
            return $query->whereIn('status', $statuses);
        } else {
            return $query->where('status', $statuses);
        }
    }

    //QUERY SCOPE TO SEARCH POST
    public function scopeSearch($query, $str)
    {
        $str = '%' . $str . '%';

        return $query->where('title', 'like', $str)
            ->orWhere('slug', 'like', $str)
            ->orWhere('content', 'like', $str);
    }
}
