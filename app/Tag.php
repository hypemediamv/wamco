<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;
class Tag extends Model
{
    public static $_instance = null;

    public static function i()
    {
        $class = get_called_class();
        if (!static::$_instance) {
            static::$_instance = new $class();
        }

        return static::$_instance;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_tags', 'tag_id', 'post_id');
    }
    

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id');
    }
}
