<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{

  public function cart_products(){
    return $this->hasMany(CartProduct::class, 'cart_id');
  }

    //
}
