<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionDateZone extends Model
{

  public function collection_date_sites(){
    return $this->hasMany(CollectionDateSite::class, 'collection_date_zone_id');
  }
    //
}
