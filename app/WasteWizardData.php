<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class WasteWizardData extends Model
{

  public function bin(){
    return $this->belongsTo(WasteWizardDataBin::class,'waste_wizard_data_bin_id' );
  }
    //
}
