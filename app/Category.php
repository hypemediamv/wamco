<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Category extends Model
{
  protected $table = 'categories';
  protected $fillable = ['*'];
  public static $_instance = null;

  public static function i()
  {
    $class = get_called_class();
    if (!static::$_instance) {
      static::$_instance = new $class();
    }

    return static::$_instance;
  }

  public function posts(){
    return $this->hasMany(Post::class, 'category_id');
  }

  //GET SLUG
  public function getBySlug($slug)
  {
    return static::where('slug', 'like', $slug)->first();
  }
    //

    public function products(){
      return $this->hasMany(Product::class, 'category_id');
    }

  public function withPostsCount()
  {
    $class = get_called_class();

    return $class::leftJoin('posts', 'posts.category_id', '=', 'categories.id')
        ->where('posts.status', 'active')
        ->groupBy('categories.id')
        ->orderBy('categories.title')
        ->get(['categories.*', DB::raw('COUNT(posts.id) as num')]);
  }

}
