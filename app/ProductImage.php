<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{

  public function images(){
    return $this->hasMany(Image::class, 'image_id');
  }

  public function product(){
    return $this->belongsTo(Product::class,'product_id');
  }

  public function image(){
    return $this->belongsTo(Image::class, 'image_id');
  }
    //
}
