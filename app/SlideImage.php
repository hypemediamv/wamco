<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SlideImage extends Model
{

  public function image(){
    return $this->belongsTo(Image::class,'image_id');
  }

  public function slide(){
    return $this->belongsTo(Slide::class, 'slide_id');
  }
    //
}
