<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionDate extends Model
{

  public function collection_date_site(){
    return $this->belongsTo(CollectionDateSite::class, 'collection_date_site_id');
  }
    //
}
