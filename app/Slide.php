<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slide extends Model
{
    //


    public function slideshow(){
      return $this->belongsTo(Slideshow::class, 'slideshow_id');
    }

    // public function slide_image(){
    //   return $this->belongsTo(SlideImage::class, 'slide_image_id');
    // }

    public function slide_image(){
      return $this->hasOne(SlideImage::class, 'slide_id');
    }
}
