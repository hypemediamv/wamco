<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slideshow extends Model
{
  public function slides(){
    return $this->hasMany(Slide::class, 'slideshow_id');
  }
    //
}
