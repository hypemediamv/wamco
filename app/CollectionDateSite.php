<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CollectionDateSite extends Model
{

  public function collection_date_zone(){
    return $this->belongsTo(CollectionDateZone::class, 'collection_date_zone_id');
  }

  public function collection_dates(){
    return $this->hasMany(CollectionDate::class, 'collection_date_site_id');

  }
    //
}
