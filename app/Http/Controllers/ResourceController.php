<?php

namespace App\Http\Controllers;

use App\Page;
use App\PageType;
use App\Resource;
use App\ResourceTab;
use App\Document;
use App\Image;
use Storage;
use Notifications;
use Illuminate\Http\Request;
use lluminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Input;
class ResourceController extends Controller
{

  public function __construct(){
     $this->page_type = PageType::where('name','resource')->first();
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $pages = Page::where('page_type_id',$this->page_type->id)->get();
      return view('pages.resource', ['pages' => $pages]);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $resource_tab = ResourceTab::find($request->resource_tab_id);
      $resource_page = Page::find($request->page_id);



      $resource = new Resource;
      $resource->name = $request->resource_name;
      $resource->description = $request->resource_description;

      if($request->resource_icon != null){

        // $image_path = $request->file('resource_icon')->store('public/images');
        // $image_changed_path = str_replace("public/","/",$image_path);

        $path = $request->file('resource_icon')->store('storage/images/resources');
        $changed_path = str_replace("storage/","/public/storage/",$path);

        $image = new Image;
        $image->filename = $request->file('resource_icon')->getClientOriginalName();
        $image->path =$changed_path;
        $image->save();

        $resource->icon()->associate($image);

      }

      if($request->file('document')->isValid()) {

        // $document_path = $request->file('document')->store('public/documents');
        // $document_changed_path = str_replace("public/","/",$document_path);
        $path = $request->file('document')->store('storage/documents');
        $changed_path = str_replace("storage/","/storage/",$path);

        $document = new Document;
        $document->filename = $request->file('document')->getClientOriginalName();
        $document->path =$changed_path;
        $document->save();
      } else {
        Notifications::add('Please select a file lower than 2MB', 'error');
        return redirect()->back()->withInput();
      }


      $resource->document()->associate($document);
      $resource->resource_tab()->associate($resource_tab);
      $resource->page()->associate($resource_page);
      $resource->save();

      Notifications::add('Resource successfully added', 'success');

      return redirect()->back();





        //
    }

    public function store_tab(Request $request){
      $resource_tab = new ResourceTab;
      $resource_tab->name = $request->resource_tab_name;
      $resource_tab->save();

      Notifications::add('Resource Tab successfully added', 'success');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $resource_page = Page::with('resources.icon')->find($id);
      $resource_tabs = ResourceTab::all();

      return view('pages.resources.index', ['resource_page' => $resource_page, 'resource_tabs' => $resource_tabs]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $resource = Resource::with('resource_tab')->where('id', '=', $id  )->first();
      $resource_tabs = ResourceTab::get();
      return view('pages.resources.edit', ['resource' => $resource, 'resource_tabs' => $resource_tabs]);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $resource = Resource::with('resource_tab')->where('id', '=', $id  )->first();
      $resource->resource_tab_id = $request->resource_tab_id;
      $resource->name = $request->resource_name;
      $resource->description = $request->resource_description;
      $resource->save();

      Notifications::add('Resource successfully updated', 'success');
      return redirect()->route('resources.show', ['id' => $resource->page_id]);
    }


    public function destroy_tab($id){
      $resource_count = Resource::where('resource_tab_id',$id)->get();
      if($resource_count == 0){
          $resource_tab = ResourceTab::find($id);
          $resource_tab->delete();
      }

      return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $resource = Resource::with('document','icon')->findOrFail($id);

      if($resource->icon) {
        Storage::disk('public')->delete($resource->icon->path);
        $resource->icon()->delete();
      }
      if($resource->document) {
        Storage::disk('public')->delete($resource->document->path);
        $resource->document()->delete();
      }

      Notifications::add('Resource successfully deleted', 'success');

      $resource->delete();

      return redirect()->back();

    }
}
