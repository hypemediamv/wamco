<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\ProductImage;
use App\Image;
use Storage;
use App\ShopZone;
use Notifications;

class ProductController extends Controller
{

    public function __construct(){
      $this->categories = Category::where('category_type_id',2)->get();
      $this->shop_zones = ShopZone::with('products.product_images.image')->paginate(15);

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $zones = ShopZone::with('products')->get();
      $products = Product::with('category','product_images.image')->paginate(5);;
      return view('admin.shop.products',['categories' => $this->categories, 'products' => $products, 'products_title' => 'Products', 'zones' => $zones]);
        //
    }

    public function all(){

      $products = Product::with('category','product_images.image')->paginate(15);

      // $shop_zones = ShopZone::with('products.product_images.image')->paginate(15);
      return view('admin.shop.products.index',['categories' => $this->categories, 'products' => $products,  'products_title' => 'All Products', 'shop_zones' => $this->shop_zones]);

    }

    public function category_index($category_id){
      $category_name = Category::find($category_id)->title;


      $products = Product::with('category','product_images.image')->where('category_id', $category_id)->paginate(15);
      return view('admin.shop.products.index',['categories' => $this->categories, 'products' => $products,  'products_title' => $category_name,'shop_zones' => $this->shop_zones ]);

    }

    public function zone_index($shop_zone_id){
      $shop_zone_name = ShopZone::find($shop_zone_id)->name;


      $products = Product::with('shop_zones','product_images.image')->where('shop_zone_id', $shop_zone_id)->paginate(15);
      return view('admin.shop.products.index',['categories' => $this->categories, 'products' => $products,  'products_title' => $shop_zone_name, 'shop_zones' => $this->shop_zones ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->all());
      $category = Category::where('id', $request->category_id)->first();

      $product = new Product;
      $product->name = $request->name;
      $product->description = $request->description;
      $product->shop_zone_id = $request->shop_zone_id;
      if ($request->has('update_slug')) {
          $product->resluggify();
      }
      $product->category()->associate($category);
      $product->created_by = $request->user()->id;
      $product->price = $request->price;
      $product->save();

        if ($request->hasFile('product_images')) {

            foreach ($request->product_images as $uploaded_product_image) {

                $path = $uploaded_product_image->store('public/images');
                $changed_path = str_replace("public","/public",$path);

                // $path = $uploaded_product_image->store('storage/images/shop');
                // $changed_path = str_replace("storage/", "/public/storage/", $path);

                $image = new Image;
                $image->filename = $uploaded_product_image->getClientOriginalName();
                $image->path = $changed_path;
                $image->save();

                $product_image = new ProductImage;
                $product_image->product()->associate($product);
                $product_image->image()->associate($image);
                $product_image->created_by = $request->user()->id;
                $product_image->save();

            }
        }
        Notifications::add('Product Successfully Added', 'success');



      return redirect()->back();


        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $product = Product::with('category','product_images.image')->where('id',$id)->first();
      return view('admin.shop.products.single',['categories' => $this->categories, 'product' => $product,  'products_title' => $product->name ]);

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $product = Product::with('product_images.image')->find($id);

      foreach($product->product_images as $product_image){
        $image_count = Image::where('path',$product_image->image->path)->count();
        if($image_count == 1){
          Storage::disk('public')->delete($product_image->image->path);
        }

        $product_image->image()->delete();

      }
      $product->product_images()->delete();
      $product->delete();
      Notifications::add('Product Successfully deleted', 'success');


      return redirect()->back();


        //
    }
}
