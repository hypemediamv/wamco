<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\PageType;
use App\Page;
use App\Post;
use App\Slideshow;
use App\SlideImage;
use App\ResourceTab;
use App\CollectionDateZone;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
//        $this->middleware('auth');
        $page_types = PageType::select('id','name')->get();

        foreach($page_types as $key => $page_type){

            $page_types->{ $page_type->name } = $page_type->id;
            unset($page_types[$key]);


        }

        $this->page_types= $page_types;

    }


    public function collection_date_index(){

      $collection_date_zones = CollectionDateZone::with('collection_date_sites.collection_dates')->get();



      return view('collection_date_index',['collection_zones' => $collection_date_zones]);
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function old_index()
    {

      $pages= Page::with('page_type')->get();

      $slideshow = Slideshow::with('slides')->where('frontpage',1)->first();

      foreach($slideshow->slides as $slide){
        $slide->slide_image = SlideImage::where('slide_id',$slide->id)->with('image')->first();
      }

      $pages= Page::with('page_type')->orderBy('page_type_id')->get();

      foreach($pages as $key => $page){

        switch($page->page_type->id){

          case $this->page_types->service:
          $page->load(['services' => function($query){
            return $query->with('image')->take(3);
          }]);
          break;

          case $this->page_types->about:
          $page->load('staff.image');
          break;

          case $this->page_types->resource:

          $resource_tabs = ResourceTab::with(['resources' => function($query) use ($page) {
            $id = $page->id;
            return $query->with('document')->where('page_id', $id);
          }])->get();

          $page->resource_tabs = $resource_tabs;


          break;

          case $this->page_types->contact:
          $page->load('contacts');
          break;

          default: unset($pages[$key]);
        }

      }

      // dd($pages);

        $blog_posts = Post::OrderBy('created_at','desc')->take(3)->get();


      // dd($pages);

      return view('index_custom', ['slideshow' => $slideshow, 'pages' => $pages, 'blog_posts' => $blog_posts]);
    }

    public function index()
    {


      $slideshow = Slideshow::with('slides')->where('frontpage',1)->first();

      foreach($slideshow->slides as $slide){
        $slide->slide_image = SlideImage::where('slide_id',$slide->id)->with('image')->first();
      }


      $service_page = Page::with('services')->whereHas('page_type' ,function($q){
        return $q->where('name','service');
      })->first();



      $about_page = Page::with('staff.image')->whereHas('page_type', function($r){
        return $r->where('name','about');
      })->first();


      $resource_page = Page::whereHas('page_type' , function($q){
        return $q->where('name','resource');
      })->first();

      $resource_tabs = ResourceTab::with(['resources' => function($query) use ($resource_page) {
        $id = $resource_page->id;
        return $query->with('document')->where('page_id', $id);
      }])->get();

      $resource_page->resource_tabs = $resource_tabs;

      $contact_page = Page::with('contacts')->whereHas('page_type' , function($q){
        return $q->where('name','contact');
      })->first();


        $blog_posts = Post::OrderBy('created_at','desc')->take(3)->get();


      return view('index', [
        'slideshow' => $slideshow,
        'service_page' => $service_page,
        'resource_page' => $resource_page,
        'about_page' => $about_page,
        'contact_page' => $contact_page,
        'blog_posts' => $blog_posts
      ]);
    }

    public function dashboard()
    {
        return view('landing');

    }
}
