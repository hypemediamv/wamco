<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Slideshow;
use App\SlideshowImage;
use App\Image;
use App\SlideImage;
use File;
use Storage;
use Notifications;

class SlideController extends Controller
{
    //
    public function index($id){


      $slideshow = Slideshow::find($id)->load('slides.slide_image.image');

      // dd($slideshow);


      return view('slideshows.slides.index',['slideshow' => $slideshow]);



    }


    public function store(Request $request){
        $this->validate($request, [
        'slide_image' => 'required'
      ],[
        'slide_image.required' => 'Slide image is required'
      ]);
      $image_info = getimagesize( $request->file('slide_image'));

      $width = $image_info[0];
      $height = $image_info[1];

      // if($height > 1080 || $width < 1920){
      //   return redirect()->back();
      //
      // }
      // dd($request->all(),$image_info);




      //Local
      // $path = $request->file('slide_image')->store('public/images/slideshow');
      // $changed_path = str_replace("public/","/",$path);

//        dd(asset('storage/5e0123d3c8f0eb3a6c4e7f84ecb6bf96.jpeg'));
//        $storage_path = Storage::put('images/slideTest', $request->file('slide_image'));
//        dd($storage_path);
        $path = $request->file('slide_image')->store('storage/images/slides');
        $changed_path = str_replace("storage/","/public/storage/",$path);


      $image = new Image;
      $image->filename = $request->file('slide_image')->getClientOriginalName();
      $image->path =$changed_path;
      $image->save();


      $slide =new Slide;
      $slide->slide_name = $request->slide_name;
      $slide->slide_description =$request->slide_description;
      $slide->slideshow_id = $request->slideshow_id;
      $slide->slide_order = 0;
      $slide->created_by = $request->user()->id;
      $slide->save();

      $slide_image = new SlideImage;
      $slide_image->image()->associate($image);
      $slide_image->slide()->associate($slide);
      $slide_image->save();

      Notifications::add('Slide Image Successfully Added', 'success');

      return redirect()->back();


    }

    public function destroy($id){

      $slide_image = SlideImage::with('slide','image')->find($id);
       $image_count = Image::where('path',$slide_image->image->path)->count();

       if($image_count == 1){
         Storage::disk('public')->delete($slide_image->image->path);
       }
      $slide_image->image()->delete();
      $slide_image->slide()->delete();
      $slide_image->delete();

      Notifications::add('Slide Image Successfully deleted', 'success');

      return redirect()->back();



    }
}
