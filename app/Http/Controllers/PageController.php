<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\PageType;
use App\Contact;
use Notifications;
class PageController extends Controller
{

   public function __construct(){
    $this->page_types = PageType::all();
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pages = Page::with('page_type')->get();
      return view('pages.index',['pages' => $pages, 'page_types' => $this->page_types]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {


      $page_type = PageType::where('name', strtolower($request->page_type))->first();

      if($request->page_id == null){
        $page = new Page;
        $page->name = $request->page_name;
        $page->description = $request->page_description;
        $page->title = $request->page_title;
        $page->active = 1;
        $page->created_by = $request->user()->id;
        $page->background_color = $request->background_color;
        $page->page_type()->associate($page_type);
        $page->save();
        Notifications::add($page->name .' Page Successfully Added', 'success');

      }else{
        $page = Page::find($request->page_id);
        $page->name = $request->page_name;
        $page->description = $request->page_description;
        $page->title = $request->page_title;
        $page->background_color = $request->background_color;
        $page->update();
        Notifications::add($page->name .' Page Successfully Updated', 'success');
      }


        return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $page = Page::where('id',$id)->first()->load('page_type');

      switch($page->page_type->name){
        case 'service'  : return redirect('/admin/services');
                          break;
        case 'contact'  : return redirect('/admin/contacts');
                          break;
        case 'about'    : return redirect('/admin/about');
                          break;
        case 'resources': return redirect('/admin/resources');
                          break;
        default         : return redirect()->back();
      }

        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $edit_page = Page::find($id);
      return redirect()->back()->with(['edit_page' => $edit_page]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $page = Page::find($id);
      $page->name = $request->name;
      $page->description = $request->description;
      $page->title = $request->title;
      $page->page_type_id = $request->page_type_id;
      $page->active = 1;
      $page->created_by = $request->user()->id;
      $page->update();

      Notifications::add($page->name .' Page Successfully updated', 'success');

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

       Page::find($id)->delete();
       Notifications::add($page->name .' Page Successfully deleted', 'success');
       return back();
        //
    }
}
