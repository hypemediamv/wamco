<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CollectionDateZone;
use App\CollectionDateSite;

class CollectionDateSiteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $collection_date_sites = CollectionDateSite::with('collection_date_zone')->get();

      $collection_date_zones = CollectionDateZone::get();


      return view('admin.collection_date.site',[
        'collection_date_sites' => $collection_date_sites,
        'collection_date_zones' =>  $collection_date_zones

      ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'collection_date_zone_id' => 'required'
      ],[
        'name.required' => 'Collection date site name is required',
        'collection_date_zone_id.required' => 'Collection zone is required'
      ]);

      $collection_date_site = new  CollectionDateSite;
      $collection_date_site->name = $request->name;
      $collection_date_site->collection_date_zone_id =       $request->collection_date_zone_id;
      $collection_date_site->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $collection_date_site = CollectionDateSite::find($id);
      $collection_date_site->delete();
      return redirect()->back();

        //
    }
}
