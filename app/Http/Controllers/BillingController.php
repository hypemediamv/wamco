<?php

namespace App\Http\Controllers;

use Notifications;
use Illuminate\Http\Request;
use App\BillingService;
use App\Billing;
use App\User;

class BillingController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

      $data = [
        'billings' => Billing::with('billingService', 'users')->paginate(5),
        'billing_services' => BillingService::get(),
        'users' => User::get()
      ];

      // dd($data);

      return view('admin.billings.billing', $data);
      //
  }

  public function store(Request $request) {
      $this->validate($request,[
        'billing_service_id' => 'required',
        'content' => 'required',
        'billing_user_id' => 'required'
      ],[
        'billing_service_id.required' => 'Please select the billing service name',
        'content.required' => 'User billing content cannot be blank',
        'billing_user_id.required' => 'Please assign the bill to the user'
      ]);

      Billing::create($request->all());
      Notifications::add('User bill is created', 'success');

      return redirect()->back();
  }

    public  function edit($id) {
        $billing_services = BillingService::get();
        $users = User::get();
        $billing = Billing::with('BillingService')->findOrFail($id);
//        dd(!empty($billing->BillingService));
        return view('admin.billings.billing-edit')->with(['billing' => $billing, 'billing_services' => $billing_services, 'users' => $users ]);
    }

    public function update(Request $request, $id) {
        $this->validate($request,[
            'billing_service_id' => 'required',
            'content' => 'required',
            'billing_user_id' => 'required'
        ],[
            'billing_service_id.required' => 'Please select the billing service name',
            'content.required' => 'User billing content cannot be blank',
            'billing_user_id.required' => 'Please assign the bill to the user'
        ]);

        $billing = Billing::findOrFail($id);
//        dd($request->all(), $billing);
        $billing->billing_service_id = $request->get('billing_service_id');
        $billing->content = $request->get('content');
        $billing->billing_date = $request->get('billing_date');
        $billing->billing_user_id = $request->get('billing_user_id');
        $billing->save();
        Notifications::add('User bill is updated', 'success');

        return redirect()->action('BillingController@index');


    }


    public function destroy($id) {
      $data = Billing::with('billingService')->findOrFail($id);
        $data->delete();
        Notifications::add('User bill is deleted', 'success');

        return redirect()->back();

  }
}
