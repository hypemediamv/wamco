<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\CollectionDate;
use App\CollectionDateSite;

class CollectionDateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $collection_date_sites = CollectionDateSite::get();

      $collection_dates = CollectionDate::with('collection_date_site.collection_date_zone')->get();


      return view('admin.collection_date.index',[
        'collection_dates' => $collection_dates,
        'collection_date_sites' => $collection_date_sites

      ]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required',
        'content' => 'required',
        'collection_date_site_id' => 'required'
      ],[
        'name.required' => 'Collection date name is required',
        'content.required' => 'Collection date Content is required',
        'collection_date_site_id.required' => 'Collection date site is required'
      ]);
      $collection_date = new CollectionDate;
      $collection_date->name = $request->name;
      $collection_date->details =$request->details;
      $collection_date->content = $request->content;
      $collection_date->collection_date_site_id = $request->collection_date_site_id;
      $collection_date->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $collection_date = CollectionDate::find($id);
      $collection_date->delete();
      return redirect()->back();

        //
    }
}
