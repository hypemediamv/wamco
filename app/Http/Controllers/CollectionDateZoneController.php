<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CollectionDateZone;

class CollectionDateZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $collection_date_zones =  CollectionDateZone::get();
      return view('admin.collection_date.zone',['collection_date_zones' => $collection_date_zones]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Collection zone name is required'
      ]);
      $collection_date_zone = new CollectionDateZone;
      $collection_date_zone->name = $request->name;
      $collection_date_zone->save();

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

      $collection_date_zone = CollectionDateZone::findOrFail($id);
      $collection_date_zone->delete();

      return redirect()->back();
        //
    }
}
