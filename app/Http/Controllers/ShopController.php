<?php

namespace App\Http\Controllers;

use App\ShopZone;
use Illuminate\Http\Request;
use App\Product;
use App\Category;
use Auth;
use App\User;

class ShopController extends Controller
{

    public function __construct(){
      $this->products = Product::with('product_images.image')->paginate(15);
      $this->categories= Category::with('products.product_images.image')->where('category_type_id',2)->get();
      $this->shop_zones= ShopZone::with('products.product_images.image')->get();

    }

    private function getSignedUser(){

      if(Auth::user() != null){
        return User::with('cart.cart_products.product')->where('id', Auth::user()->id )->first();
  
      }

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('shop.index',['products' => $this->products, 'categories'=> $this->categories, 'signed_user' => $this->getSignedUser(),
      'shop_zones' => $this->shop_zones]);
        //
    }

    public function admin_index()
    {
      return view('admin.shop.index',['products' => $this->products, 'categories'=> $this->categories]);
        //
    }

    public function category_index($category_id){
      $category_title = Category::find($category_id)->title;

      $products = Product::with('category','product_images.image')->where('category_id', $category_id)->paginate(15);
      return view('shop.index',['products' => $products, 'categories'=> $this->categories, 'title' => $category_title , 'signed_user' => $this->getSignedUser()]);
    }

    public function product_index($product_id){
      $product = Product::with('category','product_images.image')->where('id', $product_id)->first();
      return view('shop.products.single',['product' => $product, 'categories'=> $this->categories, 'signed_user' => $this->getSignedUser()]);

    }

    public function product_search(Request $request){

      if($request->q == null){
        return redirect()->route('shop-index');
      }

      $query = $request->q;

      $products = Product::with('category','product_images.image')->Search($query)->paginate(15);
      $product_count = $products->count();

      if($product_count > 0){
        if($product_count == 1){
          $title = $product_count." result found for ".'"'.$query.'"';
        }else{
          $title = $product_count." results found for ".'"'.$query.'"';
        }
      }else{
        $title ="No  results found for ".'"'.$query.'"';

      }
      return view('shop.index',['products' => $products, 'categories'=> $this->categories, 'title' => $title , 'signed_user' => $this->getSignedUser()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $slug
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        return view('shop.products.front-single')->with(['product' => $this->products->where('slug',$slug)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
