<?php

namespace App\Http\Controllers;

use App\Service;
use App\PageType;
use App\Page;
use App\Image;
use File;
use Storage;
use Notifications;
use Illuminate\Http\Request;

class ServiceController extends Controller
{


   public function __construct(){
      $this->page_type = PageType::where('name','service')->first();
   }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Responsep
     */
    public function index()
    {

      $pages = Page::where('page_type_id',$this->page_type->id)->get();
      return view('pages.service', ['pages' => $pages]);
        //_
    }

    public function single($id){
      $pages = Page::with('page_type')->get();
      $service = Service::with('image')->find($id);
      return view('pages.services.single',['pages' => $pages, 'service' => $service]);


    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request){

      $page = Page::find($request->page_id);


      // $path = $request->file('service_image')->store('public/images');
      // $changed_path = str_replace("public/","/",$path);

        $path = $request->file('service_image')->store('storage/images/services');
        $changed_path = str_replace("storage/","/public/storage/",$path);


      $image = new Image;
      $image->filename = $request->file('service_image')->getClientOriginalName();
      $image->path =$changed_path;


      $image->save();



      $service = new Service;
      $service->name = $request->service_name;
      $service->description = $request->service_description;
      $service->detail = $request->service_detail;
      $service->image()->associate($image);
      $service->page()->associate($page);
      $service->created_by = $request->user()->id;
      $service->save();

      Notifications::add('Service Successfully Added', 'success');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

      $service_page = Page::with('services')->find($id);
      return view('pages.services.index',[ 'service_page' => $service_page]);




        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $service = Service::findOrfail($id);
        return view('pages.services.edit', ['service' => $service]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $service = Service::findOrfail($id);
        $service->name = $request->get('service_name');
        $service->description = $request->get('service_description');
        $service->detail = $request->get('service_detail');

        if ($request->hasFile('service_image')) {

            $path = $request->file('service_image')->store('storage/images/services');
            $changed_path = str_replace("storage/","/public/storage/",$path);

            $image = new Image;
            $image->filename = $request->file('service_image')->getClientOriginalName();
            $image->path =$changed_path;
            $image->save();
            $service->image()->associate($image);
        }

        $service->created_by = auth()->user()->id;

        $service->save();

        Notifications::add('Service Successfully updated', 'success');

        return redirect()->back();

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */

    public function destroy($id){

    $service = Service::with('image')->find($id);


     $image_count = Image::where('path',$service->image->path)->count();


     if($image_count == 1){
       Storage::disk('public')->delete($service->image->path);
     }
    $service->image()->delete();
    $service->delete();

    Notifications::add('Service Successfully deleted', 'success');

      return redirect()->back();



    }
}
