<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Http\Request;
use App\ShopZone;
use Notifications;
class ShopZoneController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $shop_zones = ShopZone::get();
        return view('admin.shop.shop_zone.index', ['shop_zones' => $shop_zones]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'name' => 'required'
      ],[
        'name.required' => 'Shop zone name is required'
      ]);
      $shop_zone = new ShopZone;
      $shop_zone->name = $request->name;
      $shop_zone->save();
      Notifications::add('Shop Zone Successfully addedd', 'success');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response(
     */
    public function edit($id)
    {
      $shop_zone = ShopZone::with('products')->findOrFail($id);
      return view('admin.shop.shop_zone.edit')->with(['shop_zone' => $shop_zone]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $this->validate($request,[
          'name' => 'required'
      ],[
          'name.required' => 'Shop zone name is required'
      ]);

      $shop_zone = ShopZone::findOrFail($id);
      $shop_zone->name = $request->get('name');
      $shop_zone->save();
      Notifications::add('Shop Zone is updated', 'success');

      return redirect()->action('ShopZoneController@index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $shop_zone = ShopZone::with('products')->findOrFail($id);
      if(count($shop_zone->products) == 0) {
        $shop_zone->delete();
        Notifications::add('Shop Zone Successfully deleted', 'success');
      } else {
        Notifications::add('Shop Zone cannot be deleted. it has some products added', 'error');
      }

      return redirect()->back();
    }
}
