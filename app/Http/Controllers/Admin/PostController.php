<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Category;
use App\Image;
use App\Post;
use App\PostTag;
use App\Tag;
use Illuminate\Support\Facades\Redirect;
use Title;
use Notifications;

class PostController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $posts = Post::with('category');
      // dd($posts);


        if (request()->has('status')) {
            $posts->byStatus(request('status'));
        } else {
                $posts->whereNotIn('status', ['deleted']);
        }

        Title::prepend('Posts');

        $q = request()->get('q', null);

        $data = [
            'posts' => $posts->sort()->paginate(10),
            'url_params' => request()->except(['q']),
            'q' => $q,
            'status' => request('status', 'all'),
            'title' => Title::renderr(' : ', true),
            'categories' => Category::where('category_type_id', 1)->get(),
        ];
        return view('admin.posts.index', $data);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Title::prepend('New Post');

        $data = [
            'categories' => Category::where('category_type_id', 1)->get(),
            'title'      => Title::renderr(' : ', true),
            'post'       => null,
            'save_url'   => route('admin-posts-store'),
            'tags'       => Tag::all(),
        ];

        return view('admin.posts.post', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Requests\PostRequest $request, $post_id = null)
    {
      // dd($request->all());

        $post = Post::findOrNew($post_id);

        if (empty($post)) {
            redirect()->back()->withInput();
        }

        $post->title = $request->get('title');
        $post->content = $request->get('content');
        $post->category_id = $request->get('category_id');

        if ($request->hasFile('featuredImg')) {

            // $path = $request->file('featuredImg')->store('public/images/blog');
            // $changed_path = str_replace("public","/public",$path);
            $path = $request->file('featuredImg')->store('storage/images/blog');
            $changed_path = str_replace("storage/","/public/storage/",$path);

            $image = new Image;
            $image->filename = $request->file('featuredImg')->getClientOriginalName();
            $image->path =$changed_path;
            $image->save();
            $post->image()->associate($image);
        }


        if ($request->has('update_slug')) {
            $post->resluggify();
        }
        $post->created_by = auth()->user()->id;
        $post->status = $request->get('status');
        $post->published_at = $request->get('published_at');
        $post->save();


        $this->_setTags($request->get('tags'), $post->id);

        Notifications::add('Blog post saved', 'success');

        return Redirect::route('admin-posts');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $post_id
     * @return \Illuminate\Http\Response
     */
    public function edit($post_id)
    {
        $post = Post::with('tags')->find($post_id);

        Title::prepend('Edit Post');
        Title::prepend($post->title);

        $data = [
            'categories' => Category::all(),
            'post'       => $post,
            'title'      => Title::renderr(' : ', true),
            'save_url'   => route('admin-posts-store', ['post_id' => $post_id]),
            'tags'       => Tag::all(),
        ];
//        dd($data);

        return view('admin.posts.post', $data);
    }

    public function toDraft($post_id)
    {
        $this->_setPostStatus($post_id, 'draft');
        Notifications::add('Post sent to drafts', 'warning');

        return Redirect::back();
    }

    public function toActive($post_id)
    {
        $this->_setPostStatus($post_id, 'active');
        Notifications::add('Post published', 'success');

        return Redirect::back();
    }

    public function toDeleted($post_id)
    {
        $this->_setPostStatus($post_id, 'deleted');
        Notifications::add('Post deleted', 'danger');

        return Redirect::back();
    }

    public function toDeleteForever($post_id)
    {
        $post = Post::find($post_id);
        $post->delete($post);
        Notifications::add('Post deleted forever', 'info');

        return Redirect::route('admin-posts');
    }

    public function toCategory($post_id, $category_id)
    {
        $category = Category::find($category_id);

        if (empty($category)) {
            Notifications::add('Category doesn\'t exist', 'danger');

            return Redirect::back();
        }

        $post = Post::find($post_id);
        $post->category_id = $category_id;
        $post->save();

        Notifications::add('Post Title "'.str_limit($post->title, '35', '...').'" moved to category "'.e($category->title).'"', 'info');

        return Redirect::back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    private function _setTags($tags_str, $post_id)
    {
        PostTag::where('post_id', $post_id)->delete();

        $tags = explode(', ', $tags_str);

        foreach ($tags as $tag) {
            if (trim($tag) == '') {
                continue;
            }
            $tag = mb_strtolower($tag);
            $dbtag = Tag::where('tag', 'like', $tag)->first();
            if (empty($dbtag)) {
                $dbtag = new Tag();
                $dbtag->tag = strip_tags($tag);
                $dbtag->save();
            }
            $post_tag = new PostTag();

            $post_tag->post_id = $post_id;
            $post_tag->tag_id = $dbtag->id;
            $post_tag->save();
        }
    }

    private function _setPostStatus($post_id, $status)
    {
        $post = Post::find($post_id);
        $post->status = $status;
        $post->save();

        return $post;
    }


}
