<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use Title;
use Conf;
use Illuminate\Http\Request;
use Notifications;
class SiteSettingsController extends Controller
{
    public function index() {
        Title::prepend('Settings');
        $data = [
            'title' => Title::renderr(' : ', true),
        ];

        view()->share('menu_item_active', 'settings');

        return view('admin.settings.index', $data);
    }

    public function social()
    {
        $links = Conf::get('social.links', [], false);

        if (!isset($links[0]['url'])) {
            $links = [];
            Conf::set('social.links', $links);
        }

        $data = [
            'title'    => 'Social Media Integration',
            'services' => trans('socials.services'),
            'created'  => $links,
        ];
        Title::prepend('Settings');
        Title::prepend($data['title']);
        view()->share('menu_item_active', 'settings');

        return view('admin.settings.social', $data);
    }

    public function socialLinksSave(Request $request) {
        $socials = Conf::get('social.links');

        $socials[] = [
            'service'    => $request->get('service'),
            'url'        => $request->get('url'),
            'show_title' => $request->has('show_title'),
        ];

        Conf::set('social.links', $socials);

        Notifications::add('New ' .$socials[0]['service']. ' Link is saved', 'success');

        return redirect()->route('admin-settings-social');
    }
    
    public function socialLinksDelete($index) {
        $socials = Conf::get('social.links');

        $link = ($socials[0]['service']);

        unset($socials[$index]);

        Conf::set('social.links', $socials);

        Notifications::add('Existing '. $link. ' Link is Deleted', 'success');

        return redirect()->route('admin-settings-social');
    }
    
    public function sitemap()
    {
        $sitemap_filename = Conf::get('sitemap.filename', 'sitemap.xml', false);

        Title::prepend('Settings');
        Title::prepend('Sitemap.xml file');

        $data = [
            'title'            => Title::renderr(' : ', true),
            'sitemap_exists'   => file_exists(public_path($sitemap_filename)),
            'sitemap_filename' => $sitemap_filename,
        ];

        view()->share('menu_item_active', 'settings');

        return view('admin.settings.sitemap', $data);
    }
}
