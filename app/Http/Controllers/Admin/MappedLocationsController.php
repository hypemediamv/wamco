<?php

namespace App\Http\Controllers\Admin;

use App\Image;
use App\MappedLocation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Title;
use Notifications;

class MappedLocationsController extends Controller
{
    /**
     * @param $id
     * @return mixed
     */
    private function getRecord($id)
    {
        $location = MappedLocation::findorFail($id);
        return $location;
    }

    public function index()
    {

        $mapped_location = MappedLocation::orderBy('name')->with('image');
        Title::prepend('Mapped Locations');

        $data = [
            'mapped_location' => $mapped_location->paginate(10),
            'title' => Title::renderr(' : ', true),
        ];

        return view('admin.mapped-location.index', $data);
    }

    public function create()
    {
        return view('admin.mapped-location.create');
    }

    public function store(Request $request)
    {

        $name_check = MappedLocation::where('address', $request->address)->exists();

        if ($name_check) {
            Notifications::add('The location name '.$request->address .' already added', 'error');
            return redirect()->back()->withInput();
        }

        if($request->hasFile('locationImage')) {
          $changed_path = $this->getImagePath($request);

          $image = new Image();
          $image->filename = $request->locationImage->getClientOriginalName();
          $image->path = $changed_path;

          $image->save();
        }



        $location = new MappedLocation();
        $location->name = $request->name;
        $location->description = $request->description;
        $location->address = $request->address;
        $location->latitude = $request->latitude;
        $location->longitude = $request->longitude;
        $location->contact_name = $request->contact_name;
        $location->number = $request->number;
        $location->email = $request->email;
        if ($request->locationImage) {$location->image()->associate($image);}
        $location->created_by = $request->user()->id;
        $location->save();

        Notifications::add('Location added successfully', 'success');
        return redirect()->back();

    }

    public function destroy($id)
    {
        $location = $this->getRecord($id);
        if($location->image_id) {
          $image = Image::where('id', $location->image_id)->get();
          $image[0]->delete();
          $file= $image[0]['path'];
          $filename = storage_path().'/app/public'.$file;
          \File::delete($filename);;
        }

        $location->delete();
        Notifications::add('Location deleted successfully', 'success');

        return redirect()->back();
    }

    /**
     * @param Request $request
     * @return mixed
     */
    private function getImagePath(Request $request)
    {
        $path = $request->locationImage->store('storage/images/locations');
        $changed_path = str_replace("storage/","/public/storage/",$path);
        return $changed_path;
    }

}
