<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\PostTag;
use App\Tag;
use Illuminate\Http\Request;
use Title;
use Notifications;

class TagsController extends Controller
{
    public function index()
    {
        Title::prepend('Manage Tags');

        $data = [
            'title' => Title::renderr(' : ', true),
            'tags'  => Tag::all(),
        ];

        return view('admin.tags.index', $data);
    }

    public function destroy($tag_id)
    {
        Tag::destroy($tag_id);
        PostTag::where(['tag_id' => $tag_id])->delete();

        Notifications::add('Tag removed', 'success');

        return redirect()->back();
    }
    
    
}
