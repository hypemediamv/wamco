<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Input;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\JsonResponse;

class FilesController extends Controller
{
    //upload image using ajax
    public function uploadImageAjax(Request $request)
    {
        $path = public_path('upload');
        if ($request->ajax()) {

//            return var_dump(Response::json($request->all()));
            $file = $request->file('fileToUpload');

            $filename = generate_filename($path, $file->getClientOriginalExtension());

            try {
                $file->move($path, $filename);
                $file = '/upload/'.$filename;
                $data = [
                    'message' => 'uploadSuccess',
                    'file'    => $file,
                ];
            } catch (FileException $e) {
                $data = [
                    'message' => 'uploadError',
                ];
            }
        } else {
            $data = [
                'message'  => 'uploadNotAjax',
                'formData' => Input::all(),
            ];
        }

        return new JsonResponse($data);
    }
}
