<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\BillingService;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Notifications;


class BillingServiceController extends Controller
{
  /**
   * Display a listing of the resource.
   *
   * @return \Illuminate\Http\Response
   */
  public function index()
  {

      $data = ['billings' => BillingService::with('billing')->paginate(5), 'users' => User::pluck('name', 'id')];

      // dd($data);

      return view('admin.billing_services.billing-service', $data);
      //
  }

  public function store(Request $request) {
      $this->validate($request,[
        'service_name' => 'required'
      ],[
        'service_name.required' => 'Please fill the service name field'
      ]);

      BillingService::create($request->all());

      Notifications::add('Billing services is created', 'success');

      return redirect()->back();
  }

    public  function edit($id) {
        $billing_services = BillingService::findOrFail($id);
        return view('admin.billing_services.billing-service-edit')->with(['billing_services' => $billing_services]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function update(Request $request, $id) {
        $this->validate($request,[
            'service_name' => 'required'
        ],[
            'service_name.required' => 'Please fill the service name field'
        ]);

        $billing_services = BillingService::findOrFail($id);
        $billing_services->service_name = $request->get('service_name');
        $billing_services->save();
        Notifications::add('Billing services is updated', 'success');

        return redirect()->action('BillingServiceController@index');


    }


  public function destroy($id) {
      $data = BillingService::with('billing')->findOrFail($id);
      $data->delete();
      Notifications::add('Billing services is deleted', 'success');

      return redirect()->back();

  }

}
