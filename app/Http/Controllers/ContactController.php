<?php

namespace App\Http\Controllers;
use App\Contact;
use App\PageType;
use App\Page;
use Illuminate\Http\Request;
use Notifications;
class ContactController extends Controller
{

  public function __construct(){
     $this->page_type = PageType::where('name','contact')->first();
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $pages = Page::where('page_type_id',$this->page_type->id)->get();
      return view('pages.contact', ['pages' => $pages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $page = Page::find($request->page_id);

      $contact = new Contact;
      $contact->name = $request->contact_name;
      $contact->address = $request->contact_address;
      $contact->phone_no = $request->phone_no;
      $contact->postal_code = $request->postal_code;
      $contact->email = $request->email;
      $contact->country = $request->country;
      $contact->fax_no = $request->fax_no;
      $contact->additional_info = $request->additional_info;
      $contact->page()->associate($page);
      $contact->save();

      Notifications::add('Contact Details Successfully Added', 'success');

      return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
      $contact_page = Page::with('contacts')->find($id);
      return view('pages.contacts.index', ['contact_page' => $contact_page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      $contact = Contact::findOrfail($id);
      return view('pages.contacts.edit', ['contact' => $contact]);


      //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

      $contact = Contact::find($id);
      $contact->name = $request->contact_name;
      $contact->address = $request->contact_address;
      $contact->phone_no = $request->phone_no;
      $contact->postal_code = $request->postal_code;
      $contact->email = $request->email;
      $contact->country = $request->country;
      $contact->fax_no = $request->fax_no;
      $contact->additional_info = $request->additional_info;
      $contact->save();

      Notifications::add('Contact Details Successfully Updated', 'success');

      return redirect()->back();

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      Contact::find($id)->delete();
      Notifications::add('Contact Details Successfully Deleted', 'success');
      return redirect()->back();
        //
    }
}
