<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use Illuminate\Http\Request;
use Title;
use Conf;

class BlogController extends Controller
{
    public function __construct()
    {
        Title::prepend(Conf::get('app.sitename'));
    }

    public function index($slug = '')
    {
        if ($slug != '') {
            $category = Category::i()->getBySlug($slug);
            if (empty($category)) {
                abort(404);
            }

            $category_id = $category->id;
            view()->share('active_category', $category_id);


            Title::prepend($category->slug);
        } else {
            $category = null;
            $category_id = null;
        }

        $search_blog = request('search_blog', null);

        if (!empty($search_blog )) {
        }

        $posts = Post::i()->getPostsByCategoryId($category_id, $search_blog);

        $data = [
            'posts' => $posts,
            'category' => $category,
            'search_blog' => $search_blog,
            'title' => Title::renderr(' : ', true),
        ];
        return view('blog.blog-front', $data);
    }

    public function view($slug)
    {
        $post = Post::i()->getBySlug($slug);

        try {
            if ($post->status == 'active') {

            }
        } catch (QueryException $e) {
            //This is just for demo purposes.
        }

        return view('blog.partials.single_post', ['post' => $post]);
    }

}
