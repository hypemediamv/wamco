<?php

namespace App\Http\Controllers;

use App\User;
use App\Billing;
use Illuminate\Http\Request;
use Hash;

class UserLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      return view('auth.user-login');
        //
    }


    public function verification($id){

      $user = User::find($id);
      return view('auth.pending_verification', ['user' => $user]);
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $this->validate($request, [
        'email' => 'required',
        'password' => 'required'
      ],
      [
        'email.required' => "User email is required",
        'password.required' => "User password is required"
      ]);
      $email = $request->email;

      $user = User::where('email',$email)->where('is_admin',0)->first();
      if($user!= null) {
        $password_match = Hash::check( $request->password, $user->password);

        if($password_match){

          $billings = Billing::with('billing_service','user')->where('billing_user_id',$user->id)->get();

          return view('billing.index',['billings' => $billings]);
        }
      }
      return redirect()->back();


        //
    }

    public function profile($user_id){

      $billings = Billing::with('billing_service','user')->where('billing_user_id',$user_id)->get();
      return view('billing.index',['billings' => $billings]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
