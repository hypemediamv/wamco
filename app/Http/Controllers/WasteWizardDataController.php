<?php

namespace App\Http\Controllers;

use App\WasteWizardData;
use App\WasteWizardDataBin;
use Notifications;
use Illuminate\Http\Request;

class WasteWizardDataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $datas = WasteWizardData::with('bin')->get();


        $bins = WasteWizardDataBin::get();
        return view('admin.wizard.index',['datas' => $datas, 'bins' => $bins]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = new WasteWizardData;
        $data->name = $request->name;
        $data->description = $request->description;
        $data->waste_wizard_data_bin_id = $request->bin_id;
        $data->save();
        Notifications::add('Waste Data successfully added', 'success');

        return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

        $data = WasteWizardData::find($id);

        return view('admin.data.show',['data' => $data]);       //
    }


    public function search($keyword){
        $results = WasteWizardData::where('name',$keyword)->get();

        return view('admin.data.results',['results' => $results]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    // public function text(Request $request){
    //
    //   $data = WasteWizardData::find($request->data_id);
    //   $data->delete();
    //   Notifications::add('Waste Data successfully deleted', 'success');
    //
    //   return redirect()->back();
    //
    // }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = WasteWizardData::find($id);
        $data->name = $request->name;
        $data->description = $request->description;
        $data->update();
        Notifications::add('Waste Data successfully updated', 'success');

        return redirect()->back();

        //
    }


    public function api(Request $request){

      $object = new \StdClass;
      $datas = WasteWizardData::with('bin.image')->where('name', 'LIKE', "%".$request->q."%")->get();


      $object->items = $datas;

      return response()->json($object);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = WasteWizardData::find($id);
      $data->delete();
      Notifications::add('Waste Data successfully deleted', 'success');
      return redirect()->back();

        //
    }


}
