<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use Notifications;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $categories = Category::all();

      return view('category.index', ['categories' => $categories]);
        //
    }

    public function shop_index(){
      $categories = Category::where('category_type_id', 2)->get();
      return view('admin.shop.categories', ['categories' => $categories ]);
    }

    public function post_index()
    {
        $categories = Category::where('category_type_id', 1)->get();
        return view('admin.posts.categories', ['categories' => $categories ]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

      $category = new Category;
      $category->title =$request->name;
      $category->slug =$request->name;
      $category->description =$request->description;
      $category->category_type_id = $request->category_type_id;
      $category->save();

      Notifications::add('Category Successfully Added', 'success');

      return redirect()->back();


        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

      return redirect()->back()->with('edit_category', Category::find($id));
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */


     public function updateOrDelete(Request $request){


       $category = Category::find($request->category_id);
       if(isset($request->delete)){
         $category->load('products');
         if($category->products != '[]'){
           return redirect()->back();
         }
         $category->delete();
         Notifications::add('Category Successfully Deleted', 'success');

       }else{
         $category->title = $request->name;
         $category->description = $request->description;
         $category->save();
         Notifications::add('Category Successfully Updated', 'success');

       }

       return redirect()->back();

     }
    public function update(Request $request, $id)
    {

        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
