<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Requests;
use Auth;
use App\Cart;
use App\CartProduct;
use App\User;
use App\Category;
use App\Product;
use Validator;
use \Cart as LaravelCart;
class CartController extends Controller
{

  // public function __construct(){
  //   $this->categories= Category::where('category_type_id',2)->get();
  //
  // }

  // private function getSignedUser(){
  //   return User::with('cart.cart_products.product')->where('id', Auth::user()->id )->first();
  // }
  //
  //   private function addCartProduct($cart_id, $product_id, $quantity = 1){
  //
  //
  //     $cart_product = new CartProduct;
  //     $cart_product->cart_id  = $cart_id;
  //     $cart_product->product_id = $product_id;
  //     $cart_product->quantity = $quantity;
  //     $cart_product->save();
  //
  //   }
  //
  //   private function updateCartProduct($cart_id, $product_id, $quantity){
  //     $cart_product =CartProduct::where('cart_id', $cart_id)->where('product_id', $product_id)->first();
  //     $cart_product->quantity = $quantity;
  //     $cart_product->save();
  //   }


    // public function add($product_id){
    //
    //
    //
    //
    //
    //   return redirect()->back()->with(['message' => 'Your item has been successfully added to cart', 'type' => 'success']);
    //
    //
    // }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      // return view('shop.cart.front-cart')->with(['cart' => LaravelCart::content()]);
      return view('shop.cart.front-cart');

//      $user = User::with('cart.cart_products.product')->where('id', Auth::user()->id )->first();
//
//      $total = 0;
//
//      if($user->cart!= null){
//
//
//      foreach($user->cart->cart_products as $product){
//        $total= $total + $product->product->price * $product->quantity;
//      }
//
//      $user->cart->total = $total;
//
//        }
//      return view('shop.cart.index',['user' => $user, 'categories' => $this->categories, 'signed_user' => $this->getSignedUser()]);

        //
    }


    public function order(){
      return view('shop.cart.front-cart-checkout');
    //   $user = User::with('cart.cart_products.product')->where('id', Auth::user()->id )->first();
    //
    //   $total = 0;
    //   foreach($user->cart->cart_products as $product){
    //     $total= $total + $product->product->price * $product->quantity;
    //   }
    //
    //   $user->cart->total = $total;
    //   return view('shop.cart.order',['user' => $user, 'categories' => $this->categories, 'signed_user' => $this->getSignedUser()]);
    //
    }



    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);
         if ($validator->fails()) {
            session()->flash('error_message', 'Quantity must be between 1 and 5.');
            return redirect()->back();
         }
        $product = Product::with('product_images.image')->find($request->id);
        // dd($product);
        $duplicates = LaravelCart::search(function ($cartItem, $rowId) use ($request) {
            return $cartItem->id === $request->id;
        });
        // dd($duplicates);
        if (!$duplicates->isEmpty()) {
            return redirect('shop/cart')->withSuccessMessage('Item is already in your cart!');
        }

        LaravelCart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->quantity, 'price' => $product->price,
              'options' => ['image' => $product->product_images[0]->image->path, 'description' => $product->description]])->associate('Product');
        return redirect('shop/cart')->withSuccessMessage('Item was added to your cart!');


      // if(Auth::user()) {
      //   $user =  Auth::user();
      //   $cart = Cart::where('user_id',$user->id)->first();
      //
      //   if($cart == null){
      //     $cart = new Cart;
      //     $cart->user_id = $user->id;
      //     $cart->save();
      //   }
      //
      //   $cart_already_with_product = CartProduct::where('cart_id', $cart->id)->where('product_id', $product_id)->first();
      //
      //   if($cart_already_with_product != null){
      //     $quantity_increment  =  $cart_already_with_product->quantity + 1;
      //     $this->updateCartProduct($cart->id, $product_id, $quantity_increment);
      //   }else{
      //     $this->addCartProduct($cart->id, $product_id);
      //
      //   }
      // } else {
      //
      //   $product = Product::with('product_images.image')->where('id', $request->id)->first();
      //   LaravelCart::add(['id' => $product->id, 'name' => $product->name, 'qty' => $request->quantity, 'price' => $product->price,
      //       'options' => ['image' => $product->product_images[0]->image->path, 'description' => $product->description]])->associate('Product');
      //   return view('shop.cart.front-cart')->withSuccessMessage('Item was added to your cart!');
      // }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect()->action('CartController@index');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    // public function edit($id)
    // {
    //     //
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      // Validation on max quantity
        $validator = Validator::make($request->all(), [
            'quantity' => 'required|numeric|between:1,5'
        ]);
         if ($validator->fails()) {
           session()->flash('error_message', 'Quantity must be between 1 and 5.');
          return response()->json(['success' => false]);
         }
        LaravelCart::update($id, $request->quantity);
        session()->flash('success_message', 'Quantity was updated successfully!');
        return response()->json(['success' => true]);

      // $cart_product = CartProduct::find($id);
      // if(isset($request->delete)){
      //   $cart_product->delete();
      // }
      // if(isset($request->update)){
      //   $cart_product->quantity = $request->quantity;
      //   $cart_product->update();
      // }
      //
      // return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      LaravelCart::remove($id);
      return redirect()->back()->withSuccessMessage('Item has been removed!');
    }

    // public function destroyAll() {
    //   LaravelCart::destroy();
    //   return redirect()->back()->withSuccessMessage('All the item has been removed!');
    // }
}
