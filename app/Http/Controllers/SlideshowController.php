<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Slide;
use App\Slideshow;
use App\SlideshowImage;
use Notifications;
class SlideshowController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $slideshows = Slideshow::with('slides')->get();
      return view('slideshows.index', ['slideshows' => $slideshows]);

        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // dd($request->all());
      $slideshow = new Slideshow();
      $slideshow->slideshow_name = $request->slideshow_name;

      if($request->frontpage != null){
        $slideshow->frontpage = true;
        $slideshows = Slideshow::all()->where('frontpage', '=', true);
        foreach ($slideshows as $slides) {
          $slides->frontpage = false;
          $slides->save();
        }
      }else{
        $slideshow->frontpage = false;
      }
      $slideshow->created_by = $request->user()->id;

      $slideshow->save();

      Notifications::add('SlideShow Successfully Added', 'success');

      return redirect()->back();

        //
    }

    public function assign(Request $request) {
      $slideshow = Slideshow::findOrFail($request->slideshow_id);
      if ($request->frontpage != null) {
        $slideshows = Slideshow::all()->where('frontpage', '=', true);
        foreach ($slideshows as $slides) {
          $slides->frontpage = false;
          $slides->save();
        }
        $slideshow->frontpage = true;
        $slideshow->save();
      }
      else{
        $slideshow->frontpage = false;
      }
      Notifications::add('SlideShow Successfully Assigned to Frontpage', 'success');

        return redirect()->back();
    }

      public function slide_store(Request $request){







      return redirect()->back();


    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
