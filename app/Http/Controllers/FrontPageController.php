<?php

namespace App\Http\Controllers;
use App\Resource;
use App\MappedLocation;
use Illuminate\Http\Request;
use App\Page;
class FrontPageController extends Controller
{
  public function download($id) {
    $resource = Resource::find($id);
    $resource->load('document');
    // dd($resource->load('document'),$resource->document->path,$resource->document->filename);
    return( \Response::download(public_path().$resource->document->path, $resource->document->filename) );

  }

  public function mappedLocation() {
    $locations = MappedLocation::with('image')->orderBy('name')->get();
    return view('locations.index', ['locations' => $locations] );
  }

  public function companyProfile() {
    $about_company = Page::where('page_type_id', 2)->get();
    return view('pages.company-profile', compact('about_company',$about_company));
  }
}
