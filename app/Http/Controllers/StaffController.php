<?php

namespace App\Http\Controllers;
use App\Staff;
use App\PageType;
use App\Page;
use App\Image;
use File;
use Storage;
use Illuminate\Http\Request;
use Notifications;
class StaffController extends Controller
{
   public function __construct(){
     $this->page_type = PageType::where('name','about')->first();
   }
   /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Responsep
    */
   public function index()
   {

     $pages = Page::where('page_type_id',$this->page_type->id)->get();
     return view('pages.about', ['pages' => $pages]);
       //_
   }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $page = Page::find($request->page_id);


      // $path = $request->file('staff_image')->store('public/images');
      // $changed_path = str_replace("public/","/",$path);

      $path = $request->file('staff_image')->store('storage/images/staffs');
      $changed_path = str_replace("storage/","/public/storage/",$path);


      $image = new Image;
      $image->filename = $request->file('staff_image')->getClientOriginalName();
      $image->path =$changed_path;
      $image->save();



      $staff = new Staff;
      $staff->name = $request->staff_name;
      $staff->designation = $request->staff_designation;
      $staff->quote = $request->staff_quote;
      $staff->image()->associate($image);
      $staff->page()->associate($page);
      $staff->created_by = $request->user()->id;
      $staff->save();

      Notifications::add('Staff Successfully Added', 'success');

      return redirect()->back();
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
     public function show($id)
     {

       $staff_page = Page::with('staff')->find($id);
       return view('pages.staff.index',[ 'staff_page' => $staff_page]);

         //
     }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $staff = Staff::findOrfail($id);
        return view('pages.staff.edit', ['staff' => $staff]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $staff = Staff::findOrfail($id);
        $staff->name = $request->get('staff_name');
        $staff->designation = $request->get('staff_designation');
        $staff->quote = $request->get('staff_quote');

        if ($request->hasFile('staff_image')) {

            $path = $request->file('staff_image')->store('storage/images/staffs');
            $changed_path = str_replace("storage/","/public/storage/",$path);

            $image = new Image;
            $image->filename = $request->file('staff_image')->getClientOriginalName();
            $image->path =$changed_path;
            $image->save();
            $staff->image()->associate($image);
        }

        $staff->created_by = auth()->user()->id;

        $staff->save();

        Notifications::add('Staff Successfully Updated', 'success');

        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $staff = Staff::with('image')->find($id);
      $image_count = Image::where('path',$staff->image->path)->count();

       if($image_count == 1){
         Storage::disk('public')->delete($staff->image->path);
       }
      $staff->image()->delete();
      $staff->delete();

      Notifications::add('Staff Successfully Deleted', 'success');

      return redirect()->back();
    }
}
