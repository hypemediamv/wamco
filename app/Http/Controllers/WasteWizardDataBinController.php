<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\WasteWizardDataBin;
use App\Image;
use Notifications;
class WasteWizardDataBinController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $bins = WasteWizardDataBin::with('image')->get();
      return view('admin.wizard.bin',['bins' => $bins]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      // 
      // $path = $request->file('image')->store('public/images/bin');
      // $changed_path = str_replace("public","/public",$path);


      $path = $request->file('image')->store('storage/images/bin');
      $changed_path = str_replace("storage/","/public/storage/",$path);


      $image = new Image;
      $image->filename = $request->file('image')->getClientOriginalName();
      $image->path =$changed_path;
      $image->save();

      $bin = new WasteWizardDataBin;
      $bin->name = $request->name;
      $bin->image_id = $image->id;
      $bin->save();
      Notifications::add('Waste Bin successfully added', 'success');

      return redirect()->back();

        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
      $data = WasteWizardDataBin::with('waste_data')->find($id);
      if(count($data->waste_data) == null) {
        $data->delete();
        Notifications::add('Waste Data bin successfully deleted', 'success');
        return redirect()->back();
      } else {
        Notifications::add('Waste Data bin connot be deleted. it contain waste datas', 'error');
        return redirect()->back();
      }
    }
}
