<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Notifications;
class AboutController extends Controller
{

  public function __construct(){
     $this->page_type = PageType::where('name','about')->first();
  }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

      $about_pages = Page::where('page_type_id',$this->page_type->id)->get();
      return view('pages.about', ['about_pages' => about_pages]);
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      if($request->page_id == null){
        $page = new Page;
        $page->name = $request->page_name;
        $page->description = $request->page_description;
        $page->title = $request->page_title;
        $page->active = 1;
        $page->created_by = $request->user()->id;
        $page->page_type()->associate($this->page_type);
        $page->save();
        Notifications::add('Page Successfully Added', 'success');
      }else{
        $page = Page::find($request->page_id);
        $page->name = $request->page_name;
        $page->description = $request->page_description;
        $page->title = $request->page_title;
        $page->update();
        Notifications::add('Page Successfully Updated', 'success');
      }
      return redirect()->back();
          //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      $edit_about = Page::find($id);
      return redirect()->back()->with(['edit_about' => $edit_about]);
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
