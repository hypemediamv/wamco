<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Redirect;
use Notifications;

class PostRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title'        => 'required',
            'published_at' => ['required', 'regex:#^[0-9]{4}-[0-9]{2}-[0-9]{2}\s[0-9]{2}:[0-9]{2}:[0-9]{2}$#s'],
//            'image_id' => 'required'
        ];
    }

//    public function messages()
//    {
//        return [
//            'title.required' => 'Blog Title is required',
//            'image_id.required' => 'Featured Image is required.'
//        ];
//    }

    public function response(array $errors)
    {
        return Redirect::back()->withInput();
    }

    public function formatErrors(Validator $validator)
    {
        foreach ($validator->errors()->all() as $error) {
            Notifications::add($error, 'danger');
        }

        $messages = [
            'title.required' => 'Blog Title is required',
            'image_id.required' => 'Featured Image is required.'
        ];

        return $validator->errors()->getMessages();
    }

}
