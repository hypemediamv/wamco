<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class FrontPageMenuComposer
{
    public function compose(View $view)
    {
        $pages = \App\Page::all();
        $view->with('pages', $pages);
    }
}