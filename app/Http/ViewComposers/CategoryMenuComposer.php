<?php

namespace App\Http\ViewComposers;

use Illuminate\Contracts\View\View;

class CategoryMenuComposer
{
    public function compose(View $view)
    {
        $categories = \App\Category::i()->withPostsCount();
        $posts_count = \App\Post::active()->count();
        $view->with('categories', $categories);
        $view->with('posts_count', $posts_count);
    }
}