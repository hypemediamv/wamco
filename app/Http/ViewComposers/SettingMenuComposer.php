<?php
/**
 * Created by PhpStorm.
 * User: shim
 * Date: 3/15/2017
 * Time: 4:30 PM
 */

namespace App\Http\ViewComposers;


use Illuminate\Contracts\View\View;

class SettingMenuComposer
{
    public function compose(View $view)
    {
        $items = [
            [
                'title' => 'Social Media Integration',
                'icon'  => 'fa-share-square-o',
                'url'   => route('admin-settings-social'),
                'route' => 'admin-settings-social',
            ],
//            [
//                'title' => 'Sitemap',
//                'icon'  => 'fa-sitemap',
//                'url'   => route('admin-settings-sitemap'),
//                'route' => 'admin-settings-sitemap',
//            ],
        ];

        $view->with('items', $items);
    }

}