<?php
namespace App\Http\ViewComposers;

use App\Post;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;

class BlogArchiveMenuComposer {
    
    public function compose(View $view)
    {
       $post_links = Post::all()->where('status', '=', 'active')->groupBy(function($date) {
            return Carbon::parse($date->created_at)->format('F Y');
        });

        $view->with('post_links', $post_links);
    }
}