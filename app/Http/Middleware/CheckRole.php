<?php

namespace App\Http\Middleware;

use App\Http\Controllers;
use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

      $user = $request->user();

      if($user!= null){

        if($user->is_admin == 1){
          return $next($request);
        }

        if($user->verified == 0){
          return redirect()->action('UserLoginController@verification',['id' => $user->id]);
        }else{
          return redirect()->action('UserLoginController@profile',['id' => $user->id]);
        }


      }else{
        return redirect('/register');
      }

    }
}
