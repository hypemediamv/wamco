<?php

namespace App\Providers;

use App\Http\ViewComposers\SettingMenuComposer;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer('blog.partials.categories-menu', \App\Http\ViewComposers\CategoryMenuComposer::class);
        view()->composer('blog.partials.blog-archived', \App\Http\ViewComposers\BlogArchiveMenuComposer::class);
        view()->composer('layouts.partials.front-navigation', \App\Http\ViewComposers\FrontPageMenuComposer::class);
        view()->composer('*', \App\Http\ViewComposers\SocialLinkComposer::class);

        //admin
        view()->composer('admin.partials.settings-menu',SettingMenuComposer::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
