<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MappedLocation extends Model
{
    protected $table = 'mapped_locations';
    
    protected $fillable = ['*'];
    
    public function image(){
        return $this->belongsTo(Image::class,'image_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by');
    }
}
