<?php

/**
 * Created by PhpStorm.
 * User: shim
 * Date: 12/23/2016
 * Time: 9:04 PM
 */
namespace App\Services;

use App\Post;
class Blog
{
    public function getRelatedPosts($tags, $except = null)
    {
        $limit = 4;
        $tag_ids = $tags->pluck('tag');
        $related = Post::whereHas('tags', function ($q) use ($tag_ids) {
            $q->whereIn('tag', $tag_ids);
        });
        if (!empty($except)) {
            $related = $related->where('id', '!=', $except);
        }
        $related = $related->active()->orderBy('created_at')
            ->take($limit)
            ->get();

        //If posts not enough for maximum
        if ($related->count() < $limit) {
            $left = $limit - $related->count();
            $excluded = $related->pluck('id')->toArray();
            $excluded[] = $except;
            $additional = Post::whereNotIn('id', $excluded)->active()->orderByRaw('RAND()')->limit($left)->get();
            $related = $related->merge($additional);
        }

        return $related;
    }

}