<!-- Menu Toggle Script -->
$("#menu-toggle").click(function(e) {
    e.preventDefault();
    $("#wrapper").toggleClass("toggled");
});



$(function(){
    $('#editor').trumbowyg({
        btnsDef: {
            image: {
                dropdown: ['insertImage'],
                ico: 'insertImage'
            }
        },
        upload:{serverPath: '/upload-image-ajax'},
        btns: ['formatting',
            '|', 'btnGrp-design',
            '|', 'link',
            '|', 'image',
            '|', 'btnGrp-justify',
            '|', 'btnGrp-lists',
            '|', 'more'],
        mobile: true,
        tablet: true,
        autogrow: true

    });
});

$("#js_image").fileinput({
    showUpload: false,
    maxFileCount: 5,
    showRemove: false,
    previewFileType: 'image',
    initialPreviewCount: 5,
    autoReplace: true,
    allowedFileTypes: ['image'],
    previewTemplates: {
        image: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   <img src="{data}" style="max-width: 10%;" class="" title="{caption}" alt="{caption}">\n' +
        '</div>\n',
        generic: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   {content}\n' +
        '</div>\n'
    }
});

$("#js_image2").fileinput({
    'allowedFileExtensions' : ['jpg', 'png','gif'],
    showRemove:false,
    showUpload:false,
});


$("#documentFile").fileinput({
    showUpload: false,
    maxFileCount: 1,
    showRemove: true,
    initialPreviewCount: 1,
    autoReplace: true,
    allowedFileExtensions : ['pdf', 'doc', 'docx', 'xsl', 'xslx'],
    previewTemplates: {
        image: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   <img src="{data}" style="max-width: 100%;" class="" title="{caption}" alt="{caption}">\n' +
        '</div>\n',
        generic: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   {content}\n' +
        '</div>\n'
    }
});
