/**
 * Created by shim on 4/10/2017.
 */
    // '<div class="marker-img"> <img src="' + locationObj[i].image.path + '" alt=""/></div>' +
function getDirection(lati, longi) {
    initMap = (
        function (){

            for(var i =0; i < locationObj.length; i++) {
                info[i] = '<div class="map-marker-custom">' +
                    '<div class="marker-info">' +
                    '<h4>' + locationObj[i].name + '</h4>' +
                    '<p class="rq-listing-item-address">' + locationObj[i].address + '</p>' +

                    '</div>' +
                    '</div>';
                locations[i] =
                    [info[i], locationObj[i].latitude, locationObj[i].longitude, i];

            }


            var map = new google.maps.Map(document.getElementById('map'), {
                zoom: 15,
                center: new google.maps.LatLng(lati, longi),
                scrollwheel: false,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            });
            // var mc = new MarkerClusterer(map);

            var infowindow = new google.maps.InfoWindow({
            });
            function load_info() {
                google.maps.event.addListener(marker, 'click', (function(marker, i) {
                    return function() {
                        infowindow.setContent(locations[i][0]);
                        infowindow.open(map, marker);
                        var iwOuter = $('.gm-style-iw');
                        var iwBackground = iwOuter.prev();
                        iwBackground.children(':nth-child(2)').css({'display' : 'none'});
                        iwBackground.children(':nth-child(4)').css({'display' : 'none'});
                        iwBackground.children(':first-child').css({'display' : 'none'});
                        //iwBackground.css({'display' : 'none'});
                        var iwCloseBtn = iwOuter.next();
                        iwCloseBtn.css({ opacity: '1', right: '40px', top: '25px' });
                    }
                })(marker, i));

            }


            var marker, i, markers=[];
            for (i = 0; i < locations.length; i++) {
                marker = new google.maps.Marker({
                    position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                    icon: 'map/img/real-estate/listing-plain-heading/map/icon-5'+'.png' ,
                    map: map
                });
                markers.push(marker);
                load_info();
            }
        }());

}
