<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Auth::routes();

Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'HomeController@index');
  Route::get('/collection-date', 'HomeController@collection_date_index');


    Route::get('/wizard', 'WasteWizardController@index');


    Route::resource('/user-login', 'UserLoginController');

    // Route::get('/home2','HomeController@old_index');

    Route::get('testscroll', function() {
    return view('index');
    });

    Route::get('/verification/{id}', 'UserLoginController@verification');


    Route::get('/blog', [
        'as'   => 'blog-index',
        'uses' => 'BlogController@index',
    ]);

    Route::get('/view/{slug}', [
        'as'   => 'view',
        'uses' => 'BlogController@view',
    ]);

    Route::get('/category/{slug?}', [
        'as'   => 'category',
        'uses' => 'BlogController@index',
    ]);

    Route::get('/service/single/{id}/', [
          'as'   => 'service-single',
          'uses' => 'ServiceController@single',
    ]);

    Route::get('/shop',[
    'as' => 'shop-index',
    'uses' =>  'ShopController@index',

    ]);

    Route::get('/shop/search', [
      'as'   => 'shop-search',
      'uses' => 'ShopController@product_search',
    ]);

    Route::get('/shop/category/{category}', [
      'as'   => 'shop-category',
      'uses' => 'ShopController@category_index',
    ]);

    Route::get('/shop/product/{product}', [
      'as'   => 'shop-product',
      'uses' => 'ShopController@product_index',
    ]);

    Route::get('/shop/single-product/{slug}', [
      'as'   => 'shop-product-single',
      'uses' => 'ShopController@show',
    ]);

    Route::get('/wizard/api/','WasteWizardDataController@api');

    //::::::::::::::::Front Page Controller Routes:::::::::::::::::://
    Route::get('zone-locations', 'FrontPageController@MappedLocation');
    Route::get('/downloads/{id}', 'FrontPageController@download');
    Route::get('company-profile', [
      'as' => 'company-profile',
      'uses' => 'FrontPageController@companyProfile',
    ]);

//    Route::post('/shop/cart/destroy-all','CartController@destroyAll');
    Route::resource('/shop/cart','CartController');

    // Route::patch('/shop/cart/add/{product}', 'CartController@add');
    Route::post('/shop/cart/order', 'CartController@order');


    Route::get('/profile/{user_id}','UserLoginController@profile')->middleware(['auth']);


    Route::group(['prefix' => 'admin', 'middleware' => ['auth','checkrole']], function() {



        Route::post('/text','WasteWizardDataController@text');
        Route::resource('/wizard-data','WasteWizardDataController');

        Route::post('/waste-data-bin-trash','WasteWizardDataBinController@trash');
        Route::resource('/wizard-data-bin','WasteWizardDataBinController');

        //:::::::::Billing Services:::::::::://
        Route::resource('/billing-services','BillingServiceController');
        Route::resource('/billings','BillingController');

        Route::resource('/user-verification','UserVerificationController');



        //:::::::DASHBOARD:::::::://
        Route::get('/dashboard', 'HomeController@dashboard');

        //:::::::SLIDE SHOW:::::::://
        Route::resource('/slideshows', 'SlideshowController' );
        Route::post('/slideshow/assign', 'SlideshowController@assign');
        Route::resource('/slideshow/slide', 'SlideController' );
        Route::get('/slideshow/{slideshow_id}', 'SlideController@index');
        Route::post('/slideshow/slide/add', 'SlideController@store');

        //:::::::PAGES:::::::://
        Route::resource('/pages', 'PageController' );
        Route::resource('/services', 'ServiceController' );
        Route::resource('/about','StaffController');
        Route::resource('/contacts', 'ContactController' );
        Route::resource('/resources', 'ResourceController');
        Route::post('/resources/tab/', 'ResourceController@store_tab');
        Route::delete('/resource/tab/{tab}', 'ResourceController@destroy_tab');
        Route::post('/resource/download/{id}/', 'ResourceController@download');

        //:::::::POSTS:::::::://
        Route::get('/posts', [
            'as'   => 'admin-posts',
            'uses' => 'Admin\PostController@index',
        ]);

        Route::get('/posts/new', [
            'as'   => 'admin-posts-new',
            'uses' => 'Admin\PostController@create',
        ]);

        Route::get('/posts/edit/{post_id}', [
            'as'             => 'admin-post-edit',
            'uses'           => 'Admin\PostController@edit',
        ])->where(['post_id' => '[0-9]+']);

        Route::post('/posts/store/{post_id?}', [
            'as'             => 'admin-posts-store',
            'uses'           => 'Admin\PostController@store',
        ])->where(['post_id' => '[0-9]+']);


        Route::get('/posts/to-draft/{post_id}', [
            'as'             => 'admin-post-to-draft',
            'uses'           => 'Admin\PostController@toDraft',
        ])->where(['post_id' => '[0-9]+']);

        Route::get('/posts/to-active/{post_id}', [
            'as'             => 'admin-post-to-active',
            'uses'           => 'Admin\PostController@toActive',
        ])->where(['post_id' => '[0-9]+']);

        Route::get('/posts/to-deleted/{post_id}', [
            'as'             => 'admin-post-to-deleted',
            'uses'           => 'Admin\PostController@toDeleted',
        ])->where(['post_id' => '[0-9]+']);

        Route::get('/posts/to-delete-forever/{post_id}', [
            'as'             => 'admin-post-to-delete-forever',
            'uses'           => 'Admin\PostController@toDeleteForever',
        ])->where(['post_id' => '[0-9]+']);

        Route::get('/posts/to-category/{post_id}/{category_id}', [
            'as'             => 'admin-post-to-category',
            'uses'           => 'Admin\PostController@toCategory',
        ])->where(['post_id' => '[0-9]+', 'category_id' => '[0-9]+']);

        //:::::::::TAGS::::::::://
        Route::get('/tags', [
            'as'   => 'admin-tags',
            'uses' => 'Admin\TagsController@index',
        ]);

        Route::get('/tags/remove/{tag_id}', [
            'as'            => 'admin-tags-remove',
            'uses'          => 'Admin\TagsController@destroy',
        ])->where(['tag_id' => '[0-9]+']);

        //:::::::CATEGORY:::::::://
        Route::post('/category','CategoryController@store');
        Route::get('/post/categories',[
            'as' => 'postCategory',
            'uses' => 'CategoryController@post_index'
        ]);

        Route::resource('/data','DataSearchController');
        Route::resource('/collection-date-zone','CollectionDateZoneController');
        Route::resource('/collection-date-site','CollectionDateSiteController');
        Route::resource('/collection-date','CollectionDateController');
        Route::resource('/shop-zone','ShopZoneController');



        Route::post('/category/UpdateOrDelete','CategoryController@updateOrDelete');

        Route::resource('/category','CategoryController');

        //:::::::SHOP:::::::://
        Route::get('/shop','ShopController@admin_index');
        Route::group(['prefix'=> 'shop'],function(){
            Route::get('/categories',[
              'as' => 'categories',
              'uses' => 'CategoryController@shop_index'
            ]);

            Route::get('/products', 'ProductController@index');

            Route::get('/products/all/', [
              'as'   => 'admin-products-all',
              'uses' => 'ProductController@all',
            ]);

            Route::get('/products/category/{category}', [
              'as'   => 'admin-products-category',
              'uses' => 'ProductController@category_index',
            ]);

            Route::get('/products/zone/{shop_zone}', [
              'as'   => 'admin-products-zone',
              'uses' => 'ProductController@zone_index',
            ]);

            Route::get('/product/{product}', [
              'as'   => 'admin-product',
              'uses' => 'ProductController@show',
            ]);

            Route::post('/product/add','ProductController@store');
            Route::post('/product/{id}/delete','ProductController@destroy');

        });

        //::::::::::::SETTINGS:::::::::::://
        Route::get('/settings', [
            'as' => 'admin-settings',
            'uses' => 'Admin\SiteSettingsController@index'
        ]);

        Route::get('/settings/sitemap', [
            'as'   => 'admin-settings-sitemap',
            'uses' => 'Admin\SiteSettingsController@sitemap',
        ]);

        Route::get('/settings/social', [
            'as'   => 'admin-settings-social',
            'uses' => 'Admin\SiteSettingsController@social',
        ]);

        Route::post('/settings/social-links', [
            'as'   => 'admin-settings-social-links-save',
            'uses' => 'Admin\SiteSettingsController@socialLinksSave',
        ]);

        Route::get('/settings/social-links/{index}/delete', [
            'as'   => 'admin-settings-social-links-delete',
            'uses' => 'Admin\SiteSettingsController@socialLinksDelete',
        ]);

        //::::::::::::Mapped Services:::::::::::://
        Route::get('/mapped-locations', [
            'as'   => 'admin-mapped-locations',
            'uses' => 'Admin\MappedLocationsController@index',
        ]);

        Route::get('/mapped-locations/create', [
            'as'   => 'admin-mapped-location-create',
            'uses' => 'Admin\MappedLocationsController@create',
        ]);

        Route::post('/mapped-locations/store', [
            'as'   => 'admin-mapped-location-store',
            'uses' => 'Admin\MappedLocationsController@store',
        ]);

        Route::get('/mapped-locations/delete/{id}', [
            'as'   => 'admin-mapped-location-delete',
            'uses' => 'Admin\MappedLocationsController@destroy',
        ]);
    });
});

//:::::::FILES::::::::// api route
Route::any('/upload-image-ajax', [
    'as'   => 'admin-upload-image-ajax',
    'uses' => 'Admin\FilesController@uploadImageAjax',
]);
