
$('#tags').tokenfield();
$('#tags').on('tokenfield:createtoken', function (event) {
    var existingTokens = $(this).tokenfield('getTokens');
    $.each(existingTokens, function (index, token) {
        if (token.value === event.attrs.value)
            event.preventDefault();
    });
});

//DateTimePicker
$(function () {
    $("#publishedAt").datetimepicker({
        locale: 'en',
        format: 'YYYY-MM-DD HH:mm:ss'
    });
});

$('.add-tag').on('click', function(){
    var inputTags = $('#tags');
    var newTag = $(this).attr('data-tag');
    var tags = inputTags.tokenfield('getTokens');
    tags.push(newTag);
    inputTags.tokenfield('setTokens', tags);
});

$("#featuredImg").fileinput({
    showUpload: false,
    maxFileCount: 1,
    showRemove: true,
    previewFileType: 'image',
    initialPreviewCount: 1,
    autoReplace: true,
    allowedFileTypes: ['image'],
    previewTemplates: {
        image: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   <img src="{data}" style="max-width: 50%;" class="" title="{caption}" alt="{caption}">\n' +
        '</div>\n',
        generic: '<div class="" id="{previewId}" data-fileindex="{fileindex}">\n' +
        '   {content}\n' +
        '</div>\n'
    }
});


$('#summernote').summernote({
  dialogsInBody: true,
  height: 300,                 // set editor height
  minHeight: null,             // set minimum height of editor
  maxHeight: null,             // set maximum height of editor
  focus: true                  // set focus to editable area after initializing summernote
});
